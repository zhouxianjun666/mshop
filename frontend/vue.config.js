const path = require('path');
const ZipPlugin = require('zip-webpack-plugin');
const config = {
    lintOnSave: true,
    productionSourceMap: true,
    transpileDependencies: [/muse-ui/],
    pages: {
        shop: 'src/shop/main.js',
        admin: 'src/admin/main.js'
    },
    configureWebpack: {
        plugins: [
            new ZipPlugin({
                path: path.join(__dirname, 'dist'),
                filename: 'dist.zip'
            })
        ],
        devServer: {
            disableHostCheck: true
        }
    }
};
module.exports = config;
