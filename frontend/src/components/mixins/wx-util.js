import { ShareLink } from '@/libs/shop-common';
export default {
    data () {
        return {
            jsApiList: [
                'updateAppMessageShareData',
                'updateTimelineShareData',
                'onMenuShareAppMessage',
                'onMenuShareTimeline',
                'openLocation'
            ],
            shareData: null,
            _$loading: null,
            _$isInit: false
        };
    },
    methods: {
        async wxJsSign () {
            const result = await this.fetch(
                '/api/wx/config/sign',
                { params: { url: window.location.href } },
                false
            );
            return result;
        },
        async initWxConfig (id) {
            this.shareData = {
                title: '小秘书微商城',
                desc: '小秘书微商城',
                link: ShareLink(id),
                imgUrl: this.imgs[0]
            };
            const result = await this.wxJsSign();
            console.log(this.shareData);
            console.log(result);
            if (result.success) {
                wx.config({
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: result.data.appId, // 必填，公众号的唯一标识
                    timestamp: result.data.timestamp, // 必填，生成签名的时间戳
                    nonceStr: result.data.nonceStr, // 必填，生成签名的随机串
                    signature: result.data.signature, // 必填，签名
                    jsApiList: this.jsApiList // 必填，需要使用的JS接口列表
                });
                wx.ready(() => {
                    if (this.shareData) {
                        if (this.updateTimelineShareData) {
                            wx.updateAppMessageShareData(this.shareData); // 1.4 分享到朋友
                            wx.updateTimelineShareData(this.shareData); // 1.4分享到朋友圈
                        } else {
                            wx.onMenuShareTimeline(this.shareData);
                            wx.onMenuShareAppMessage(this.shareData);
                        }
                    }
                });
            }
        },
        openWxMap (point, address, options = {}) {
            wx.openLocation(
                Object.assign(
                    {
                        latitude: point.lat, // 纬度，浮点数，范围为90 ~ -90
                        longitude: point.lng, // 经度，浮点数，范围为180 ~ -180。
                        name: '', // 位置名
                        address: address, // 地址详情说明
                        scale: 15, // 地图缩放级别,整形值,范围从1~28。默认为最大
                        infoUrl: '' // 在查看位置界面底部显示的超链接,可点击跳转
                    },
                    options
                )
            );
        }
    }
};
