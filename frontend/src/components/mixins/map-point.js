export default {
    methods: {
        transformPoint (data) {
            if (!data) {
                return { lng: 0, lat: 0 };
            }
            let address = data.split(',');
            return {
                lng: Number(address[0]),
                lat: Number(address[1])
            };
        }
    }
};
