const IMG_ADDRESS = process.env.VUE_APP_IMG_ADDRESS;
export default {
    methods: {
        transformImgs (data) {
            let subImgs = (data.subInfo || []).reduce((r, c) => {
                (c.img || []).forEach(e => r.add(e));
                return r;
            }, new Set());
            if (this.$store.state.user.merchant && this.$store.state.user.merchant.platform === 'standalone') {
                return [...(data.img || []), ...subImgs].filter(i => i);
            }
            return [...(data.img || []), ...subImgs].filter(i => i).map(i => `${IMG_ADDRESS}/${i}`);
        }
    }
};
