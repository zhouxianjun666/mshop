export default {
    data () {
        return {
            table: {
                data: [],
                query: {
                    pageNum: 1,
                    pageSize: 10
                },
                total: 0
            },
            loadingBtn: false
        };
    },
    mounted () {
        this.doQuery();
    },
    methods: {
        async beforeQuery () {},
        async afterQuery () {},
        async processQueryData (data, res) {
            return data;
        },
        async doQuery () {
            let before = await this.beforeQuery();
            if (before !== false) {
                const res = await this.fetch(this.table.url, { params: this.table.query });
                if (res.success && res.value) {
                    const data = await this.processQueryData(
                        res.value.list ? res.value.list : res.value || [], res
                    );
                    this.table.data = data;
                    this.table.total = Number(res.value.total) || this.table.data.lenght;
                } else {
                    this.table.data = [];
                    this.table.total = 0;
                }
            }
            this.loadingBtn = false;
            await this.afterQuery();
        },
        async changePage (page) {
            this.table.query.pageNum = page;
            this.doQuery();
        },
        async changePageSize (size) {
            this.table.query.pageSize = size;
            this.doQuery();
        }
    }
};
