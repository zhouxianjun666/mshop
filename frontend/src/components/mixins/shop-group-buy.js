import dayjs from 'dayjs';
import rules from './rules';
export default {
    mixins: [rules],
    data () {
        return {
            title: '填写订单',
            ask: '订单就要完成了，您确定要离开吗？',
            url: '',
            vo: {
                booking: Number(this.$route.query.bookingLimit) === 5140 ? dayjs().add(Number(this.$route.query.bookingDayLimit), 'day').format('YYYY-MM-DD 00:00:00') : dayjs().format('YYYY-MM-DD 00:00:00'),
                end: null,
                code: this.$route.query.code,
                days: 1,
                name: null,
                phone: null,
                quantity: 1,
                sex: 3001
            },
            calendar: []
        };
    },
    computed: {
        maxQuantity () {
            return Number(this.$route.query.maxQuantity) > 0 ? Number(this.$route.query.maxQuantity) : this.stock;
        },
        price () {
            return (
                (this.vo.quantity * this.startPrice * this.days) / 100
            );
        },
        stock () {
            if (!this.vo.booking) {
                return 0;
            }
            let find = this.findDay(this.vo.booking);
            return find ? find.stock : 0;
        },
        startPrice () {
            if (!this.vo.booking) {
                return Number(this.$route.query.price);
            }
            let find = this.findDay(this.vo.booking);
            if (Number(this.$route.query.single) === 0) {
                return find ? find.price : 0;
            } else {
                return find ? find.data.groupPrice : 0;
            }
        },
        days () {
            if (!this.vo.booking || !this.vo.end) {
                return 0;
            }
            return dayjs(this.vo.end).diff(
                dayjs(this.vo.booking),
                'day'
            );
        }
    },
    mounted () {
        this.initCalendar();
    },
    methods: {
        findDay (day) {
            return this.calendar.find(r =>
                dayjs(Number(r.start)).isSame(dayjs(day).startOf('d'))
            );
        },
        getPrice (day) {
            let find = this.findDay(day);
            if (Number(this.$route.query.single) === 0) {
                return find ? find.price / 100 || 0 : '-';
            } else {
                return find ? find.data.groupPrice / 100 || 0 : '-';
            }
        },
        disableDate (day) {
            let find = this.findDay(day);
            if (dayjs(day).format('YYYY-MM-DD') === dayjs().add(Number(this.$route.query.bookingDayLimit), 'day').format('YYYY-MM-DD')) {
                const now = dayjs().format('YYYY-MM-DD HH:mm');
                const booking = `${dayjs().add(Number(this.$route.query.bookingDayLimit), 'day').format('YYYY-MM-DD')} ${this.$route.query.bookingTimeLimit}`;
                return !dayjs(now).isBefore(dayjs(booking));
            }
            return !(find);
        },
        disableEndDate (day) {
            let start = dayjs(this.vo.booking);
            return (
                this.disableDate(day) ||
                (start.isValid() && !dayjs(day).isAfter(start))
            );
        },
        async initCalendar () {
            let start = null;
            if (Number(this.$route.query.bookingLimit) === 5140) {
                start = dayjs().add(Number(this.$route.query.bookingDayLimit), 'day').format('YYYY-MM-DD 00:00:00');
            } else {
                start = dayjs().format('YYYY-MM-DD 00:00:00');
            }
            let end = dayjs()
                .add(2, 'M')
                .format('YYYY-MM-DD 23:59:59');
            let result = await this.fetch(`/api/group/product/calendar/${this.$route.query.code}`, {
                params: {
                    start,
                    end
                }
            });
            this.calendar = result.success ? result.value : [];
        },
        success (result) {
            this.$refs.form.clear();
            this.$toasted.success('下单成功');
            this.$router.push({
                name: 'order-group-pay',
                params: {
                    orderSn: result.value.number,
                    money: result.value.money,
                    name: result.data.subName,
                    start: result.value.booking
                }
            });
        },
        error () {},
        after () {},
        judge (result) {
            return result.success === true;
        },
        async validate () {
            let validate = await this.$refs.form.validate();
            return validate;
        },
        async submit () {
            const validate = await this.validate();
            if (!validate) {
                return;
            }
            this.vo.days = this.days;
            let result = await this.fetch(
                `${this.url}`,
                {
                    data: this.vo,
                    method: 'POST',
                    params: { single: Number(this.$route.query.single) === 0 ? 'true' : 'false', number: this.$route.query.number }
                }
            );
            if (await this.judge(result)) {
                await this.success(result);
            } else {
                await this.error(result);
            }
            this.after();
        }
    }
};
