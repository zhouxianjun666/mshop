import dayjs from 'dayjs';
import rules from './rules';
import qs from 'qs';
export default {
    mixins: [rules],
    data () {
        return {
            title: '填写订单',
            ask: '订单就要完成了，您确定要离开吗？',
            url: '',
            vo: {
                visitor: {
                    name: null,
                    phone: null,
                    quantity: 1
                },
                item: {
                    product_sub_code: this.$route.query.code,
                    start: dayjs().format('YYYY-MM-DD 00:00:00'),
                    end: null,
                    days: 1
                }
            },
            calendar: []
        };
    },
    computed: {
        price () {
            return (
                (this.vo.visitor.quantity * this.startPrice * this.days) / 100
            );
        },
        stock () {
            if (!this.vo.item.start) {
                return 0;
            }
            let find = this.findDay(this.vo.item.start);
            return find ? find.stock : 0;
        },
        startPrice () {
            if (!this.vo.item.start) {
                return Number(this.$route.query.price);
            }
            let find = this.findDay(this.vo.item.start);
            return find ? find.price : 0;
        },
        days () {
            if (!this.vo.item.start || !this.vo.item.end) {
                return 0;
            }
            return dayjs(this.vo.item.end).diff(
                dayjs(this.vo.item.start),
                'day'
            );
        }
    },
    mounted () {
        this.initCalendar();
    },
    methods: {
        findDay (day) {
            return this.calendar.find(r =>
                dayjs(Number(r.start)).isSame(dayjs(day).startOf('d'))
            );
        },
        getPrice (day) {
            let find = this.findDay(day);
            return find ? find.price / 100 || 0 : '-';
        },
        disableDate (day) {
            let find = this.findDay(day);
            return !(find);
        },
        disableEndDate (day) {
            let start = dayjs(this.vo.item.start);
            return (
                this.disableDate(day) ||
                (start.isValid() && !dayjs(day).isAfter(start))
            );
        },
        async initCalendar () {
            let start = dayjs().format('YYYY-MM-DD 00:00:00');
            let end = dayjs()
                .add(2, 'M')
                .format('YYYY-MM-DD 23:59:59');
            let productType = this.$route.query.productType;
            let url = null;
            if (!productType) {
                url = '/api/limit/product/calendar/';
            } else {
                url = '/api/rebate/product/calendar/';
            }
            let result = await this.fetch(`${url}${this.$route.query.code}`, {
                params: {
                    start,
                    end
                }
            });
            this.calendar = result.success ? result.value : [];
        },
        async formData () {
            let form = {
                from: 351,
                shipping: Object.assign({}, this.vo.visitor),
                items: [
                    Object.assign(
                        {
                            visitor: [Object.assign({}, this.vo.visitor)],
                            quantity: this.vo.visitor.quantity
                        },
                        this.vo.item,
                        {
                            days: this.days
                        }
                    )
                ]
            };
            ['sex', 'quantity', 'sid'].forEach(d =>
                Reflect.deleteProperty(form.shipping, d)
            );
            Reflect.deleteProperty(form.items[0], 'end');
            return form;
        },
        success (result) {
            this.$refs.form.clear();
            this.$toasted.success('下单成功');
            this.$router.push({
                name: 'order-pay',
                params: {
                    orderSn: result.value.number,
                    money: result.value.money,
                    name: result.value.subName,
                    start: result.value.booking
                }
            });
        },
        error () {},
        after () {},
        judge (result) {
            return result.success === true;
        },
        async validate () {
            let validate = await this.$refs.form.validate();
            return validate;
        },
        async submit () {
            const validate = await this.validate();
            const query = qs.parse(window.location.search, {
                ignoreQueryPrefix: true
            });
            if (query._from) {
                query._from = query._from.includes('#')
                    ? query._from.substring(0, query._from.indexOf('#'))
                    : query._from;
                console.log(query);
            }
            if (!validate) {
                return;
            }
            const productType = this.$route.query.productType;
            let form = await this.formData();
            let result = await this.fetch(
                `${this.url}/${!productType ? 2 : 1}`,
                {
                    data: form,
                    method: 'POST',
                    params: { code: this.$route.query.code, _from: query._from }
                }
            );
            if (await this.judge(result)) {
                await this.success(result);
            } else {
                await this.error(result);
            }
            this.after();
        }
    }
};
