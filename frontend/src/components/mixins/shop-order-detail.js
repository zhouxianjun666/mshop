export default {
    data () {
        return {
            detail: {
                data: {}
            },
            custRefundRule: {},
            remindCount: 0
        };
    },
    mounted () {
        this.loadDetail();
    },
    computed: {
        refundBtnName () {
            return this.remindCount > 0 ? '申请退订' : '没有可退的票';
        },
        rerfunBtnTo () {
            return this.remindCount > 0
                ? {
                    path: '/order/refund',
                    query: { item_sn: this.detail.data.itemNumber }
                }
                : null;
        }
    },
    methods: {
        toPay () {
            this.$router.push({
                name: 'order-pay',
                params: {
                    orderSn: this.detail.data.orderNumber,
                    money: this.detail.data.money,
                    name: this.detail.data.subName,
                    start: this.detail.start
                }
            });
        },
        loadDetailParams () {
            return {
                number: this.$route.query.number
            };
        },
        async loadDetail () {
            let data = await this.fetch(
                `/api/order/detail/${this.$route.query.number}`
            );
            this.detail = data.value;
            this.remindCount =
                this.detail.data.quantity -
                this.detail.usedQuantity -
                this.detail.refundQuantity;
            this.custRefundRule = JSON.parse(this.detail.data.refundRule);
        }
    }
};
