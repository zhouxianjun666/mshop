import dayjs from 'dayjs';
import { ValidateMobile, ValidateIdCard } from '@/libs/common';
export default {
    data () {
        return {
            rules: {
                date: [
                    {
                        validate: val => val && dayjs(val).isValid(),
                        message: '请选择日期'
                    }
                ],
                start: [
                    {
                        validate: val => val && dayjs(val).isValid(),
                        message: '请选择开始日期'
                    }
                ],
                end: [
                    {
                        validate: val => val && dayjs(val).isValid(),
                        message: '请选择结束日期'
                    },
                    {
                        validate: val => dayjs(val).isAfter(dayjs(this.vo.item ? this.vo.item.start : this.vo.booking)),
                        message: '结束日期必须大于开始日期'
                    }
                ],
                name: [
                    {
                        validate: val => val && val.length > 0,
                        message: '请输入姓名'
                    }
                ],
                mobile: [
                    {
                        validate: ValidateMobile,
                        message: '请输入正确的手机号码'
                    }
                ],
                sid: [
                    {
                        validate: ValidateIdCard,
                        message: '请输入正确的身份证号码'
                    }
                ]
            }
        };
    }
};
