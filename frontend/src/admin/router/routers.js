import Main from '@/admin/views/main';

/**
 * 除了原生参数外可配置的参数:
 * meta: {
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面不会缓存
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 * }
 */

export default [
    {
        path: '/login',
        name: 'login',
        meta: {
            title: 'Login - 登录',
            hideInMenu: true
        },
        component: () => import('@/admin/views/login.vue')
    },
    {
        path: '/',
        name: '_home',
        redirect: '/home',
        component: Main,
        meta: {
            hideInMenu: true,
            notCache: true
        },
        children: [
            {
                path: '/home',
                name: 'home',
                meta: {
                    hideInMenu: true,
                    title: '首页',
                    notCache: true
                },
                component: () => import('@/admin/views/single-page/home.vue')
            }
        ]
    },
    {
        path: '/401',
        name: 'error_401',
        meta: {
            hideInMenu: true
        },
        component: () => import('@/admin/views/error-page/401.vue')
    },
    {
        path: '/500',
        name: 'error_500',
        meta: {
            hideInMenu: true
        },
        component: () => import('@/admin/views/error-page/500.vue')
    },
    {
        path: '*',
        name: 'error_404',
        meta: {
            hideInMenu: true
        },
        component: () => import('@/admin/views/error-page/404.vue')
    },
    {
        path: '/member',
        name: 'member',
        component: Main,
        meta: {
            title: '会员管理'
        },
        children: [
            {
                path: 'member/list',
                name: 'MemberList',
                meta: {
                    icon: 'md-people',
                    title: '会员管理'
                },
                component: () => import('@/admin/views/member/index.vue')
            },
            {
                path: 'member/info',
                name: 'MemberInfo',
                meta: {
                    title: '会员详情',
                    hideInMenu: true,
                    notCache: true
                },
                component: () => import('@/admin/views/member/info.vue')
            }
        ]
    },
    {
        path: '/rebate',
        name: 'rebate',
        component: Main,
        meta: {
            title: '返利营销'
        },
        children: [
            {
                path: 'rebate/list',
                name: 'RebateIndex',
                meta: {
                    icon: 'logo-usd',
                    title: '返利营销'
                },
                component: () => import('@/admin/views/rebate/index.vue')
            },
            {
                path: 'rebate/prodList',
                name: 'RebateProdList',
                meta: {
                    title: '添加返利',
                    hideInMenu: true
                },
                component: () => import('@/admin/views/rebate/productList.vue')
            }
        ]
    },
    {
        path: '/limit',
        name: 'limit',
        component: Main,
        meta: {
            title: '限时抢购'
        },
        children: [
            {
                path: 'limit/list',
                name: 'LimitList',
                meta: {
                    icon: 'md-cart',
                    title: '限时抢购'
                },
                component: () => import('@/admin/views/limit/index.vue')
            },
            {
                path: 'limit/prodList',
                name: 'LimitProdList',
                meta: {
                    title: '添加抢购',
                    hideInMenu: true
                },
                component: () => import('@/admin/views/limit/productList.vue')
            }
        ]
    },
    {
        path: '/group-booking',
        name: 'group-booking',
        component: Main,
        meta: {
            title: '拼团'
        },
        children: [
            {
                path: 'group-booking/index',
                name: 'GroupBookingIndex',
                meta: {
                    icon: 'logo-usd',
                    title: '拼团'
                },
                component: () => import('@/admin/views/group-booking/index.vue')
            },
            {
                path: 'group-booking/prodList',
                name: 'GroupBookingProdList',
                meta: {
                    title: '添加拼团',
                    hideInMenu: true
                },
                component: () => import('@/admin/views/group-booking/productList.vue')
            }
        ]
    },
    {
        path: '/report',
        name: 'report',
        component: Main,
        meta: {
            title: '报表',
            icon: 'md-stats'
        },
        children: [
            {
                path: 'rebate/report',
                name: 'RebateReport',
                meta: {
                    title: '返利报表'
                },
                component: () => import('@/admin/views/report/rebateReport.vue')
            },
            {
                path: 'rebate/detail',
                name: 'RebateDetail',
                meta: {
                    title: '返利明细'
                },
                component: () => import('@/admin/views/report/rebateDetail.vue')
            },
            {
                path: 'limit/report',
                name: 'LimitReport',
                meta: {
                    title: '抢购报表'
                },
                component: () => import('@/admin/views/report/limitReport.vue')
            },
            {
                path: 'limit/detail',
                name: 'LimitDetail',
                meta: {
                    title: '抢购明细'
                },
                component: () => import('@/admin/views/report/limitDetail.vue')
            },
            {
                path: 'group/report',
                name: 'GroupReport',
                meta: {
                    title: '拼团明细'
                },
                component: () => import('@/admin/views/report/groupDetail.vue')
            }
        ]
    },
    {
        path: '/cash',
        name: 'cash',
        component: Main,
        meta: {
            title: '提现管理'
        },
        children: [
            {
                path: 'cash/list',
                name: 'CashList',
                meta: {
                    icon: 'md-cash',
                    title: '提现管理'
                },
                component: () => import('@/admin/views/cash/index.vue')
            },
            {
                path: 'cash/info',
                name: 'CashInfo',
                meta: {
                    title: '提现详情',
                    hideInMenu: true,
                    notCache: true
                },
                component: () => import('@/admin/views/cash/info.vue')
            }
        ]
    },
    {
        path: '/banner',
        name: 'banner',
        component: Main,
        meta: {
            title: '广告管理'
        },
        children: [
            {
                path: 'banner/index',
                name: 'BannerIndex',
                meta: {
                    icon: 'logo-usd',
                    title: '广告管理'
                },
                component: () => import('@/admin/views/banner/index.vue')
            }
        ]
    },
    {
        path: '/refund',
        name: 'refund',
        meta: {
            title: '待退款列表',
            hideInMenu: process.env.VUE_APP_CAN_REFUND !== 'true'
        },
        component: Main,
        children: [
            {
                path: 'refund/list',
                name: 'refundList',
                meta: {
                    icon: 'md-settings',
                    title: '待退款列表',
                    hideInMenu: process.env.VUE_APP_CAN_REFUND !== 'true'
                },
                component: () => import('@/admin/views/refund/index.vue')
            }
        ]
    }, {
        path: '/setup',
        name: 'setup',
        meta: {
            icon: 'md-settings',
            title: '系统设置'
        },
        component: Main,
        children: [
            {
                path: 'setup/cash',
                name: 'SetupCash',
                meta: {
                    title: '提现设置'
                },
                component: () => import('@/admin/views/settings/cashSetting.vue')
            },
            {
                path: 'setup/wx',
                name: 'SetupWx',
                meta: {
                    title: '微信配置'
                },
                component: () => import('@/admin/views/settings/wxSetting.vue')
            },
            {
                path: 'wx/edit',
                name: 'wxEdit',
                meta: {
                    title: '编辑',
                    hideInMenu: true
                },
                component: () => import('@/admin/views/settings/wxEdit.vue')
            }
        ]
    }
];
