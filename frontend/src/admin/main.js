import Vue from 'vue';
import axios from 'axios';
import App from './App';
import router from './router';
import store from './store';
import iView from 'iview';
import vuescroll from 'vuescroll';
import 'vuescroll/dist/vuescroll.css';
import 'iview/dist/styles/iview.css';
import '@/assets/icons/iconfont.css';
import './common.less';

Vue.use(vuescroll);

const BASE_URL = process.env.VUE_APP_SERVER_ADDRESS;

Vue.prototype.$super = function (options) {
    return new Proxy(options, {
        get: (options, name) => {
            if (options.methods && name in options.methods) {
                return options.methods[name].bind(this);
            }
        }
    });
};
/**
 * 拉取服务器信息
 * @param url
 * @param showError
 * @param config
 * @returns {Promise.<*>}
 */
Vue.prototype.fetch = async (url, config, showError = true) => {
    iView['LoadingBar'].start();
    try {
        if (!url) {
            throw new Error('url must be not null');
        }
        const response = await axios(
            url,
            Object.assign(
                {
                    baseURL: BASE_URL,
                    withCredentials: true
                },
                config
            )
        );
        iView.LoadingBar.finish();
        const data = Object.assign({ success: false }, response.data || {});
        if (!data.success) {
            iView['LoadingBar'].error();
            if (data.code === 99) {
                await app.$store.dispatch('handleLogOut');
                app.$router.replace('/login');
            } else {
                if (showError) {
                    iView['Notice'].error({ title: data.msg || '操作失败' });
                }
            }
            return data;
        }
        return data;
    } catch (err) {
        iView['LoadingBar'].error();
        if (showError) {
            iView['Notice'].error({ title: err });
        }
        return { success: false };
    }
};

Vue.use(iView);
Vue.config.productionTip = false;

/* eslint-disable no-new */
const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
