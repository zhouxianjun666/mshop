import '@babel/polyfill';
import Vue from 'vue';
import MuseUI from 'muse-ui';
import Toasted from 'vue-toasted';
import Message from 'muse-ui-message';
import NProgress from 'muse-ui-progress';
import Loading from 'muse-ui-loading';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import store from './store';
import 'muse-ui/dist/muse-ui.css';
import 'muse-ui-message/dist/muse-ui-message.css';
import 'muse-ui-progress/dist/muse-ui-progress.css';
import 'muse-ui-loading/dist/muse-ui-loading.css';
import './common.less';
import './style.less';
import merge from 'merge';
import qs from 'qs';

Vue.use(MuseUI);
Vue.use(Toasted, { duration: 3000 });
Vue.use(Message);
Vue.use(NProgress);
Vue.use(Loading);

Vue.prototype.$super = function (options) {
    return new Proxy(options, {
        get: (options, name) => {
            if (options.methods && name in options.methods) {
                return options.methods[name].bind(this);
            }
        }
    });
};

const BASE_URL = process.env.VUE_APP_SERVER_ADDRESS;
axios.defaults.baseURL = BASE_URL;
axios.interceptors.request.use(config => {
    const query = qs.parse(window.location.search, {
        ignoreQueryPrefix: true
    });
    let hash = location.hash;
    if (hash) {
        const index = hash.indexOf('?');
        if (index > -1) {
            const hashQuery = qs.parse(hash.substring(index), {
                ignoreQueryPrefix: true
            });
            query.openid = query.openid || hashQuery.openid;
            query._s = query._s || hashQuery._s;
            query.mch = query.mch || hashQuery.mch;
        }
    }
    return merge.recursive(
        false,
        {
            withCredentials: true,
            headers: {
                'x-wx-openid': query.openid,
                'x-wx-sign': query._s,
                'x-wx-mch': query.mch
            }
        },
        config
    );
});
axios.interceptors.response.use(async res => {
    NProgress.done();
    const data = Object.assign({ success: false }, res.data || {});
    if (!data.success) {
        throw new URIError(data.msg || '操作失败!');
    }
    return data;
});

/**
 * 拉取服务器信息
 * @param url
 * @param showError
 * @param config
 * @returns {Promise.<*>}
 */
Vue.prototype.fetch = async (url, config, showError = true) => {
    NProgress.start();
    try {
        if (!url) {
            throw new Error('url must be not null');
        }
        const response = await axios(url, config);
        return response;
    } catch (err) {
        NProgress.done();
        if (showError) {
            Vue.toasted.error(err.message);
        }
        if (
            (err.code && err.code === 403) ||
            (err.response && err.response.status === 403)
        ) {
            window.location.reload();
        }
        return { success: false };
    }
};

/* eslint-disable no-new  */
new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
