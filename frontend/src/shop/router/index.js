import Vue from 'vue';
import Router from 'vue-router';
import NProgress from 'muse-ui-progress';
import Message from 'muse-ui-message';
import * as routers from './routers';
import { isWx } from '@/libs/common';
import store from '../store';

Vue.use(Router);
const router = new Router({
    routes: Reflect.ownKeys(routers)
        .map(k => routers[k])
        .filter(r => r.path && r.component)
});

router.beforeEach(async (to, from, next) => {
    NProgress.start();
    if (to.matched.length) {
        let meta = to.matched[0].meta;
        // 判断是否含有参数
        if (Array.isArray(meta.params) && meta.params.length > 0) {
            if (
                !to.params ||
                !meta.params.every(p => Reflect.has(to.params, p))
            ) {
                Message.alert('缺少参数', '提示', { type: 'warning' });
                next({ replace: true, path: from.path });
                return;
            }
        }
        if (meta.auth === true) {
            if (!isWx()) {
                Message.alert('请在微信客户端中打开', '提示', {
                    type: 'error'
                });
                next(false);
                NProgress.done();
                return;
            }
            const member = await store.dispatch('member');
            if (member) {
                next();
                return;
            }
            next({
                name: 'bind',
                params: { jump: to }
            });
            return;
        }
    }
    next();
});
router.afterEach(() => {
    NProgress.done();
});
router.onError(error => {
    const pattern = /Loading chunk (\d)+ failed/g;
    const isChunkLoadFailed = error.message.match(pattern);
    const targetPath = router.history.pending.fullPath;
    if (isChunkLoadFailed) {
        console.log('[router-error] loading', error);
        router.replace(targetPath);
    }
});
export default router;
