export const Index = {
    path: '/',
    name: 'index',
    component: () => import('@/shop/views/index.vue')
};
export const Bind = {
    path: '/bind',
    name: 'bind',
    component: () => import('@/shop/views/bind.vue')
};
export const Member = {
    path: '/member',
    name: 'member',
    meta: { auth: true },
    component: () => import('@/shop/views/member.vue')
};
export const MemberInfo = {
    path: '/member-info',
    name: 'member-info',
    meta: { auth: true },
    component: () => import('@/shop/views/member-info.vue')
};
/* 返利，抢购产品详情 */
export const TicketDetail = {
    path: '/detail/ticket',
    name: 'ticket-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/detail/ticket.vue')
};
export const HotelDetail = {
    path: '/detail/hotel',
    name: 'hotel-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/detail/hotel.vue')
};
export const LineDetail = {
    path: '/detail/line',
    name: 'line-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/detail/line.vue')
};
/* 拼团产品详情 */
export const GroupTicketDetail = {
    path: '/groupDetail/ticket',
    name: 'group-ticket-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupDetail/ticket.vue')
};
export const GroupHotelDetail = {
    path: '/groupDetail/hotel',
    name: 'group-hotel-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupDetail/hotel.vue')
};
export const GroupLineDetail = {
    path: '/groupDetail/line',
    name: 'group-line-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupDetail/line.vue')
};
/* 拼团子产品详情 */
export const GroupTicketSubDetail = {
    path: '/groupSubDetail/ticket',
    name: 'group-ticket-sub-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupSubDetail/ticket.vue')
};
export const GroupHotelSubDetail = {
    path: '/groupSubDetail/hotel',
    name: 'group-hotel-sub-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupSubDetail/hotel.vue')
};
export const GroupLineSubDetail = {
    path: '/groupSubSubDetail/line',
    name: 'group-line-sub-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/groupSubDetail/line.vue')
};
/* 抢购子产品详情 */
export const LimitSubDetail = {
    path: '/limitSubDetail/ticket',
    name: 'limit-sub-detail',
    meta: { auth: false },
    component: () => import('@/shop/views/limitSubDetail/ticket.vue')
};
export const TotalRebate = {
    path: '/rebate/total-rebate',
    name: 'total-rebate',
    meta: { auth: true },
    component: () => import('@/shop/views/rebate/total-rebate.vue')
};
// 普通产品预订
export const BuyTicket = {
    path: '/order/buy/ticket',
    name: 'buy-ticket',
    meta: { auth: true },
    component: () => import('@/shop/views/order/buy/ticket.vue')
};
export const BuyHotel = {
    path: '/order/buy/hotel',
    name: 'buy-hotel',
    meta: { auth: true },
    component: () => import('@/shop/views/order/buy/hotel.vue')
};
export const BuyLine = {
    path: '/order/buy/line',
    name: 'buy-line',
    meta: { auth: true },
    component: () => import('@/shop/views/order/buy/line.vue')
};
// 拼团产品预订
export const GroupBuyTicket = {
    path: '/order/groupBuy/ticket',
    name: 'group-buy-ticket',
    meta: { auth: true },
    component: () => import('@/shop/views/order/groupBuy/ticket.vue')
};
export const GroupBuyHotel = {
    path: '/order/groupBuy/hotel',
    name: 'group-buy-hotel',
    meta: { auth: true },
    component: () => import('@/shop/views/order/groupBuy/hotel.vue')
};
export const GroupBuyLine = {
    path: '/order/groupBuy/line',
    name: 'group-buy-line',
    meta: { auth: true },
    component: () => import('@/shop/views/order/groupBuy/line.vue')
};
export const OrderPay = {
    path: '/order/pay',
    name: 'order-pay',
    meta: { auth: true, params: ['orderSn', 'money', 'name', 'start'] },
    component: () => import('@/shop/views/order/pay.vue')
};
export const OrderGroupPay = {
    path: '/order/group-pay',
    name: 'order-group-pay',
    meta: { auth: true, params: ['orderSn', 'money', 'name', 'start'] },
    component: () => import('@/shop/views/order/group-pay.vue')
};
export const OrderList = {
    path: '/order/list',
    name: 'order-list',
    meta: { auth: true },
    component: () => import('@/shop/views/order/list.vue')
};
/* 拼团订单列表 */
export const GroupOrderList = {
    path: '/group/order/list',
    name: 'group-order-list',
    meta: { auth: true },
    component: () => import('@/shop/views/order/group-list.vue')
};
// 订单详情
export const OrderDetailTicket = {
    path: '/order/detail/ticket',
    name: 'order-detail-ticket',
    meta: { auth: true },
    component: () => import('@/shop/views/order/detail/ticket.vue')
};
export const OrderDetailHotel = {
    path: '/order/detail/hotel',
    name: 'order-detail-hotel',
    meta: { auth: true },
    component: () => import('@/shop/views/order/detail/hotel.vue')
};
export const OrderDetailLine = {
    path: '/order/detail/line',
    name: 'order-detail-line',
    meta: { auth: true },
    component: () => import('@/shop/views/order/detail/line.vue')
};
// 订单退订
export const OrderRefund = {
    path: '/order/refund',
    name: 'order-refund',
    meta: { auth: true },
    component: () => import('@/shop/views/order/refund.vue')
};
// 提现申请
export const DestoonFinanceCash = {
    path: '/destoon-finance-cash',
    name: 'destoon-finance-cash',
    meta: { auth: true },
    component: () => import('@/shop/views/destoon-finance-cash.vue')
};
// 外连接跳转
export const jump = {
    path: '/jump',
    name: 'jump',
    meta: { auth: true },
    component: () => import('@/shop/views/jump.vue')
};
