import axios from 'axios';

export default {
    state: {
        wx: null,
        member: null,
        merchant: null,
        title: null
    },
    mutations: {
        setWx (state, wx) {
            state.wx = wx;
        },
        setTitle (state, title) {
            state.title = title;
        },
        setMember (state, member) {
            state.member = member;
        },
        setMerchant (state, merchant) {
            state.merchant = merchant;
        }
    },
    actions: {
        async member ({ state, commit }, force = false) {
            if (!state.wx || force === true) {
                const result = await axios('/info', { params: { force } });
                if (result.success) {
                    const member = result.value.member;
                    commit('setMember', member);
                    commit('setMerchant', result.value.merchant);
                    commit('setTitle', result.value.merchant.name);
                    commit('setWx', result.data.wx);
                    if (member && !result.data.wx) {
                        commit('setWx', { headimgurl: member.avatar, nickname: member.nickname });
                    }
                }
                return result.value ? result.value.member : null;
            }
            return state.member;
        }
    }
};
