'use strict';
export function getAttribute (dic, prop, value, isArray = false) {
    let result = [];
    for (let item of dic) {
        if (item[prop] === value) {
            if (!isArray) return item;
            result.push(item);
        }
    }
    return isArray ? result : null;
}
export function opt (dic, value, defVal = null, prop = 'id') {
    let v = getAttribute(dic, prop, value, false);
    return v || defVal;
}

export const Status = [{
    id: 0,
    name: '启用'
}, {
    id: 1,
    name: '禁用'
}];

export const StatusDiy = (...diy) => exports.Status.map(item => Object.assign(item, { name: diy.shift() }));

export const LevelStar = [{
    id: 5500,
    value: 5
}, {
    id: 5501,
    value: 4
}, {
    id: 5502,
    value: 3
}, {
    id: 5503,
    value: 2
}, {
    id: 5504,
    value: 1
}];

export const LineLabel = [{
    id: 5974,
    name: '亲子游'
}, {
    id: 5975,
    name: '爸妈游'
}, {
    id: 5976,
    name: '雪山之旅'
}, {
    id: 5977,
    name: '摄影之旅'
}, {
    id: 5978,
    name: '表演秀'
}, {
    id: 5979,
    name: '经济游'
}, {
    id: 5980,
    name: '浪漫之旅'
}, {
    id: 5981,
    name: '世界遗产'
}, {
    id: 5982,
    name: '踏青之旅'
}, {
    id: 5983,
    name: '五一'
}, {
    id: 5984,
    name: '十一'
}, {
    id: 5985,
    name: '中秋'
}, {
    id: 5986,
    name: '元旦'
}, {
    id: 5987,
    name: '免费接送机'
}];

export const SexType = [{
    id: 3000,
    name: '未知'
}, {
    id: 3001,
    name: '男'
}, {
    id: 3002,
    name: '女'
}];

export const MemberSexType = [{
    id: 0,
    name: '未知'
}, {
    id: 1,
    name: '男'
}, {
    id: 2,
    name: '女'
}];

export const MemberBillType = [{
    id: 1,
    name: '返利'
}, {
    id: 2,
    name: '提现'
}];

export const WithdrawType = [{
    id: 1,
    name: '微信'
}, {
    id: 2,
    name: '支付宝'
}, {
    id: 3,
    name: '银行卡'
}];

export const WithdrawStatus = [{
    id: 1,
    name: '待审核'
}, {
    id: 2,
    name: '待付款'
}, {
    id: 3,
    name: '已提现'
}];

export const WithdrawLimitType = [{
    id: 1,
    name: '每周'
}, {
    id: 2,
    name: '每月'
}];

export const groupType = [{
    id: 1,
    name: '创建'
}, {
    id: 2,
    name: '开启'
}, {
    id: 3,
    name: '失败'
}, {
    id: 4,
    name: '完成'
}];

export const orderType = [{
    id: 1,
    name: '待支付'
}, {
    id: 2,
    name: '支付中'
}, {
    id: 3,
    name: '已支付'
}, {
    id: 4,
    name: '已取消'
}];
