import dayjs from 'dayjs';
import WordArray from 'crypto-js/lib-typedarrays';
import SHA1 from 'crypto-js/sha1';
/**
 * 把vo对象中的字符串数字转为Number
 * 当数字>=13位则不处理
 * @param { * } vo 对象
 */
export const StringToNumber = vo => {
    if (!vo) return vo;
    if (Array.isArray(vo)) {
        vo.forEach((item, index) => vo[index] = StringToNumber(item));
    } else {
        if (typeof vo === 'object') {
            Reflect.ownKeys(vo).forEach(key => {
                let value = vo[key];
                if (!value || value === '' || value === true || value === false || typeof value === 'function') return;
                if (Array.isArray(value) || typeof value === 'object') {
                    vo[key] = StringToNumber(value);
                    return;
                }
                if (!isNaN(value) && value.length < 13) {
                    vo[key] = Number(value);
                }
            });
        } else {
            if (!isNaN(vo) && vo.length < 13) {
                vo = Number(vo);
            }
        }
    }
    return vo;
};

/**
 * 数字格式化展示 例: 123456 => 123,456
 * @param {Number | String} num 源数字
 * @param {Number} count 多少位分隔
 * @param {String} sp 分隔符
 */
export const NumberFormat = (num, count = 3, sp = ',') => {
    return num.toString().replace(new RegExp(`(\\d)(?=(?:\\d{${count}})+$)`, 'g'), `$1${sp}`);
};

/**
 * 强制小数位 例4.5 => 4.50; 1 => 1.00; 1.123 => 1.12
 * @param {Number | String} num 源数字
 * @param {Number} count 小数位数
 */
export const ForceDecimal = (num, count = 2) => {
    let str = num.toString();
    let [start, end = ''] = str.split('.');
    return `${start}.${end.length > count ? end.substring(0, count) : end.padEnd(count, '0')}`;
};

/**
 * 手机号码验证
 * @param {Number | String} mobile 手机号码
 * @returns Boolean
 */
export const ValidateMobile = mobile => {
    return mobile && mobile.match(/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/);
};

/**
 * 时间戳格式化
 * @param {String | Number} val 时间戳
 * @param {String} format 格式
 */
export const DateFormat = (val, format = 'YYYY-MM-DD HH:mm:ss') => {
    return val ? dayjs(Number(val)).format(format) : '-';
};

/**
 * 邮箱链接
 * @param {String} val 邮箱地址
 */
export const EmailLink = (val) => {
    return `<a href="mailto:${val}">${val}</a>`;
};

/**
 * 身份证号码验证
 * @param {Number | String} code 身份证号码
 * @returns Boolean
 */
export const ValidateIdCard = code => {
    let city = {
        11: '北京',
        12: '天津',
        13: '河北',
        14: '山西',
        15: '内蒙古',
        21: '辽宁',
        22: '吉林',
        23: '黑龙江 ',
        31: '上海',
        32: '江苏',
        33: '浙江',
        34: '安徽',
        35: '福建',
        36: '江西',
        37: '山东',
        41: '河南',
        42: '湖北 ',
        43: '湖南',
        44: '广东',
        45: '广西',
        46: '海南',
        50: '重庆',
        51: '四川',
        52: '贵州',
        53: '云南',
        54: '西藏 ',
        61: '陕西',
        62: '甘肃',
        63: '青海',
        64: '宁夏',
        65: '新疆',
        71: '台湾',
        81: '香港',
        82: '澳门',
        91: '国外 '
    };

    if (!code || !/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X|x)$/i.test(code) || !city[code.substr(0, 2)]) {
        return false;
    }
    // 18位身份证需要验证最后一位校验位
    if (code.length === 18) {
        code = [...code];
        // ∑(ai×Wi)(mod 11)
        // 加权因子
        const factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        // 校验位
        const parity = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2', 'x'];
        let sum = 0;
        for (let i = 0; i < 17; i++) {
            sum += code[i] * factor[i];
        }

        return parity[sum % 11] === code[17];
    }
    return true;
};

/**
 * 判断是否是微信浏览器
 */
export const isWx = () => /MicroMessenger/i.test(navigator.userAgent);

// 单位转换成元
export const formatNumber = function (numbers) {
    if (numbers) { return Number(numbers / 100).toFixed(2); }
    return Number(0).toFixed(2);
};

/**
 * 文件sha1值
 * @param {*} file 文件
 * @param {*} progress 进度
 */
export const FileSha1 = (file, progress = (percent, loaded, total) => {}) => {
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onprogress = (e) => progress(Math.floor((e.loaded / e.total) * 100), e.loaded, e.total);
        reader.onloadend = (e) => {
            resolve(SHA1(WordArray.create(e.target.result)).toString());
        };
        reader.readAsArrayBuffer(file);
    });
};
