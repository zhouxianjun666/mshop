import qs from 'qs';
/**
 * 预定游玩信息解析
 * @param {String} notes 信息
 * @param {*} sp 分隔符
 * @param {*} join 换行符
 */
export const ParseNotes = (notes, sp = '+', join = '\n') => {
    if (!notes) {
        return '';
    }
    let index = notes.indexOf(sp);
    if (index > 0 && index < notes.length - 1) {
        let before = notes.substring(0, index);
        if (before.endsWith(join)) {
            return `${notes.substring(0, index)}${notes.substr(index + sp.length)}`;
        }
        return `${notes.substring(0, index)}${join}${notes.substr(index + sp.length)}`;
    }
    return notes.endsWith(sp) ? notes.substring(0, notes.length - sp.length) : notes;
};

export const ShareLink = (id) => {
    const query = qs.parse(window.location.search, {
        ignoreQueryPrefix: true
    });
    let hash = location.hash;
    if (hash) {
        const index = hash.indexOf('?');
        if (index > -1) {
            const hashQuery = qs.parse(hash.substring(index), {
                ignoreQueryPrefix: true
            });
            Reflect.deleteProperty(hashQuery, 'openid');
            Reflect.deleteProperty(hashQuery, '_s');
            hash = `${hash.substring(0, index)}?${qs.stringify(hashQuery, { encode: false })}`;
        }
    }
    Reflect.deleteProperty(query, 'openid');
    Reflect.deleteProperty(query, '_s');
    query._from = btoa(id);
    const rightUrl = encodeURIComponent(`${location.origin}${location.pathname}?${qs.stringify(query, { encode: false })}${hash}`);
    let BASE_URL = process.env.VUE_APP_SERVER_ADDRESS;
    if (!BASE_URL) {
        BASE_URL = `${document.location.protocol}//${document.domain}`;
    }
    return `${BASE_URL}/wx/jump/go?url=${rightUrl}&mch=${query.mch}`;
};
