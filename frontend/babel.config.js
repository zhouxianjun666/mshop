module.exports = {
    presets: [
        ['@vue/app', {
            useBuiltIns: 'entry'
        }]
    ],
    plugins: ['jsx-v-model']
};
