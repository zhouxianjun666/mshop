#! /bin/bash

# 参数注入环境变量
init(){
  if [ -f "./boot.ini" ];then
    IFS='='
    while read k v
    do
      # 去掉空行，未赋值参数以及#开头注释行
      if [ "${k:0:1}" == "#" ] || [ -z "$v" ]; then
        continue
      fi
      export $k="$v"
    done < boot.ini
  else
    echo "no system properties, use default."
  fi
}

# 启动方法
start(){
 exec java -Xms128m -Xmx2048m -jar ../target/mshop-0.0.1-SNAPSHOT.jar 5 > /dev/null &
}

# 停止方法
stop(){
 ps -ef|grep java|grep -v grep|grep mshop-0.0.1-SNAPSHOT.jar|awk '{print $2}'|while read pid
 do
    kill -9 $pid
 done
}

case "$1" in
start)
init
start
;;
stop)
stop
;;
restart)
stop
init
start
;;
init)
init
;;
*)
printf 'Usage: %s {start|stop|restart}\n' "$prog"
exit 1
;;
esac
