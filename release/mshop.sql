﻿/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.1.114
 Source Server Type    : MySQL
 Source Server Version : 50634
 Source Host           : 192.168.1.114:3306
 Source Schema         : mshop

 Target Server Type    : MySQL
 Target Server Version : 50634
 File Encoding         : 65001

 Date: 20/12/2018 11:16:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_banner`;
CREATE TABLE `t_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seq` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `effective_start` datetime(0) NOT NULL COMMENT '有效开始时间',
  `effective_end` datetime(0) NULL DEFAULT NULL COMMENT '有效结束时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'banner 横幅广告' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_group_order
-- ----------------------------
DROP TABLE IF EXISTS `t_group_order`;
CREATE TABLE `t_group_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number` bigint(20) NOT NULL COMMENT '拼团号',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '当前已加入人数',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `member_id` int(11) NOT NULL COMMENT '发起者',
  `product_id` int(11) NOT NULL COMMENT '主产品ID',
  `sub_code` bigint(20) NOT NULL COMMENT '子产品编号',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品名称',
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品名称',
  `single` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否单人直接购买',
  `status` int(4) NOT NULL COMMENT '状态:1-创建;2-开启;3-失败;4-完成;',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '成团时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `number`(`number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '团队订单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_group_order_member
-- ----------------------------
DROP TABLE IF EXISTS `t_group_order_member`;
CREATE TABLE `t_group_order_member`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `order_number` bigint(20) NULL DEFAULT NULL COMMENT '主订单号',
  `item_number` bigint(20) NULL DEFAULT NULL COMMENT '子订单号',
  `group_id` int(11) NOT NULL COMMENT '拼团ID',
  `number` bigint(20) NOT NULL COMMENT '拼团流水号',
  `booking` datetime(0) NOT NULL COMMENT '游玩日期',
  `days` int(11) NOT NULL COMMENT '天数',
  `quantity` int(11) NOT NULL COMMENT '数量(一天的张数)',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `sid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `sex` int(11) NULL DEFAULT NULL COMMENT '性别',
  `money` int(11) NOT NULL COMMENT '用户支付金额',
  `transaction_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信支付订单号',
  `status` int(4) NOT NULL DEFAULT 1 COMMENT '状态:1-待支付;2-支付中;3-已支付;4-已取消;',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `number`(`number`) USING BTREE,
  INDEX `member_id`(`member_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '拼团订单成员表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_group_product
-- ----------------------------
DROP TABLE IF EXISTS `t_group_product`;
CREATE TABLE `t_group_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `product_id` int(11) NOT NULL COMMENT '主产品ID',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品名称',
  `product_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品图片',
  `sub_code` bigint(20) NOT NULL COMMENT '子产品编号',
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品名称',
  `sub_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品图片',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '供应商',
  `type` int(11) NOT NULL COMMENT '产品类型',
  `purchase_price` int(11) NOT NULL DEFAULT 0 COMMENT '进货价(分)(设置时当天进货价)',
  `price` int(11) NOT NULL DEFAULT 0 COMMENT '售价(单独购买)',
  `group_price` int(11) NOT NULL DEFAULT 0 COMMENT '拼团价',
  `num` int(11) NOT NULL DEFAULT 1 COMMENT '成团人数',
  `max_quantity` int(11) NOT NULL DEFAULT 0 COMMENT '最大张数0不限定',
  `notes` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '须知',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_merchant_code`(`merchant_id`, `sub_code`) USING BTREE COMMENT '同一个子产品在同一个商户下只能有一个拼团'
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '拼团产品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_limit_product
-- ----------------------------
DROP TABLE IF EXISTS `t_limit_product`;
CREATE TABLE `t_limit_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `product_id` int(11) NOT NULL COMMENT '主产品ID',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品名称',
  `product_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品图片',
  `sub_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品图片',
  `sub_code` bigint(20) NOT NULL COMMENT '子产品编号',
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品名称',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '供应商',
  `type` int(11) NOT NULL COMMENT '产品类型',
  `purchase_price` int(11) NULL DEFAULT 0 COMMENT '进货价(分)(设置时当天进货价)',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `price` int(11) NOT NULL COMMENT '抢购价(分)',
  `max_quantity` int(11) NOT NULL DEFAULT 0 COMMENT '会员该产品最大购买数量',
  `seq` int(11) NOT NULL DEFAULT 0 COMMENT '序号',
  `notice` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '抢购须知',
  `start_time` datetime(0) NOT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_mch_sub`(`merchant_id`, `sub_code`) USING BTREE COMMENT '一个子产品同一个商户同时只能限购一个'
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '限时抢购产品' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_member
-- ----------------------------
DROP TABLE IF EXISTS `t_member`;
CREATE TABLE `t_member`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `phone` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `balance` int(11) NOT NULL DEFAULT 0 COMMENT '余额(分)',
  `rebate` int(11) NOT NULL DEFAULT 0 COMMENT '返利总额(分)',
  `openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信openid',
  `sex` int(1) NULL DEFAULT 0 COMMENT '性别(值为1时是男性，值为2时是女性，值为0时是未知)',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `alipay_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `alipay_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付宝姓名',
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡银行名称',
  `bank_branch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡银行支行',
  `bank_account_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡账号名',
  `bank_card_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡卡号',
  `pid` int(11) NULL DEFAULT NULL COMMENT '推广会员ID',
  `refresh_time` datetime(0) NULL DEFAULT NULL COMMENT '推广刷新时间',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '状态:0-禁用;1-启用;2-限制提现',
  `version` bigint(20) NOT NULL DEFAULT 1 COMMENT '版本号',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_mch_phone`(`merchant_id`, `phone`) USING BTREE,
  UNIQUE INDEX `uk_mch_openid`(`merchant_id`, `openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员(推广员)表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_member_bills
-- ----------------------------
DROP TABLE IF EXISTS `t_member_bills`;
CREATE TABLE `t_member_bills`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `ref_id` int(11) NOT NULL COMMENT '关联ID',
  `type` int(1) NOT NULL COMMENT '类型: 1-返利;2-提现',
  `old` int(11) NOT NULL COMMENT '变动之前余额(分)',
  `newer` int(11) NOT NULL COMMENT '变动之后余额(分)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员余额流水表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_merchant
-- ----------------------------
DROP TABLE IF EXISTS `t_merchant`;
CREATE TABLE `t_merchant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `access_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业授权ID',
  `access_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业授权KEY',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微店名称',
  `phone` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平台',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平台地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID(冗余字段)',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `product_id` int(11) NOT NULL COMMENT '主产品ID',
  `sub_code` bigint(20) NOT NULL COMMENT '子产品编号',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品名称',
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品名称',
  `shop_product_id` int(11) NOT NULL COMMENT '商城产品ID',
  `type` int(11) NOT NULL COMMENT '类型:1-返利;2-限购;3-拼团',
  `order_number` bigint(20) NOT NULL COMMENT '主订单编号',
  `item_number` bigint(20) NOT NULL COMMENT '子订单编号',
  `quantity` int(11) NOT NULL DEFAULT 0 COMMENT '数量(一天的张数)',
  `days` int(11) NOT NULL DEFAULT 1 COMMENT '天数',
  `booking` datetime(0) NOT NULL COMMENT '游玩日期',
  `money` int(11) NOT NULL DEFAULT 0 COMMENT '用户需要支付的金额(分)',
  `original_money` int(11) NOT NULL COMMENT '原价(订单金额(分))',
  `rebate` int(11) NULL DEFAULT 0 COMMENT '返利金额(分)',
  `from_member` int(11) NULL DEFAULT NULL COMMENT '哪个会员分享的',
  `transaction_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信支付订单号',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '状态:1-待支付;2-支付中;3-已支付;',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
  `product_type` int(11) NULL DEFAULT NULL COMMENT '产品类型',
  `refund_rule` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退订规则',
  `contact` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单联系人',
  `contact_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单联系人手机',
  `form_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单数据',
  `log` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单日志',
  `is_rebate` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否返利',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_number`(`order_number`) USING BTREE,
  UNIQUE INDEX `item_number`(`item_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_rebate_product
-- ----------------------------
DROP TABLE IF EXISTS `t_rebate_product`;
CREATE TABLE `t_rebate_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `product_id` int(11) NOT NULL COMMENT '主产品ID',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品名称',
  `product_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主产品图片',
  `sub_code` bigint(20) NOT NULL COMMENT '子产品编号',
  `sub_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品名称',
  `sub_imgs` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子产品图片',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '供应商',
  `type` int(11) NOT NULL COMMENT '产品类型',
  `purchase_price` int(11) NULL DEFAULT 0 COMMENT '进货价(分)(设置时当天进货价)',
  `price` int(11) NOT NULL DEFAULT 0 COMMENT '营销售价',
  `rebate_type` int(2) NOT NULL DEFAULT 1 COMMENT '返利类型: 1-百分比;2-固定',
  `rebate` int(11) NULL DEFAULT 0 COMMENT '返利(0~100)%',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_mch_sub`(`merchant_id`, `sub_code`) USING BTREE COMMENT '同一个子产品在同一个商户下只能有一个返利'
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '返利产品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_refund_order
-- ----------------------------
DROP TABLE IF EXISTS `t_refund_order`;
CREATE TABLE `t_refund_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_number` bigint(20) NOT NULL COMMENT '主订单编号',
  `item_number` bigint(20) NOT NULL COMMENT '子订单编号',
  `number` bigint(20) NOT NULL COMMENT '退订编号',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `quantity` int(11) NOT NULL COMMENT '退订数量',
  `pay_amount` int(11) NOT NULL COMMENT '订单支付金额',
  `fee` int(11) NOT NULL COMMENT '手续费',
  `money` int(11) NOT NULL COMMENT '退支付者金额',
  `original_fee` int(11) NOT NULL COMMENT '原手续费',
  `cause` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原因',
  `status` int(11) NOT NULL COMMENT '状态:1-退订中;2-退款中;3-退款成功',
  `refund_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信退款单号',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `number`(`number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `t_withdraw`;
CREATE TABLE `t_withdraw`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `type` int(1) NOT NULL COMMENT '提现类型:1-微信;2-支付宝;3-银行卡',
  `money` int(11) NOT NULL COMMENT '提现金额(分)',
  `payment_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '付款账号(支付宝账号、银行卡号)',
  `payer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '付款人',
  `status` int(1) NOT NULL COMMENT '状态:1-待审核;2-待付款;3-已提现',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '申请/创建时间 ',
  `audit_time` datetime(0) NULL DEFAULT NULL COMMENT '审核时间',
  `payment_time` datetime(0) NULL DEFAULT NULL COMMENT '付款时间',
  `bill_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流水号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '提现表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_withdraw_config
-- ----------------------------
DROP TABLE IF EXISTS `t_withdraw_config`;
CREATE TABLE `t_withdraw_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `week_day` int(1) NULL DEFAULT NULL COMMENT '每周几(1~7)',
  `month_day` int(2) NULL DEFAULT NULL COMMENT '每月多少号(1~31)',
  `type` int(1) NOT NULL COMMENT '类型: 1 - week; 2 - month',
  `limit` int(11) NULL DEFAULT 1 COMMENT '最低限额(分)',
  `start` time(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end` time(0) NULL DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_mch`(`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '提现配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_wx_config
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_config`;
CREATE TABLE `t_wx_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户ID',
  `certificate` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信P12证书',
  `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信应用ID',
  `mch_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信商户ID',
  `mch_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信商户KEY',
  `app_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信应用秘钥',
  `center` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否走PHP微信中控',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信配置' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
