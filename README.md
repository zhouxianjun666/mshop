# mshop

微店产品：返利营销、限时抢购、拼团

## 打包
```
mvn clean -DskipTests package -Ptest
```

## 启动 
```
java -jar mshop-0.0.1-SNAPSHOT.jar > log 2>&1 &
```

## 微信支付配置
- 支付授权目录(JSAPI支付授权目录) - http://域名/frontend/
- 开通现金红包
- 登录微店后台配置微信配置

## 微信公众号配置
- IP白名单
- JS接口安全域名
- 网页授权域名

## 初始化微店商户
在`t_merchant`表中配置:
- `access_id` - 企业授权ID
- `access_key` - 企业授权KEY
- `name` - 微店展示名称
- `phone` - 手机号码(登录)
- `password` SALT: 手机号码;PASS: 密码。[md5($salt.$pass)](http://www.cmd5.com/hash.aspx)
- `platform` - 平台码(小秘书3.0:all580)
- `url` - 平台地址(http://192.168.1.250:10001)
