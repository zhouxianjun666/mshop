package com.all580.mshop.manager;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.StrUtil;
import cn.hutool.db.DbRuntimeException;
import com.all580.mshop.Constant;
import com.all580.mshop.dto.Result;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TWithdraw;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.mapper.TWithdrawMapper;
import com.all580.mshop.mapper.TWxConfigMapper;
import com.all580.mshop.vo.WithdrawPayerVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 12:56
 */
@Component
@Slf4j
public class WithdrawManager {
    @Resource
    private TWithdrawMapper withdrawMapper;
    @Resource
    private TWxConfigMapper wxConfigMapper;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private MemberManager memberManager;

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void payment(WithdrawPayerVo vo, TWithdraw withdraw, TMerchant merchant) {
        // 保存会员流水
        TMember member = memberManager.balanceChange(withdraw.getMemberId(), Constant.MemberBillType.WITHDRAW, withdraw.getId(), -withdraw.getMoney());
        // 更新提现记录
        update(withdraw, vo);

        if (withdraw.getType() == Constant.WithdrawType.WX) {
            Result<?> result = sendRedpack(withdraw, member, merchant);
            if (!result.isSuccess()) {
                throw new RuntimeException(result.getMsg());
            }
        }
    }

    private void update(TWithdraw withdraw, WithdrawPayerVo vo) {
        int ret = withdrawMapper.updateByPrimaryKeySelective(new TWithdraw()
                .setId(withdraw.getId())
                .setStatus(Constant.WithdrawStatus.SUCCESS)
                .setPaymentTime(new Date())
                .setPaymentAccount(vo.getPaymentAccount())
                .setPayer(vo.getPayer())
        );
        if (ret <= 0) {
            throw new DbRuntimeException("更新提现记录失败");
        }
    }

    private Result<?> sendRedpack(TWithdraw withdraw, TMember member, TMerchant merchant) {
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("请先配置微信支付");
        }
        String billNo = withdraw.getBillNo();
        if (StrUtil.isNotBlank(billNo)) {
            Result<?> result = wxManager.queryRedpack(billNo, config);
            if (!result.isSuccess()) {
                return result;
            }
            String status = result.get("status");
            if ("FAILED".equals(status)) {
                billNo = null;
                log.info("微信提现:{} 发送失败, 重新生成订单号", withdraw.getBillNo());
            }
        }
        if (StrUtil.isBlank(billNo)) {
            billNo = String.valueOf(snowflake.nextId());
            withdrawMapper.updateByPrimaryKeySelective(new TWithdraw().setId(withdraw.getId()).setBillNo(billNo));
        }
        return wxManager.sendRedpack(String.valueOf(billNo), member.getOpenid(), withdraw.getMoney(), "返利", "快乐就完事了", merchant.getName(), config);
    }
}
