package com.all580.mshop.manager;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.all580.mshop.dto.AliyunSms;
import com.all580.mshop.dto.Result;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 17:18
 */
@Component
public class AliyunSmsManager {
    @Autowired
    private AliyunSms aliyunSms;
    private IAcsClient acsClient = null;

    private IAcsClient getAcsClient() throws ClientException {
        if (acsClient == null) {
            IClientProfile profile = DefaultProfile.getProfile(aliyunSms.getRegion(), aliyunSms.getAccessId(), aliyunSms.getAccessKey());
            DefaultProfile.addEndpoint(aliyunSms.getRegion(), aliyunSms.getRegion(), "Dysmsapi", "dysmsapi.aliyuncs.com");
            this.acsClient = new DefaultAcsClient(profile);
        }
        return this.acsClient;
    }

    /**
     * 新阿里云短信发送
     *
     * @param template 短信模板
     * @param params   短信参数
     * @param phones   接收手机号
     * @return
     */
    @SneakyThrows
    public Result<?> send(String template, Map<String, String> params, String... phones) {
        String phone = ArrayUtil.join(phones, ",");
        SendSmsRequest request = new SendSmsRequest();
        request.setMethod(MethodType.POST);
        request.setPhoneNumbers(phone);
        request.setSignName(aliyunSms.getSign());
        request.setTemplateCode(template);
        if (params != null && !params.isEmpty()) {
            request.setTemplateParam(JSONUtil.toJsonStr(params));
        }
        SendSmsResponse response = getAcsClient().getAcsResponse(request);
        boolean success = response.getCode() != null && "OK".equals(response.getCode());

        return Result.builder()
                .code(success ? Result.SUCCESS : Result.FAIL)
                .msg(CODE_MSG.getOrDefault(response.getCode(), response.getMessage()))
                .value(response)
                .build();
    }

    private static final Map<String, String> CODE_MSG = new HashMap<String, String>() {{
        put("isp.RAM_PERMISSION_DENY", "RAM权限DENY");
        put("isp.OUT_OF_SERVICE", "业务停机");
        put("isp.PRODUCT_UN_SUBSCRIPT", "未开通云通信产品的阿里云客户");
        put("isp.PRODUCT_UNSUBSCRIBE", "产品未开通");
        put("isp.ACCOUNT_NOT_EXISTS", "账户不存在");
        put("isp.ACCOUNT_ABNORMAL", "账户异常");
        put("isp.SMS_TEMPLATE_ILLEGAL", "短信模板不合法");
        put("isp.SMS_SIGNATURE_ILLEGAL", "短信签名不合法");
        put("isp.INVALID_PARAMETERS", "参数异常");
        put("isp.SYSTEM_ERROR", "系统错误");
        put("isp.MOBILE_NUMBER_ILLEGAL", "非法手机号");
        put("isp.MOBILE_COUNT_OVER_LIMIT", "手机号码数量超过限制");
        put("isp.TEMPLATE_MISSING_PARAMETERS", "模板缺少变量");
        put("isp.TEMPLATE_PARAMS_ILLEGAL", "模板参数非法");
        put("isp.BUSINESS_LIMIT_CONTROL", "业务限流");
        put("isp.INVALID_JSON_PARAM", "JSON参数不合法，只接受字符串值");
        put("isp.BLACK_KEY_CONTROL_LIMIT", "黑名单管控");
        put("isp.PARAM_LENGTH_LIMIT", "参数超出长度限制");
        put("isp.PARAM_NOT_SUPPORT_URL", "不支持URL");
        put("isp.AMOUNT_NOT_ENOUGH", "账户余额不足");
    }};
}
