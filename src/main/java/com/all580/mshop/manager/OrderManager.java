package com.all580.mshop.manager;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.*;
import com.all580.mshop.mapper.*;
import com.all580.mshop.util.Util;
import com.alone.tk.mybatis.JoinExample;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/13 8:57
 */
@Component
@Slf4j
public class OrderManager {
    @Resource
    private TRebateProductMapper rebateProductMapper;
    @Resource
    private TLimitProductMapper limitProductMapper;
    @Resource
    private TMemberMapper memberMapper;
    @Resource
    private TOrderMapper orderMapper;
    @Resource
    private TMerchantMapper merchantMapper;
    @Resource
    private TRefundOrderMapper refundOrderMapper;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private PlatformManager platformManager;
    @Autowired
    private SystemConfig systemConfig;

    @Locks("ORDER:CREATE:MEMBER:#{#memberId}")
    public BasicProduct check(TMerchant merchant, int type, long code, int memberId, Map<String, Object> params) {
        BasicProduct product;
        switch (type) {
            case Constant.ProductType.REBATE:
                product = rebateProductMapper.selectOne((TRebateProduct) new TRebateProduct().setMerchantId(merchant.getId()).setSubCode(code));
                break;
            case Constant.ProductType.LIMIT:
                product = limitProductMapper.selectOne((TLimitProduct) new TLimitProduct().setMerchantId(merchant.getId()).setSubCode(code));
                break;
            default:
                throw new RuntimeException("不支持的类型");
        }
        if (product == null) {
            throw new RuntimeException("产品不存在");
        }
        if (product instanceof TLimitProduct) {
            TLimitProduct limitProduct = (TLimitProduct) product;
            Date startTime = limitProduct.getStartTime();
            Date endTime = limitProduct.getEndTime();
            boolean in = DateUtil.isIn(new Date(), startTime, endTime);
            if (!in) {
                throw new RuntimeException(String.format("不在限购时间范围内: %s ~ %s", DateUtil.formatDateTime(startTime), DateUtil.formatDateTime(endTime)));
            }
            Integer maxQuantity = ((TLimitProduct) product).getMaxQuantity();
            if (maxQuantity != null && maxQuantity > 0) {
                List items = (List) params.get("items");
                if (!CollectionUtils.isEmpty(items)) {
                    Map map = (Map) items.get(0);
                    int quantity = MapUtils.getIntValue(map, "quantity");
                    if (quantity > maxQuantity) {
                        throw new RuntimeException("超出限购数量:" + maxQuantity);
                    }
                    int count = orderMapper.selectByJoinExampleTransform(JoinExample.builder(TOrder.class).noAlias()
                            .addCol("sum(quantity)")
                            .where(JoinExample.Where.custom(false)
                                    .andEqualTo(TOrder::getMemberId, memberId)
                                    .andEqualTo(TOrder::getType, Constant.ProductType.LIMIT)
                                    .andEqualTo(TOrder::getShopProductId, product.getId())
                                    .andNotEqualTo(TOrder::getStatus, Constant.OrderStatus.CANCEL)
                            )
                            .build(),
                            int.class
                    );
                    if (count + quantity > maxQuantity) {
                        throw new RuntimeException("超出限购数量:" + maxQuantity);
                    }
                }
            }
        }
        return product;
    }

    public TMember parseFromMember(TMerchant merchant, String from) {
        if (StrUtil.isBlank(from)) {
            return null;
        }
        TMember member = null;
        try {
            String plain = Base64.decodeStr(from);
            int id = Integer.valueOf(plain);
            member = memberMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            log.warn("解析来源会员异常:{}", from, e);
        }
        if (member != null && member.getMerchantId() != merchant.getId().intValue()) {
            throw new RuntimeException("分享者不在当前商户下");
        }
        return member;
    }

    @Locks("#{#order.number}")
    public boolean processOrderStatus(Order order, TWxConfig config) {
        if (order.getStatus() == Constant.OrderStatus.PAYING) {
            log.info("订单: {} 支付中,主动查询支付状态", order.getNumber());
            Result<?> queryResult = queryOrderStatus(order, config);
            int status = queryResult.get("status");
            if (status == 0) {
                throw new RuntimeException("查询订单支付状态异常");
            }
            if (status == Constant.OrderStatus.PAYING) {
                throw new RuntimeException("该订单正在支付中");
            }
            if (status == Constant.OrderStatus.PAID) {
                ((OrderManager) AopContext.currentProxy()).paid(order.getNumber(), queryResult.get("transaction_id"));
                return true;
            }
        }
        return false;
    }

    @Retryable(value = { RuntimeException.class, Exception.class }, backoff = @Backoff(delay = 2000, multiplier = 2))
    public Result<?> queryOrderStatus(Order order, TWxConfig config) {
        Result<TreeMap<String, Object>> result = wxManager.queryOrder(order, config);
        if (!result.isSuccess()) {
            throw new RuntimeException(result.getMsg());
        }
        TreeMap<String, Object> map = result.getValue();
        String tradeState = MapUtils.getString(map, "trade_state", "NOTPAY");
        switch (tradeState) {
            case "SUCCESS":
                return Result.ok(map).put("transaction_id", map.get("transaction_id")).put("status", Constant.OrderStatus.PAID);
            case "NOTPAY":
            case "CLOSED":
            case "REVOKED":
            case "PAYERROR":
            case "REFUND":
                return Result.ok().put("status", Constant.OrderStatus.PENDING_PAY);
            case "USERPAYING":
                return Result.ok().put("status", Constant.OrderStatus.PAYING);
            default:
                return Result.ok().put("status", 0);
        }
    }

    @Locks("#{#order.number}")
    public Result<?> pay(Order order, TWxConfig config, TMember member, HttpServletRequest request) {
        Result<TreeMap<String, Object>> result = wxManager.pay(order, config, member.getOpenid(), ServletUtil.getClientIP(request));
        Assert.isTrue(result.isSuccess(), result.getMsg());
        Map<String, Object> map = result.getValue();
        String nonceStr = RandomUtil.randomString(15);
        long timestamp = System.currentTimeMillis() / 1000;
        String prepayId = MapUtils.getString(map, "prepay_id");
        String sign = WxManager.sign((TreeMap<String, Object>) MapUtil.builder(new TreeMap<String, Object>())
                .put("appId", config.getAppId())
                .put("nonceStr", nonceStr)
                .put("package", "prepay_id=" + prepayId)
                .put("signType", "MD5")
                .put("timeStamp", timestamp)
                .build(), config.getMchKey());
        return Result.ok(new WxPayResult()
                .setAppId(config.getAppId())
                .setPrepayId(prepayId)
                .setCodeUrl(MapUtils.getString(map, "code_url"))
                .setNonceStr(nonceStr)
                .setTimestamp(timestamp)
                .setSign(sign)
        );
    }

    @Retryable(value = { RuntimeException.class, Exception.class }, backoff = @Backoff(delay = 2000, multiplier = 2))
    public Result<?> queryRefundStatus(TRefundOrder refundOrder, TWxConfig config) {
        Result<TreeMap<String, Object>> result = wxManager.queryRefund(refundOrder, config);
        if (!result.isSuccess()) {
            throw new RuntimeException(result.getMsg());
        }
        TreeMap<String, Object> map = result.getValue();
        String refundStatus = MapUtils.getString(map, "refund_status_0", "REFUNDCLOSE");
        switch (refundStatus) {
            case "SUCCESS":
                return Result.ok(map).put("refund_id", map.get("refund_id_0")).put("status", Constant.RefundOrderStatus.SUCCESS);
            case "REFUNDCLOSE":
                return Result.ok().put("status", Constant.RefundOrderStatus.PENDING);
            case "PROCESSING":
            case "CHANGE":
                return Result.ok().put("status", Constant.RefundOrderStatus.PENDING_MONEY);
            default:
                return Result.ok().put("status", 0);
        }
    }

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @Locks("#{#number}")
    public void paid(long number, String transactionId) {
        TOrder order = orderMapper.selectOne(new TOrder().setOrderNumber(number));
        checkPaid(order);
        TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
        Assert.notNull(merchant, "商户不存在");
        orderMapper.updateByPrimaryKeySelective(new TOrder()
                .setId(order.getId())
                .setStatus(Constant.OrderStatus.PAID)
                .setTransactionId(transactionId)
                .setPayTime(new Date())
        );

        // 调用平台支付
        Result<?> result = platformManager.orderApi(merchant).payment(merchant, order);
        if (!result.isSuccess()) {
            Result<Boolean> retryPayment = platformManager.orderApi(merchant).canRetryPayment(merchant, order, result);
            if (retryPayment.isSuccess() && retryPayment.getValue()) {
                result = platformManager.orderApi(merchant).retryPayment(merchant, order);
            }
        }
        // 退款
        if (!result.isSuccess()) {
            orderMapper.updateByPrimaryKeySelective(new TOrder().setId(order.getId()).setStatus(Constant.OrderStatus.CANCEL).setLog(Util.cutStr(result.getMsg(), 5000)));
            TRefundOrder refundOrder = refundOrderMapper.selectOne(new TRefundOrder().setNumber(order.getNumber()));
            if (refundOrder == null) {
                refundOrder = new TRefundOrder()
                        .setCreateTime(new Date())
                        .setFee(0)
                        .setOriginalFee(0)
                        .setItemNumber(order.getItemNumber())
                        .setOrderNumber(order.getOrderNumber())
                        .setMemberId(order.getMemberId())
                        .setMerchantId(merchant.getId())
                        .setPayAmount(order.getMoney())
                        .setMoney(order.getMoney())
                        .setNumber(order.getNumber())
                        .setQuantity(order.getQuantity())
                        .setCause("平台下单失败自动退款")
                        .setStatus(Constant.RefundOrderStatus.PENDING);
                refundOrderMapper.insertSelective(refundOrder);
            }
            refundConfirm(merchant, refundOrder.getNumber());
            return;
        }

        sendWxTemplate(order.getMemberId(), systemConfig.getOrderPaidWxTemplate(), merchant.getAccessId(), MapUtil.builder(new HashMap<String, Object>(5))
                .put("first", "您的订单下单成功！")
                .put("keyword1", DateUtil.formatDateTime(order.getCreateTime()))
                .put("keyword2", order.getSubName())
                .put("keyword3", order.getOrderNumber())
                .put("remark", String.format("联系人：%s，联系电话：%s，预订数量：%d，祝您使用愉快！", order.getContact(), order.getContactPhone(), order.getQuantity()))
                .build()
        );
    }

    protected void checkPaid(Order order) {
        if (order.getStatus() == Constant.OrderStatus.PAID) {
            throw new RuntimeException("订单已支付");
        }
        if (order.getStatus() != Constant.OrderStatus.PAYING) {
            throw new RuntimeException("订单状态不正确: " + order.getStatus());
        }
    }

    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> refundConfirm(TMerchant merchant, long number) {
        TRefundOrder refundOrder = refundOrderMapper.selectOne(new TRefundOrder().setMerchantId(merchant.getId()).setNumber(number));
        Assert.notNull(refundOrder, "退订订单不存在");
        if (refundOrder.getStatus() == Constant.RefundOrderStatus.SUCCESS) {
            return Result.fail(Result.HAS_REFUND, "该订单已成功退款");
        }
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("未配置微信支付信息,请联系商家");
        }
        if (refundOrder.getStatus() == Constant.RefundOrderStatus.PENDING_MONEY) {
            Result<?> result = queryRefundStatus(refundOrder, config);
            if (!result.isSuccess()) {
                return result;
            }
            int status = result.get("status");
            if (status == 0) {
                return Result.fail("查询退款状态异常");
            }
            if (status == Constant.RefundOrderStatus.PENDING_MONEY) {
                return Result.fail("该订单正在退款中");
            }
            if (status == Constant.RefundOrderStatus.SUCCESS) {
                refundSuccess(number, result.get("refund_id"));
                return Result.ok();
            }
        }
        int ret = refundOrderMapper.updateByPrimaryKeySelective(new TRefundOrder().setId(refundOrder.getId()).setStatus(Constant.RefundOrderStatus.PENDING_MONEY));
        if (ret <= 0) {
            return Result.fail();
        }
        Result<TreeMap<String, Object>> result = wxManager.refund(refundOrder, config);
        if (!result.isSuccess()) {
            throw new RuntimeException("退款发起失败");
        }
        TreeMap<String, Object> map = result.getValue();
        String refundId = MapUtils.getString(map, "refund_id");
        refundSuccess(number, refundId);
        return result;
    }

    private void refundSuccess(long number, String refundId) {
        TRefundOrder refundOrder = refundOrderMapper.selectOne(new TRefundOrder().setNumber(number));
        Assert.notNull(refundOrder, "退订订单不存在");
        Assert.isTrue(StrUtil.isNotBlank(refundId), "退款ID错误");
        if (refundOrder.getStatus() == Constant.RefundOrderStatus.SUCCESS) {
            throw new RuntimeException("订单已退款");
        }
        refundOrder.setRefundId(refundId);
        refundOrder.setStatus(Constant.RefundOrderStatus.SUCCESS);
        refundOrderMapper.updateByPrimaryKeySelective(refundOrder);
    }

    public List<PlatformOrderInfo> list(TMerchant merchant, SearchOrder search, Integer pageNum, Integer pageSize) {
        List<TOrder> orders;
        Page<PlatformOrderInfo> page = null;
        if (pageNum != null && pageSize != null) {
            orders = PageHelper.startPage(pageNum, pageSize).doSelectPage(() -> orderMapper.search(search));
            page = new Page<>(pageNum, pageSize, false);
        } else {
            orders = orderMapper.search(search);
        }

        List<PlatformOrderInfo> list = Collections.emptyList();
        if (!CollectionUtils.isEmpty(orders)) {
            List<PlatformOrderInfo> orderInfos = platformManager.orderApi(merchant).batchInfo(merchant, orders);
            Map<Long, PlatformOrderInfo> orderMap = orderInfos.stream().collect(Collectors.toMap(PlatformOrderInfo::getNumber, Function.identity()));
            list = orders.stream().map(order -> orderMap.get(order.getOrderNumber())
                    .put(TOrder::getProductName, order)
                    .put(TOrder::getSubName, order)
                    .put(TOrder::getProductType, order)
                    .put(TOrder::getType, order)
                    .put(TOrder::getDays, order)
                    .put(TOrder::getQuantity, order)
                    .put(TOrder::getContact, order)
                    .put(TOrder::getContactPhone, order)
                    .put(TOrder::getCreateTime, order)
                    .put(TOrder::getMoney, order)
                    .put(TOrder::getStatus, order)
                    .put(TOrder::getLog, order)
                    .<PlatformOrderInfo, TOrder>put(TOrder::getBooking, order))
                    .collect(Collectors.toList());
        }

        if (orders instanceof Page && page != null) {
            page.addAll(list);
            page.setTotal(((Page) orders).getTotal());
            return page;
        }
        return list;
    }

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    @Locks("#{#order.number}")
    public void rebate(TOrder order) {
        Integer from = order.getFromMember();
        if (from != null && from != order.getMemberId().intValue() && Objects.equals(order.getIsRebate(), false)) {
            TMember member = memberMapper.selectByPrimaryKey(order.getMemberId());
            memberManager.balanceChange(from, Constant.MemberBillType.REBATE, order.getId(), order.getRebate());
            orderMapper.updateByPrimaryKeySelective(new TOrder().setId(order.getId()).setIsRebate(true));
            if (member.getPid() == null || member.getPid() == 0) {
                memberMapper.updateByVersion(member.setPid(from));
            }
            TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
            sendWxTemplate(from, systemConfig.getOrderRebateWxTemplate(), merchant.getAccessId(), MapUtil.builder(new HashMap<String, Object>(5))
                    .put("first", "您有分销佣金到账啦！")
                    .put("keyword1", order.getOrderNumber())
                    .put("keyword2", NumberUtil.div(order.getMoney().intValue(), 100, 2))
                    .put("keyword3", NumberUtil.div(order.getRebate().intValue(), 100, 2))
                    .put("keyword4", DateUtil.formatDateTime(order.getCreateTime()))
                    .put("remark", "感谢您的分享，继续加油！")
                    .build()
            );
        }
    }

    public boolean sendWxTemplate(TemplateMsg msg, TWxConfig config) {
        try {
            if (config == null) {
                log.warn("发送微信通知失败: 未配置微信信息");
                return false;
            }
            String body = wxManager.sendTemplateMsg(msg, config);
            log.debug("发送微信通知返回: {}", body);
            return true;
        } catch (Exception e) {
            log.warn("发送微信通知失败", e);
            return false;
        }
    }

    public boolean sendWxTemplate(Integer memberId, String templateId, String accessId, Map<String, ?> data) {
        try {
            TMember member = memberMapper.selectByPrimaryKey(memberId);
            if (member == null) {
                log.warn("发送微信通知失败: 会员不存在");
                return false;
            }
            if (StrUtil.isBlank(member.getOpenid())) {
                log.warn("发送微信通知失败: 会员Openid为null");
                return false;
            }
            TWxConfig config = wxManager.getConfig(accessId);
            return sendWxTemplate(TemplateMsg.builder(member.getOpenid(), templateId).mapData(data), config);
        } catch (Exception e) {
            log.warn("发送微信通知失败", e);
            return false;
        }
    }
}
