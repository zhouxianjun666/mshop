package com.all580.mshop.manager;

import com.all580.mshop.Constant;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMemberBills;
import com.all580.mshop.mapper.TMemberBillsMapper;
import com.all580.mshop.mapper.TMemberMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/13 14:55
 */
@Component
public class MemberManager {
    @Resource
    private TMemberBillsMapper memberBillsMapper;
    @Resource
    private TMemberMapper memberMapper;

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public TMember balanceChange(int id, int type, int refId, int money) {
        TMember member = memberMapper.selectByPrimaryKey(id);
        int newer = member.getBalance() + money;
        Assert.isTrue(newer >= 0, "余额不足");
        memberBillsMapper.insertSelective(new TMemberBills()
                .setCreateTime(new Date())
                .setMemberId(id)
                .setRefId(refId)
                .setType(type)
                .setOld(member.getBalance())
                .setNewer(newer)
        );
        TMember save = new TMember().setId(id).setVersion(member.getVersion()).setBalance(money);
        if (type == Constant.MemberBillType.REBATE) {
            save.setRebate(Math.abs(money));
            member.setRebate(member.getRebate() + Math.abs(money));
        }
        memberMapper.updateByVersion(save, m -> memberMapper.updateAppend(m));
        return member.setBalance(newer);
    }
}
