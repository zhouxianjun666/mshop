package com.all580.mshop.manager;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.OrderTmp;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.SystemConfig;
import com.all580.mshop.entity.*;
import com.all580.mshop.mapper.TGroupOrderMapper;
import com.all580.mshop.mapper.TGroupOrderMemberMapper;
import com.all580.mshop.mapper.TGroupProductMapper;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.vo.CreateGroupOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/29 10:24
 */
@Component
public class GroupOrderManager extends OrderManager {
    @Resource
    private TGroupOrderMemberMapper groupOrderMemberMapper;
    @Resource
    private TGroupProductMapper groupProductMapper;
    @Resource
    private TGroupOrderMapper groupOrderMapper;
    @Resource
    private TMerchantMapper merchantMapper;
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private PlatformManager platformManager;
    @Autowired
    private SystemConfig systemConfig;

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> create(TMember member, TGroupProduct product, CreateGroupOrderVo vo, boolean single, Long number) {
        TGroupOrder order = getGroupOrder(member, product, number, single);
        List<TGroupOrderMember> members = groupOrderMemberMapper.select(new TGroupOrderMember().setGroupId(order.getId()).setMemberId(member.getId()));
        if (!CollectionUtils.isEmpty(members)) {
            List<TGroupOrderMember> cancels = members.stream().filter(m -> m.getStatus() == Constant.OrderStatus.CANCEL).collect(Collectors.toList());
            if (cancels.size() < members.size()) {
                throw new RuntimeException("您已拼了该团,请勿重复拼团");
            }
            if (cancels.size() > 3) {
                throw new RuntimeException("您多次取消本团,不能再次拼入本团");
            }
        }
        TGroupOrderMember groupOrderMember = new TGroupOrderMember()
                .setGroupId(order.getId())
                .setMemberId(member.getId())
                .setBooking(vo.getBooking())
                .setMerchantId(member.getMerchantId())
                .setNumber(snowflake.nextId())
                .setDays(vo.getDays())
                .setQuantity(vo.getQuantity())
                .setName(vo.getName())
                .setPhone(vo.getPhone())
                .setSid(vo.getSid())
                .setSex(vo.getSex())
                .setStatus(Constant.OrderStatus.PENDING_PAY)
                .setMoney((single ? product.getPrice() : product.getGroupPrice()) * vo.getQuantity() * vo.getDays())
                .setCreateTime(new Date());
        groupOrderMemberMapper.insertSelective(groupOrderMember);
        return Result.ok(groupOrderMember).put("productName", product.getProductName()).put("subName", product.getSubName());
    }

    private TGroupOrder getGroupOrder(TMember member, TGroupProduct product, Long number, boolean single) {
        TGroupOrder groupOrder;
        if (number != null && number > 0 && !single) {
            groupOrder = groupOrderMapper.selectOne(new TGroupOrder().setNumber(number).setSingle(false).setStatus(Constant.GroupOrderStatus.OPEN));
            Assert.notNull(groupOrder, "拼团号错误");
            if (groupOrder.getNum() >= product.getNum()) {
                throw new RuntimeException("该团已满");
            }
            if (new Date().after(DateUtil.offsetHour(groupOrder.getCreateTime(), systemConfig.getGroupExpiredHours()))) {
                throw new RuntimeException("该团已过期");
            }
            int ret = groupOrderMapper.updateNum(groupOrder.getId(), 1, product.getNum());
            if (ret <= 0) {
                throw new RuntimeException("该团已满");
            }
        } else {
            groupOrder = new TGroupOrder()
                    .setCreateTime(new Date())
                    .setMemberId(member.getId())
                    .setMerchantId(member.getMerchantId())
                    .setProductId(product.getProductId())
                    .setProductName(product.getProductName())
                    .setSubCode(product.getSubCode())
                    .setSubName(product.getSubName())
                    .setNumber(snowflake.nextId())
                    .setNum(1)
                    .setStatus(Constant.GroupOrderStatus.CREATE)
                    .setSingle(single);
            groupOrderMapper.insertSelective(groupOrder);
        }
        return groupOrder;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void paid(long number, String transactionId) {
        TGroupOrderMember order = groupOrderMemberMapper.selectOne(new TGroupOrderMember().setNumber(number));
        checkPaid(order);
        TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
        Assert.notNull(merchant, "商户不存在");
        order.setStatus(Constant.OrderStatus.PAID);
        order.setTransactionId(transactionId);
        order.setPayTime(new Date());
        groupOrderMemberMapper.updateByPrimaryKeySelective(order);

        TGroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(order.getGroupId());
        Assert.notNull(groupOrder, "拼团单不存在");

        if (groupOrder.getMemberId() == order.getMemberId().intValue()) {
            groupOrderMapper.updateByPrimaryKey(groupOrder.setStatus(Constant.GroupOrderStatus.OPEN));
        }

        paidToPlatform(merchant, groupOrder.getNumber());
    }

    @Locks("#{#number}")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void paidToPlatform(TMerchant merchant, long number) {
        TGroupOrder groupOrder = groupOrderMapper.selectOne(new TGroupOrder().setNumber(number).setStatus(Constant.GroupOrderStatus.OPEN));
        Assert.notNull(groupOrder, "拼团单不存在");
        if (merchant == null) {
            merchant = merchantMapper.selectByPrimaryKey(groupOrder.getMerchantId());
        }
        List<TGroupOrderMember> memberList = groupOrderMemberMapper.selectByExample(Example.builder(TGroupOrderMember.class)
                .where(WeekendSqls.<TGroupOrderMember>custom()
                        .andEqualTo(TGroupOrderMember::getGroupId, groupOrder.getId())
                        .andNotEqualTo(TGroupOrderMember::getStatus, Constant.OrderStatus.CANCEL)
                )
                .build()
        );
        boolean allPay = memberList.stream().allMatch(m -> m.getStatus() == Constant.OrderStatus.PAID);
        TGroupProduct product = groupProductMapper.selectOne((TGroupProduct) new TGroupProduct().setMerchantId(merchant.getId()).setSubCode(groupOrder.getSubCode()));
        if (!allPay || (groupOrder.getNum() < product.getNum() && !groupOrder.getSingle())) {
            return;
        }
        Constant.STRING_CACHE.put("PAID:PLATFORM:" + groupOrder.getId(), String.valueOf(number), TimeUnit.MINUTES.toMillis(20));
        TMerchant finalMerchant = merchant;
        Set<String> numbers = memberList.stream().map(m -> {
            Map<String, Object> params = new HashMap<>(3);
            params.put("from", 351);
            params.put("shipping", new HashMap<String, Object>(3) {{
                put("name", m.getName());
                put("phone", m.getPhone());
            }});
            params.put("items", Collections.singletonList(new HashMap<String, Object>(5) {{
                put("visitor", Collections.singletonList(new HashMap<String, Object>(4) {{
                    put("name", m.getName());
                    put("phone", m.getPhone());
                    put("sid", m.getSid());
                    put("quantity", m.getQuantity());
                    put("sex", m.getSex());
                }}));
                put("quantity", m.getQuantity());
                put("product_sub_code", groupOrder.getSubCode());
                put("start", DateUtil.formatDateTime(m.getBooking()));
                put("days", m.getDays());
            }}));
            Result<?> result = platformManager.orderApi(finalMerchant).create(finalMerchant, params, product);
            if (!result.isSuccess()) {
                throw new RuntimeException(result.getMsg());
            }
            OrderTmp orderTmp = platformManager.orderApi(finalMerchant).transformOrder(finalMerchant, params, result, true);
            Assert.notNull(orderTmp, "下单失败");
            m.setOrderNumber(orderTmp.getNumber()).setItemNumber(orderTmp.getItemNumber());
            groupOrderMemberMapper.updateByPrimaryKeySelective(m);
            return orderTmp;
        }).map(OrderTmp::getNumber).map(Objects::toString).collect(Collectors.toSet());

        groupOrderMapper.updateByPrimaryKey(groupOrder.setStatus(Constant.GroupOrderStatus.COMPLETE));
        Result<?> result = platformManager.orderApi(merchant).batchPayment(merchant, numbers);
        Assert.isTrue(result.isSuccess(), result.getMsg());
    }

    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public void cancelOrderMember(TGroupOrderMember order) {
        groupOrderMemberMapper.updateByPrimaryKeySelective(order.setStatus(Constant.OrderStatus.CANCEL).setCancelTime(new Date()));
        int ret = groupOrderMapper.updateNum(order.getGroupId(), -1, null);
        if (ret < 0) {
            throw new RuntimeException("该拼团单无法再减成员");
        }
    }
}
