package com.all580.mshop.manager;

import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.platform.PlatformOrderApi;
import com.all580.mshop.platform.PlatformProductApi;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 9:29
 */
@Component
public class PlatformManager implements ApplicationContextAware {
    @Setter
    private ApplicationContext applicationContext;
    private Map<String, PlatformProductApi> productApiMap;
    private Map<String, PlatformOrderApi> orderApiMap;

    @PostConstruct
    public void init() {
        productApiMap = applicationContext.getBeansOfType(PlatformProductApi.class).values().stream().collect(
                Collectors.toMap(PlatformProductApi::type, Function.identity())
        );
        orderApiMap = applicationContext.getBeansOfType(PlatformOrderApi.class).values().stream().collect(
                Collectors.toMap(PlatformOrderApi::type, Function.identity())
        );
    }

    public PlatformProductApi productApi(TMerchant merchant) {
        return productApiMap.get(merchant.getPlatform());
    }
    public PlatformOrderApi orderApi(TMerchant merchant) {
        return orderApiMap.get(merchant.getPlatform());
    }
}
