package com.all580.mshop.manager;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.*;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.config.OkHttpConfig;
import com.all580.mshop.dto.Order;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.SystemConfig;
import com.all580.mshop.dto.TemplateMsg;
import com.all580.mshop.entity.TRefundOrder;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.mapper.TWxConfigMapper;
import com.all580.mshop.util.KeyLock;
import com.all580.mshop.util.Util;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/10 13:45
 */
@Component
@Slf4j
public class WxManager {
    @Autowired
    private OkHttpClient okHttpClient;
    @Value("${wx.center.access-id:}")
    private String accessId;
    @Value("${wx.center.access-key:}")
    private String accessKey;
    @Value("${wx.center.url:}")
    private String url;
    @Value("${wishing:哈皮一下}")
    private String wishing;
    @Autowired
    private SystemConfig systemConfig;
    @Resource
    private TWxConfigMapper wxConfigMapper;

    private KeyLock<String> lock = new KeyLock<>();
    private static final int READ_PACK_MAX = 20000;
    private static final int READ_PACK_MIN = 100;
    public static final String SUCCESS = "SUCCESS";
    private Map<String, OkHttpClient> wxHttpClientMap = new HashMap<>(1);
    private Map<String, TWxConfig> wxConfigMap = new HashMap<>(1);

    /**
     * 获取微信access_token
     * 如果配置了 wx.center.access-id && wx.center.access-key && wx.center.url
     * 则从PHP中控获取否则从自己获取
     * @return
     */
    @SneakyThrows
    public String getAccessToken(TWxConfig config) {
        if (config.getCenter()) {
            if (StrUtil.isBlank(accessId) || StrUtil.isBlank(accessKey) || StrUtil.isBlank(url)) {
                throw new RuntimeException("该微信为中控获取，但未配置中控信息");
            }
            return getAccessTokenByCenter();
        }
        return getAccessTokenBySelf(config);
    }

    /**
     * 获取微信jsticket
     * 如果配置了 wx.center.access-id && wx.center.access-key && wx.center.url
     * 则从PHP中控获取否则从自己获取
     * @return
     */
    @SneakyThrows
    public String getTicket(TWxConfig config) {
        if (config.getCenter()) {
            if (StrUtil.isBlank(accessId) || StrUtil.isBlank(accessKey) || StrUtil.isBlank(url)) {
                throw new RuntimeException("该微信为中控获取，但未配置中控信息");
            }
            return getJsTicketByCenter();
        }
        return getJsTicketBySelf(config);
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    public Map<String, Object> getOpenid(String appid, String secret, String code) {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token";
        url = HttpUtil.urlWithForm(url, MapUtil.builder(new HashMap<String, Object>(4))
                .put("appid", appid)
                .put("secret", secret)
                .put("code", code)
                .put("grant_type", "authorization_code")
                .build(), CharsetUtil.CHARSET_UTF_8, true);

        return request(url);
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    public Map<String, Object> getInfo(String token, String openid) {
        String url = "https://api.weixin.qq.com/sns/userinfo";
        url = HttpUtil.urlWithForm(url, MapUtil.builder(new HashMap<String, Object>(3))
                .put("access_token", token)
                .put("openid", openid)
                .put("lang", "zh_CN")
                .build(), CharsetUtil.CHARSET_UTF_8, true);

        return request(url);
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> sendRedpack(String billNo, String openid, int money, String actName, String remark, String merchantName, TWxConfig config) {
        String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("nonce_str", RandomUtil.randomString(32));
        params.put("mch_billno", billNo);
        params.put("mch_id", config.getMchId());
        params.put("wxappid", config.getAppId());
        params.put("send_name", merchantName);
        params.put("re_openid", openid);
        params.put("total_amount", money);
        params.put("total_num", 1);
        params.put("wishing", wishing);
        params.put("client_ip", Optional.ofNullable(NetUtil.getLocalhostStr()).orElse("127.0.0.1"));
        params.put("act_name", actName);
        params.put("remark", remark);
        if (money > READ_PACK_MAX || money < READ_PACK_MIN) {
            params.put("scene_id", "PRODUCT_5");
        }
        return request(params, url, config, "现金红包");
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> queryRedpack(String billNo, TWxConfig config) {
        String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gethbinfo";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("nonce_str", RandomUtil.randomString(32));
        params.put("mch_billno", billNo);
        params.put("mch_id", config.getMchId());
        params.put("appid", config.getAppId());
        params.put("bill_type", "MCHT");
        return request(params, url, config, "现金红包查询", "NOT_FOUND");
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> pay(Order order, TWxConfig config, String openid, String ip, String type) {
        String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("appid", config.getAppId());
        params.put("mch_id", config.getMchId());
        params.put("nonce_str", RandomUtil.randomString(32));
        params.put("body", Util.cutStr(order.getProductName() + "-" + order.getSubName(), 124));
        params.put("attach", order.getAttach());
        params.put("out_trade_no", order.getNumber());
        params.put("total_fee", order.getMoney());
        params.put("spbill_create_ip", StrUtil.emptyToDefault(ip, NetUtil.getLocalhostStr()));
        params.put("notify_url", systemConfig.getDomain() + "/notify/wx/pay");
        params.put("trade_type", StrUtil.emptyToDefault(type, "JSAPI"));
        params.put("product_id", order.getSubCode());
        params.put("openid", openid);
        return request(params, url, config, "支付");
    }

    public Result<TreeMap<String, Object>> pay(Order order, TWxConfig config, String openid, String ip) {
        return pay(order, config, openid, ip, null);
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> queryOrder(Order order, TWxConfig config) {
        String url = "https://api.mch.weixin.qq.com/pay/orderquery";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("appid", config.getAppId());
        params.put("mch_id", config.getMchId());
        params.put("transaction_id", order.getTransactionId());
        params.put("out_trade_no", order.getNumber());
        params.put("nonce_str", RandomUtil.randomString(32));
        return request(params, url, config, "订单查询", "ORDERNOTEXIST");
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> refund(TRefundOrder refundOrder, TWxConfig config) {
        String url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("appid", config.getAppId());
        params.put("mch_id", config.getMchId());
        params.put("nonce_str", RandomUtil.randomString(32));
        params.put("out_trade_no", refundOrder.getOrderNumber());
        params.put("out_refund_no", refundOrder.getNumber());
        params.put("total_fee", refundOrder.getPayAmount());
        params.put("refund_fee", refundOrder.getMoney());
        return request(params, url, config, "订单申请退款");
    }

    @SneakyThrows
    public Result<TreeMap<String, Object>> queryRefund(TRefundOrder refundOrder, TWxConfig config) {
        String url = "https://api.mch.weixin.qq.com/pay/refundquery";
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("appid", config.getAppId());
        params.put("mch_id", config.getMchId());
        params.put("nonce_str", RandomUtil.randomString(32));
        params.put("out_trade_no", refundOrder.getOrderNumber());
        params.put("out_refund_no", refundOrder.getNumber());
        params.put("refund_id", refundOrder.getRefundId());
        return request(params, url, config, "订单退款查询", "REFUNDNOTEXIST");
    }

    @Retryable(value = Exception.class, backoff = @Backoff(delay = 2000, multiplier = 2))
    public JSONObject request(String url) throws Exception {
        Request request = new Request.Builder().url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return JSONUtil.parseObj(response.body().string());
            }
            return null;
        }
    }

    /**
     * 发送微信模板消息
     * @param msg
     * @return
     */
    @SneakyThrows
    public String sendTemplateMsg(TemplateMsg msg, TWxConfig config) {
        Assert.notNull(msg.getTemplate_id(), "微信模板ID不能为空");
        String token = getAccessToken(config);
        Assert.notNull(token, "获取access_token失败");
        String json = JSONUtil.toJsonStr(msg);
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";
        Request request = new Request.Builder().url(url + token).post(RequestBody.create(OkHttpConfig.JSON, json)).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return response.body().string();
            }
            return null;
        }
    }

    public Result<TreeMap<String, Object>> request(TreeMap<String, Object> params, String url, TWxConfig config, String name, String... trueCodes) throws IOException {
        Util.removeEmpty(params);
        params.put("sign", sign(params, config.getMchKey()));
        String xml = XmlUtil.mapToXmlStr(params, "xml");
        log.info("微信 {} 请求:{}", name, xml);

        Request request = new Request.Builder().url(url).post(RequestBody.create(OkHttpConfig.XML, xml)).build();
        try (Response response = getOkHttpClient(config).newCall(request).execute()) {
            log.info("微信 {} 返回: {}", name, response);
            if (response.isSuccessful() && response.body() != null) {
                String string = response.body().string();
                log.info("微信 {} 结果: {}", name, string);
                return wxResult(string, config, trueCodes);
            }
        }
        return Result.fail();
    }

    public TWxConfig getConfig(String mch) {
        synchronized (mch.intern()) {
            return wxConfigMap.computeIfAbsent(mch, key -> wxConfigMapper.selectByMerchantAccessId(key));
        }
    }

    public TWxConfig refreshConfig(String mch) {
        synchronized (mch.intern()) {
            return wxConfigMap.compute(mch, (key, old) -> wxConfigMapper.selectByMerchantAccessId(key));
        }
    }

    private OkHttpClient getOkHttpClient(TWxConfig config) {
        synchronized (config) {
            return wxHttpClientMap.computeIfAbsent(config.getAppId(), key -> OkHttpConfig.wxPayClient(config));
        }
    }

    private String getAccessTokenByCenter() throws Exception {
        Result<JSONObject> result = requestCenter("getAccToken");
        if (!result.isSuccess()) {
            return null;
        }
        return result.getValue().getStr("access_token");
    }

    private String getAccessTokenBySelf(TWxConfig config) throws Exception {
        String key = config.getAppId() + "_TOKEN";
        String token = Constant.STRING_CACHE.get(key, false);
        if (token != null) {
            return token;
        }

        try {
            lock.lock(key);
            String url = "https://api.weixin.qq.com/cgi-bin/token";
            url = HttpUtil.urlWithForm(url, MapUtil.builder(new HashMap<String, Object>(3))
                    .put("appid", config.getAppId())
                    .put("secret", config.getAppSecret())
                    .put("grant_type", "client_credential")
                    .build(), CharsetUtil.CHARSET_UTF_8, true);

            JSONObject json = request(url);
            log.debug("获取微信token返回: {}", json);
            if (json == null) {
                return null;
            }
            token = json.getStr("access_token");
            if (StrUtil.isBlank(token)) {
                return null;
            }
            Constant.STRING_CACHE.put(key, token, TimeUnit.MINUTES.toMillis(110));
            return token;
        } finally {
            lock.unlock(key);
        }
    }

    private String getJsTicketByCenter() throws Exception {
        Result<JSONObject> result = requestCenter("getJsTicket");
        if (!result.isSuccess()) {
            return null;
        }
        return result.getValue().getStr("ticket");
    }

    private String getJsTicketBySelf(TWxConfig config) throws Exception {
        String token = getAccessTokenBySelf(config);
        if (StrUtil.isBlank(token)) {
            return null;
        }
        return getJsTicketBySelf(config, token);
    }

    private String getJsTicketBySelf(TWxConfig config, String token) throws Exception {
        String key = config.getAppId() + "_TICKET";
        String ticket = Constant.STRING_CACHE.get(key, false);
        if (ticket != null) {
            return ticket;
        }

        try {
            lock.lock(key);
            String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
            url = HttpUtil.urlWithForm(url, MapUtil.builder(new HashMap<String, Object>(2))
                    .put("access_token", token)
                    .put("type", "jsapi")
                    .build(), CharsetUtil.CHARSET_UTF_8, true);

            JSONObject json = request(url);
            log.debug("获取微信ticket返回: {}", json);
            if (json == null) {
                return null;
            }
            ticket = json.getStr("ticket");
            if (StrUtil.isBlank(ticket)) {
                return null;
            }
            Constant.STRING_CACHE.put(key, ticket, TimeUnit.MINUTES.toMillis(110));
            return ticket;
        } finally {
            lock.unlock(key);
        }
    }

    private Result<JSONObject> requestCenter(String method) throws Exception {
        String json = JSONUtil.toJsonStr(Collections.singletonMap("access_id", accessId));
        String sign = DigestUtil.md5Hex(json + accessKey);
        String url = String.format("%s/Wxapi/%s?wxhost=wxpay&sign=%s", this.url, method, sign);
        log.info("调用 微信中控 请求: {}, accessId: {}, accessKey: {}", url, accessId, accessKey);
        Request request = new Request.Builder().url(url).post(RequestBody.create(OkHttpConfig.JSON, json)).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            log.info("调用 微信中控 返回: {}", response);
            if (response.isSuccessful() && response.body() != null) {
                String string = response.body().string();
                log.info("调用 微信中控 结果: {}", string);
                return Result.ok(JSONUtil.parseObj(string));
            }
            return Result.<JSONObject>fail().put("response", response);
        }
    }

    public static Result<TreeMap<String, Object>> wxResult(String string, TWxConfig config, String... trueCodes) {
        TreeMap<String, Object> map = (TreeMap<String, Object>) XmlUtil.xmlToMap(string, new TreeMap<>());
        String returnCode = MapUtils.getString(map, "return_code");
        if (SUCCESS.equals(returnCode) && config != null) {
            if (!verify(map, config)) {
                return Result.<TreeMap<String, Object>>fail("返回签名错误").value(map);
            }
        }
        String returnMsg = MapUtils.getString(map, "return_msg");
        String resultCode = MapUtils.getString(map, "result_code");
        String resultMsg = MapUtils.getString(map, "err_code_des");
        String errorCode = MapUtils.getString(map, "err_code");
        boolean success = SUCCESS.equals(returnCode) && (returnCode.equals(resultCode) || Arrays.asList(trueCodes).contains(errorCode));
        return Result.<TreeMap<String, Object>>builder()
                .code(success ? Result.SUCCESS : Result.FAIL)
                .msg(StrUtil.emptyToDefault(resultMsg, returnMsg))
                .value(map)
                .build();
    }

    public static Result<TreeMap<String, Object>> wxResult(String string) {
        return wxResult(string, null);
    }

    public static boolean verify(TreeMap<String, Object> map, TWxConfig config) {
        String resultSign = (String) map.remove("sign");
        if (resultSign == null) {
            return true;
        }
        String sign = sign(map, config.getMchKey());
        return sign.equals(resultSign);
    }

    public static String sign(TreeMap<String, Object> params, String key) {
        Util.removeEmpty(params);
        String query = HttpUtil.toParams(params);
        String sign = DigestUtil.md5Hex(HttpUtil.decode(query, StandardCharsets.UTF_8) + "&key=" + key).toUpperCase();
        log.info("微信 签名: query:{}, key:{}, sign:{}", query, key, sign);
        return sign;
    }
}
