package com.all580.mshop.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import com.all580.mshop.entity.TWxConfig;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/18 9:55
 */
@Slf4j
public class Util {
    public static <T> void nullToDefault(Map<?, T> map, T defaultVal) {
        if (!CollectionUtils.isEmpty(map)) {
            map.entrySet().stream().filter(entry -> entry.getValue() == null).forEach(entry -> entry.setValue(defaultVal));
            map.values().stream().filter(val -> val instanceof Map).forEach(v -> Util.nullToDefault((Map<?, T>) v, defaultVal));
        }
    }

    public static void removeEmpty(Map<?, ?> map) {
        if (!CollectionUtils.isEmpty(map)) {
            map.entrySet().stream().filter(entry -> StrUtil.isEmptyIfStr(entry.getValue())).collect(Collectors.toList()).forEach(entry -> map.remove(entry.getKey()));
            map.values().stream().filter(val -> val instanceof Map).forEach(v -> Util.removeEmpty((Map<?, ?>) v));
        }
    }

    public static String hide(String str, int count, char c) {
        int t = str.length() - count;
        return StrUtil.replace(str, t / 2, t / 2 + count, c);
    }

    public static <T> T readJson(JsonPath path, Object json) {
        try {
            return path.read(json);
        } catch (Exception e) {
            log.warn("读取jsonpath异常", e);
            return null;
        }
    }

    public static String cutStr(String strs, int length) {
        int sum = 0;
        String finalStr = "";
        if (null == strs || strs.getBytes().length <= length) {
            finalStr = (strs == null ? "" : strs);
        } else {
            for (int i = 0; i < strs.length(); i++) {
                String str = strs.substring(i, i + 1);
                // 累加单个字符字节数
                sum += str.getBytes().length;
                if (sum > length) {
                    finalStr = strs.substring(0, i) + "...";
                    break;
                }
            }
        }
        return finalStr;
    }

    public static String filterOpenid(HttpServletRequest request, TWxConfig wxCfg) {
        String openid = StrUtil.blankToDefault(request.getParameter("openid"), request.getHeader("x-wx-openid"));
        Map<String, String> map = HttpUtil.decodeParamMap(request.getHeader("referer"), "UTF8");
        if (StrUtil.isBlank(openid)) {
            openid = map.get("openid");
        }
        if (StrUtil.isNotBlank(openid)) {
            String sign = StrUtil.blankToDefault(request.getParameter("_s"), request.getHeader("x-wx-sign"));
            if (StrUtil.isBlank(sign)) {
                sign = map.get("_s");
            }
            if (!StrUtil.isBlank(sign)) {
                return DigestUtil.md5Hex(wxCfg.getAppSecret() + openid).equals(sign) ? openid : null;
            }
        }
        return null;
    }

    public static String filterMch(HttpServletRequest request) {
        String mch = StrUtil.blankToDefault(request.getParameter("mch"), request.getHeader("x-wx-mch"));
        if (StrUtil.isBlank(mch)) {
            Map<String, String> map = HttpUtil.decodeParamMap(request.getHeader("referer"), "UTF8");
            mch = map.get("mch");
        }
        return mch;
    }
}
