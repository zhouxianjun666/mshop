package com.all580.mshop.util;

import com.all580.mshop.annotation.Append;
import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.annotation.Version;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/6/13 17:21
 */
public class All580Provider extends MapperTemplate {
    public All580Provider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

    public String insertOrUpdate(MappedStatement ms) {
        Class<?> entityClass = getEntityClass(ms);
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.insertIntoTable(entityClass, tableName(entityClass)));
        sql.append(SqlHelper.insertColumns(entityClass, true, true, getConfig().isNotEmpty()));
        sql.append(SqlHelper.insertValuesColumns(entityClass, true, true, getConfig().isNotEmpty()));
        sql.append("ON DUPLICATE KEY UPDATE");
        String updateSetColumns = SqlHelper.updateSetColumns(entityClass, null, true, getConfig().isNotEmpty());
        sql.append(updateSetColumns.replace("<set>", "<trim suffixOverrides=\",\">").replace("</set>", "</trim>"));
        return sql.toString();
    }

    public String updateAppend(MappedStatement ms) {
        Class<?> entityClass = getEntityClass(ms);
        StringBuilder sql = updateAppendSql(ms, null);
        sql.append(SqlHelper.wherePKColumns(entityClass, true));
        return sql.toString();
    }

    public String updateAppendWhereExample(MappedStatement ms) {
        StringBuilder sql = updateAppendSql(ms, "record");
        sql.append(SqlHelper.updateByExampleWhereClause());
        return sql.toString();
    }

    private StringBuilder updateAppendSql(MappedStatement ms, String entityName) {
        Class<?> entityClass = getEntityClass(ms);
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
        sql.append("<set>");
        //获取全部列
        Set<EntityColumn> columnSet = EntityHelper.getColumns(entityClass);
        //当某个列有主键策略时，不需要考虑他的属性是否为空，因为如果为空，一定会根据主键策略给他生成一个值
        for (EntityColumn column : columnSet) {
            if (!column.isId() && isNumber(column)) {
                sql.append(SqlHelper.getIfNotNull(entityName, column, String.format("%s = %s + %s,", column.getColumn(), column.getColumn(), column.getColumnHolder(entityName)), true));
            } else if (!column.isId() && column.isUpdatable()) {
                sql.append(SqlHelper.getIfNotNull(entityName, column, column.getColumnEqualsHolder(entityName) + ",", true));
            }
        }
        sql.append("</set>");
        return sql;
    }

    private boolean isNumber(EntityColumn column) {
        return Number.class.isAssignableFrom(column.getJavaType()) &&
                !column.getEntityField().isAnnotationPresent(Version.class) &&
                column.getEntityField().isAnnotationPresent(Append.class);
    }
}
