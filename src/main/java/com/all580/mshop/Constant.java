package com.all580.mshop;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;

import java.util.concurrent.TimeUnit;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 常量
 * @date 2018/5/16 9:27
 */
public class Constant {
    /**
     * 性别类型
     */
    public static final class SexType {
        /**
         * 男
         */
        public static final int MAN = 1;
        /**
         * 女
         */
        public static final int FEMALE = 2;
        /**
         * 未知
         */
        public static final int UNKNOWN = 0;
    }

    /**
     * 会员账单类型
     */
    public static final class MemberBillType {
        /**
         * 返利
         */
        public static final int REBATE = 1;
        /**
         * 提现
         */
        public static final int WITHDRAW = 2;
    }

    /**
     * 提现类型
     */
    public static final class WithdrawType {
        /**
         * 微信
         */
        public static final int WX = 1;
        /**
         * 支付宝
         */
        public static final int ALIPAY = 2;
        /**
         * 银行卡
         */
        public static final int BANK = 3;
    }

    /**
     * 提现状态
     */
    public static final class WithdrawStatus {
        /**
         * 待审核
         */
        public static final int WAIT_AUDIT = 1;
        /**
         * 待付款
         */
        public static final int WAIT_PAY = 2;
        /**
         * 已提现
         */
        public static final int SUCCESS = 3;
    }

    /**
     * 提现限额类型
     */
    public static final class WithdrawLimitType {
        /**
         * 每周几可以提现
         */
        public static final int WEEK = 1;
        /**
         * 每月几号可以提现
         */
        public static final int MONTH = 2;
    }

    /**
     * 平台产品类型
     */
    public static final class PlatformProductType {
        /**
         * 景点
         */
        public static final int TICKET = 5101;
        /**
         * 酒店
         */
        public static final int HOTEL = 5102;
        /**
         * 线路
         */
        public static final int LINE = 5103;
    }

    /**
     * 产品类型
     */
    public static final class ProductType {
        /**
         * 返利产品
         */
        public static final int REBATE = 1;
        /**
         * 限购产品
         */
        public static final int LIMIT = 2;
    }

    /**
     * 订单状态
     */
    public static final class OrderStatus {
        /**
         * 待支付
         */
        public static final int PENDING_PAY = 1;
        /**
         * 支付中
         */
        public static final int PAYING = 2;
        /**
         * 已支付
         */
        public static final int PAID = 3;
        /**
         * 已取消
         */
        public static final int CANCEL = 4;
    }

    /**
     * 拼团单状态
     */
    public static final class GroupOrderStatus {
        /**
         * 创建
         */
        public static final int CREATE = 1;
        /**
         * 开启
         */
        public static final int OPEN = 2;
        /**
         * 失败
         */
        public static final int FAIL = 3;
        /**
         * 完成
         */
        public static final int COMPLETE = 4;
    }

    /**
     * 退订订单状态
     */
    public static final class RefundOrderStatus {
        /**
         * 退订中
         */
        public static final int PENDING = 1;
        /**
         * 退款中
         */
        public static final int PENDING_MONEY = 2;
        /**
         * 退订成功(退款成功)
         */
        public static final int SUCCESS = 3;
    }

    /**
     * 会员状态
     */
    public static final class MemberStatus {
        /**
         * 禁用
         */
        public static final int DISABLED = 0;
        /**
         * 启用
         */
        public static final int ENABLE = 1;
        /**
         * 限制提现
         */
        public static final int LIMIT_WITHDRAW = 2;
    }

    /**
     * 返利金额类型
     */
    public static final class RebateType {
        /**
         * 百分比
         */
        public static final int PERCENT = 1;
        /**
         * 固定
         */
        public static final int FIXED = 2;
    }

    public static final String WX_OPENID = "WX_OPENID";
    public static final String WX_USER_INFO = "WX_USER_INFO";
    public static final TimedCache<String, String> STRING_CACHE = CacheUtil.newTimedCache(TimeUnit.MINUTES.toMillis(5));
    static {
        STRING_CACHE.schedulePrune(TimeUnit.MINUTES.toMillis(1));
    }
}
