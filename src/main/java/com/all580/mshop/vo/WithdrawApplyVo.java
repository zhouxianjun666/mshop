package com.all580.mshop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 15:07
 */
@ApiModel("提现申请")
@Data
public class WithdrawApplyVo {
    @ApiModelProperty(value = "收款方式", notes = "1-微信;2-支付宝;3-银行卡", required = true)
    private int type;
    @ApiModelProperty(value = "提现金额", required = true)
    private int money;
}
