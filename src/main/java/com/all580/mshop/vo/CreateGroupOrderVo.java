package com.all580.mshop.vo;

import com.all580.mshop.annotation.IdCard;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/29 9:13
 */
@Data
@Accessors(chain = true)
public class CreateGroupOrderVo {
    @ApiModelProperty("子产品编号")
    private long code;
    @NotNull(message = "请选择游玩日期")
    @ApiModelProperty("游玩日期")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd")
    private Date booking;
    @ApiModelProperty("天数")
    private int days;
    @ApiModelProperty("张数")
    @Min(value = 1, message = "张数必须大于0")
    private int quantity;
    @ApiModelProperty("姓名")
    @NotBlank(message = "请输入姓名")
    private String name;
    @ApiModelProperty("手机号码")
    @NotBlank(message = "请输入手机号码")
    private String phone;
    @ApiModelProperty("身份证")
    @IdCard
    private String sid;
    @ApiModelProperty("性别")
    private Integer sex;
}
