package com.all580.mshop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 15:47
 */
@ApiModel("提现付款")
@Data
public class WithdrawPayerVo {
    @ApiModelProperty("付款账号")
    private String paymentAccount;
    @ApiModelProperty("付款人")
    private String payer;
}
