package com.all580.mshop.filter;

import com.all580.mshop.dto.Result;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/30 11:29
 */
public class LoginAuthorizationFilter extends FormAuthenticationFilter {

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        boolean bool = onAccessDenied(req, res);
        if (!bool) {
            Result.builder().code(Result.NO_LOGIN).build().write();
        }
        return bool;
    }

    protected boolean onAccessDenied(HttpServletRequest req, HttpServletResponse res) {
        return false;
    }
}
