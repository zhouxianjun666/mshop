package com.all580.mshop.filter;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxToken;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.util.Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 13:44
 */
public class WxFilter implements Filter {
    @Autowired
    private WxManager wxManager;

    public static boolean is(HttpServletRequest request) {
        String userAgent = ServletUtil.getHeaderIgnoreCase(request, "User-Agent");
        if (StrUtil.isNotBlank(userAgent)) {
            userAgent = userAgent.toUpperCase();
            return userAgent.contains("MICROMESSENGER");
        }

        return false;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = WebUtils.toHttp(request);

        if (is(req)) {
            String mch = Util.filterMch(req);
            if (StrUtil.isBlank(mch)) {
                Result.fail("缺少商家信息,请联系商家").write();
                return;
            }
            TWxConfig wxCfg = wxManager.getConfig(mch);
            if (wxCfg == null) {
                Result.fail("未配置微信信息,请联系商家").write();
                return;
            }
            String openid = Util.filterOpenid(req, wxCfg);

            if (StrUtil.isNotBlank(openid)) {
                Subject subject = SecurityUtils.getSubject();
                Object principal = subject.getPrincipal();
                if (!subject.isAuthenticated() || !(principal instanceof WxUser) ||
                        !((WxUser) principal).getMerchant().getAccessId().equals(mch)) {
                    subject.login(new WxToken(openid, openid, mch));
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
