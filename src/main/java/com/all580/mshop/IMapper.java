package com.all580.mshop;

import cn.hutool.core.bean.BeanUtil;
import com.all580.mshop.util.All580Provider;
import com.alone.tk.mybatis.JoinExampleMapper;
import lombok.SneakyThrows;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.annotation.Version;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;

import java.util.Optional;
import java.util.function.Function;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 通用Mapper
 * @date 2018/5/25 10:33
 */
public interface IMapper<T> extends Mapper<T>, MySqlMapper<T>, IdsMapper<T>, JoinExampleMapper<T> {
    /**
     * 插入或者更新实体(唯一约束)
     * @param entity 实体
     * @return
     */
    @InsertProvider(type = All580Provider.class, method = "dynamicSQL")
    int insertOrUpdate(T entity);

    @UpdateProvider(type = All580Provider.class, method = "dynamicSQL")
    int updateAppend(T entity);

    @UpdateProvider(type = All580Provider.class, method = "dynamicSQL")
    int updateAppendWhereExample(@Param("record") T entity, @Param("example") Object example);

    @SneakyThrows
    default int updateByVersion(T entity, Function<T, Integer> function) {
        Optional<EntityColumn> column = EntityHelper.getColumns(entity.getClass()).stream().filter(c -> c.getEntityField().isAnnotationPresent(Version.class)).findFirst();
        if (column.isPresent()) {
            Object version = column.get().getEntityField().getValue(entity);
            if (version == null) {
                T db = selectByPrimaryKey(entity);
                String name = column.get().getEntityField().getName();
                BeanUtil.setFieldValue(entity, name, BeanUtil.getFieldValue(db, name));
            }
            int ret = function.apply(entity);
            if(ret == 0){
                throw new RuntimeException("乐观锁更新失败");
            }
            return ret;
        }
        return function.apply(entity);
    }

    default int updateByVersion(T entity) {
        return updateByVersion(entity, this::updateByPrimaryKeySelective);
    }
}
