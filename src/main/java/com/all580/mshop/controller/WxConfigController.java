package com.all580.mshop.controller;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TWxConfigMapper;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 15:19
 */
@Api(tags = "微信(公众号)配置接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/wx/config")
public class WxConfigController {
    @Resource
    private TWxConfigMapper wxConfigMapper;
    @Autowired
    private WxManager wxManager;

    @ApiOperation(value = "获取当前登录商户的微信配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("info")
    @RequiresRoles("merchant")
    public Result<?> info() {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        return Result.ok(wxManager.getConfig(merchant.getAccessId()));
    }

    @ApiOperation(value = "新增微信配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("add")
    @RequiresRoles("merchant")
    public Result<?> add(@ApiParam(required = true) @RequestBody TWxConfig config) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        config.setMerchantId(merchant.getId());
        config.setCreateTime(new Date());
        int ret = wxConfigMapper.insertSelective(config);
        if (ret > 0) {
            wxManager.refreshConfig(merchant.getAccessId());
        }
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "修改微信配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update")
    @RequiresRoles("merchant")
    public Result<?> update(@ApiParam(required = true) @RequestBody TWxConfig config) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        TWxConfig db = wxManager.getConfig(merchant.getAccessId());
        if (db == null) {
            return Result.fail("微信配置不存在");
        }
        config.setMerchantId(null);
        int ret = wxConfigMapper.updateByPrimaryKeySelective(config);
        if (ret > 0) {
            wxManager.refreshConfig(merchant.getAccessId());
        }
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "删除微信配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove/{id}")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @PathVariable int id) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        TWxConfig db = wxManager.getConfig(merchant.getAccessId());
        if (db == null) {
            return Result.fail("微信配置不存在");
        }
        int ret = wxConfigMapper.deleteByPrimaryKey(id);
        if (ret > 0) {
            wxManager.refreshConfig(merchant.getAccessId());
        }
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "微信签名", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "签名地址,如未设置则取referer")
    })
    @GetMapping("sign")
    @RequiresRoles("wx")
    public Result<?> signature(String url, HttpServletRequest request) {
        url = StrUtil.blankToDefault(url, request.getHeader("referer"));
        if (StrUtil.isBlank(url)) {
            return Result.fail("签名地址不能为空");
        }
        url = StrUtil.subBefore(url, "#", false);
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("请配置微信支付信息");
        }
        String ticket = wxManager.getTicket(config);
        if (StrUtil.isBlank(ticket)) {
            return Result.fail("请稍候再试");
        }
        String nonceStr = RandomUtil.randomString(10);
        long timestamp = System.currentTimeMillis() / 1000;
        String signature = DigestUtil.sha1Hex(String.format("jsapi_ticket=%s&noncestr=%s&timestamp=%s&url=%s", ticket, nonceStr, timestamp, url));
        return Result.ok()
                .put("appId", config.getAppId())
                .put("timestamp", timestamp)
                .put("nonceStr", nonceStr)
                .put("signature", signature);
    }
}
