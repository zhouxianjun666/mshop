package com.all580.mshop.controller;

import com.all580.mshop.Constant;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxToken;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TMerchant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/5 14:02
 */
@Api(tags = "认证接口")
@RestController
@CrossOrigin(allowCredentials = "true")
public class AuthController {

    @ApiOperation(value = "登录", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("login")
    public Result<?> login(@ApiParam(required = true) @RequestBody TMerchant merchant) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(merchant.getPhone(), merchant.getPassword());
        subject.login(token);
        return Result.ok();
    }

    @ApiOperation(value = "登出", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("logout")
    public Result<?> logout() {
        SecurityUtils.getSubject().logout();
        return Result.ok();
    }

    @ApiOperation(value = "获取当前已授权的信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("info")
    public Result<?> info(@RequestParam(defaultValue = "false") boolean force, HttpServletRequest request) {
        Object wxInfo = request.getSession().getAttribute(Constant.WX_USER_INFO);
        Object principal = SecurityUtils.getSubject().getPrincipal();
        if (principal != null && force) {
            if (principal instanceof WxUser) {
                WxUser wxUser = (WxUser) principal;
                SecurityUtils.getSubject().login(new WxToken(wxUser.getUsername(), wxUser.getUsername(), wxUser.getMerchant().getAccessId()));
                principal = SecurityUtils.getSubject().getPrincipal();
            }
        }
        return principal == null ? Result.fail(Result.NO_LOGIN) : Result.ok(principal).put("wx", wxInfo);
    }
}
