package com.all580.mshop.controller;

import cn.hutool.core.util.StrUtil;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.sts.model.v20150401.AssumeRoleRequest;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;
import com.all580.mshop.dto.OssConfig;
import com.all580.mshop.dto.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/17 17:26
 */
@Api(tags = "OSS接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/oss")
public class OssController {
    @Autowired
    private DefaultAcsClient acsClient;
    @Autowired
    private OssConfig ossConfig;

    @ApiOperation(value = "获取STS临时授权", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("sts")
    @RequiresRoles("merchant")
    @SneakyThrows
    public Result<?> sts() {
        AssumeRoleRequest request = new AssumeRoleRequest();
        request.setMethod(MethodType.POST);
        request.setRoleArn(ossConfig.getRoleArn());
        request.setRoleSessionName("session-name");
        if (StrUtil.isNotBlank(ossConfig.getPolicy())) {
            request.setPolicy(ossConfig.getPolicy());
        }
        AssumeRoleResponse response = acsClient.getAcsResponse(request);
        return Result.ok(response.getCredentials()).put("region", ossConfig.getRegion()).put("imgBucket", ossConfig.getImgBucket());
    }
}
