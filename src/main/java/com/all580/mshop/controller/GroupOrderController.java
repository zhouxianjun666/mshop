package com.all580.mshop.controller;

import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.PlatformOrderInfo;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.*;
import com.all580.mshop.manager.GroupOrderManager;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TGroupOrderMapper;
import com.all580.mshop.mapper.TGroupOrderMemberMapper;
import com.all580.mshop.mapper.TGroupProductMapper;
import com.all580.mshop.vo.CreateGroupOrderVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 14:04
 */
@Api(tags = "拼团订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/order/group")
@Slf4j
public class GroupOrderController {
    @Resource
    private TGroupProductMapper groupProductMapper;
    @Resource
    private TGroupOrderMemberMapper groupOrderMemberMapper;
    @Resource
    private TGroupOrderMapper groupOrderMapper;
    @Autowired
    private GroupOrderManager orderManager;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private PlatformManager platformManager;

    @ApiOperation(value = "下单", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("create")
    @RequiresRoles("wx")
    @Locks("#{#number?: null}")
    public Result<?> create(@ApiParam(required = true) @RequestBody @Validated CreateGroupOrderVo vo,
                            @ApiParam(value = "拼团号") @RequestParam(required = false) Long number,
                            @ApiParam(required = true, value = "单独购买") @RequestParam boolean single) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMember member = wxUser.getMember();
        TGroupProduct product = groupProductMapper.selectOne((TGroupProduct) new TGroupProduct().setMerchantId(member.getMerchantId()).setSubCode(vo.getCode()));
        Assert.notNull(product, "产品不存在");
        if (!single && product.getMaxQuantity() != null && product.getMaxQuantity() > 0 && vo.getQuantity() > product.getMaxQuantity()) {
            return Result.fail("超出最大张数:" + product.getMaxQuantity());
        }
        if (product.getMerchantId().intValue() != member.getMerchantId()) {
            return Result.fail("无效的产品");
        }
        return orderManager.create(member, product, vo, single, number);
    }

    @ApiOperation(value = "支付", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("pay/{number}")
    @RequiresRoles("wx")
    @Locks("#{#number}")
    public Result<?> pay(@ApiParam(required = true) @PathVariable long number,
                         HttpServletRequest request) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TGroupOrderMember order = groupOrderMemberMapper.selectOne(new TGroupOrderMember().setNumber(number).setMemberId(member.getId()));
        Assert.notNull(order, "订单不存在");
        Assert.isTrue(Constant.OrderStatus.PAID != order.getStatus(), "该订单已支付");
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("未配置微信支付信息,请联系商家");
        }

        boolean checkPay = orderManager.processOrderStatus(order, config);
        if (checkPay) {
            return Result.ok();
        }
        TGroupOrder groupOrder = groupOrderMapper.selectByPrimaryKey(order.getGroupId());
        // 更新状态
        int ret = groupOrderMemberMapper.updateByPrimaryKeySelective(new TGroupOrderMember().setId(order.getId()).setStatus(Constant.OrderStatus.PAYING));
        if (ret <= 0) {
            return Result.fail();
        }

        return orderManager.pay(order.setGroupOrder(groupOrder), config, member, request);
    }

    @ApiOperation(value = "取消支付", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("cancel/{number}")
    @RequiresRoles("wx")
    @Locks("#{#number}")
    public Result<?> pay(@ApiParam(required = true) @PathVariable long number) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TGroupOrderMember order = groupOrderMemberMapper.selectOne(new TGroupOrderMember().setNumber(number).setMemberId(member.getId()).setStatus(Constant.OrderStatus.PAYING));
        Assert.notNull(order, "订单不存在");
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("未配置微信支付信息,请联系商家");
        }
        boolean checkPay = orderManager.processOrderStatus(order, config);
        if (checkPay) {
            return Result.fail("该订单已支付");
        }
        orderManager.cancelOrderMember(order);
        return Result.ok();
    }

    @ApiOperation(value = "根据子产品编号获取开团列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("list/open/{code}")
    @RequiresRoles("wx")
    public Result<?> listGroup(@PathVariable long code, @RequestParam int pageNum, @RequestParam int pageSize) {
        PageInfo<TGroupOrder> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> groupOrderMapper.selectOpenList(code));
        return Result.ok(pageInfo).put("sum", Optional.ofNullable(groupOrderMapper.sumPendingMemberByCode(code)).orElse(0));
    }

    @ApiOperation(value = "根据拼ID获取参与列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("{id}/member")
    @RequiresRoles("wx")
    public Result<?> groupMembers(@PathVariable int id, @RequestParam int pageNum, @RequestParam int pageSize) {
        PageInfo<TGroupOrderMember> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> groupOrderMemberMapper.select(new TGroupOrderMember().setGroupId(id).setStatus(Constant.OrderStatus.PAID)));
        return Result.ok(pageInfo);
    }

    @ApiOperation(value = "获取订单列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupStatus", value = "团队状态"),
            @ApiImplicitParam(name = "status", value = "个人状态"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @RequiresRoles("wx")
    public Result<?> list(Integer groupStatus, Integer status, Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        Object value;
        List<Map<String, Object>> list;
        if (pageNum != null && pageSize != null) {
            PageInfo<Map<String, Object>> pageInfo = PageHelper.startPage(pageNum, pageSize)
                    .doSelectPageInfo(() -> groupOrderMapper.selectByMember(member.getId(), groupStatus, status));
            list = pageInfo.getList();
            value = pageInfo;
        } else {
            list = groupOrderMapper.selectByMember(member.getId(), groupStatus, status);
            value = list;
        }
        if (!CollectionUtils.isEmpty(list)) {
            Set<Long> numbers = list.stream()
                    .filter(m -> MapUtils.getIntValue(m, "groupStatus") == Constant.GroupOrderStatus.COMPLETE)
                    .map(m -> MapUtils.getLongValue(m, "orderNumber"))
                    .collect(Collectors.toSet());
            if (!CollectionUtils.isEmpty(numbers)) {
                List<PlatformOrderInfo> orderInfos = platformManager.orderApi(merchant).batchInfo(merchant, numbers);
                Map<Long, PlatformOrderInfo> orderMap = orderInfos.stream().collect(Collectors.toMap(PlatformOrderInfo::getNumber, Function.identity()));
                list.stream()
                        .filter(m -> MapUtils.getIntValue(m, "groupStatus") == Constant.GroupOrderStatus.COMPLETE)
                        .forEach(m -> m.put("info", orderMap.get(MapUtils.getLongValue(m, "orderNumber"))));
            }
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "获取明细列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("detail/list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookingStart", value = "游玩日期开始"),
            @ApiImplicitParam(name = "bookingEnd", value = "游玩日期结束"),
            @ApiImplicitParam(name = "start", value = "开团时间开始"),
            @ApiImplicitParam(name = "end", value = "开团时间结束"),
            @ApiImplicitParam(name = "ownerPhone", value = "开团会员手机号"),
            @ApiImplicitParam(name = "ownerNickname", value = "开团会员昵称"),
            @ApiImplicitParam(name = "joinPhone", value = "加入会员手机号"),
            @ApiImplicitParam(name = "joinNickname", value = "加入会员昵称"),
            @ApiImplicitParam(name = "subCode", value = "子产品编号"),
            @ApiImplicitParam(name = "productName", value = "主产品名称"),
            @ApiImplicitParam(name = "subName", value = "子产品名称"),
            @ApiImplicitParam(name = "single", value = "是否单人购买"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @RequiresRoles("merchant")
    public Result<?> detailList(Date bookingStart, Date bookingEnd, Date start, Date end,
                                String ownerPhone, String ownerNickname, String joinPhone, String joinNickname,
                                Long subCode, String productName, String subName,
                                Boolean single, Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> groupOrderMemberMapper.selectDetail(
                    bookingStart, bookingEnd, start, end, ownerPhone, ownerNickname,
                    subCode, productName, subName, single, joinPhone, joinNickname, merchant.getId()));
        } else {
            value = groupOrderMemberMapper.selectDetail(bookingStart, bookingEnd, start, end, ownerPhone, ownerNickname,
                    subCode, productName, subName, single, joinPhone, joinNickname, merchant.getId());
        }
        return Result.ok(value);
    }
}
