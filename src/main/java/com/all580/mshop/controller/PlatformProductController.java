package com.all580.mshop.controller;

import com.all580.mshop.dto.PlatformProductInfo;
import com.all580.mshop.dto.PlatformProductItem;
import com.all580.mshop.dto.Result;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.manager.PlatformManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/11/23 10:21
 */
@Api(tags = "平台产品接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/platform/product")
public class PlatformProductController {
    @Autowired
    private PlatformManager platformManager;

    @ApiOperation(value = "搜索主产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true),
    })
    @GetMapping("search/product")
    @RequiresRoles("merchant")
    public Result<List<PlatformProductInfo>> searchProduct(@RequestParam int type, String name) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        List<PlatformProductInfo> list = platformManager.productApi(merchant).searchProduct(merchant, type, name);
        return Result.ok(list);
    }

    @ApiOperation(value = "搜索子产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "子产品名称"),
            @ApiImplicitParam(name = "productId", value = "主产品ID", required = true),
    })
    @GetMapping("search/sub/{productId}")
    @RequiresRoles("merchant")
    public Result<List<PlatformProductItem>> searchSub(@PathVariable int productId, String name) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        List<PlatformProductItem> list = platformManager.productApi(merchant).searchSub(merchant, productId, name);
        return Result.ok(list);
    }
}
