package com.all580.mshop.controller;

import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TBanner;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.mapper.TBannerMapper;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/11/27 10:55
 */
@Api(tags = "banner接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/banner")
public class BannerController {
    @Resource
    private TBannerMapper bannerMapper;

    @ApiOperation(value = "获取列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles(value = {"merchant", "wx"}, logical = Logical.OR)
    public Result<?> list(Integer pageNum, Integer pageSize) {
        AbstractUser user = (AbstractUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = user instanceof WxUser ? ((WxUser) user).getMerchant() : (TMerchant) user;
        Example.Builder builder = Example.builder(TBanner.class)
                .orderByAsc("seq")
                .where(WeekendSqls.<TBanner>custom()
                        .andEqualTo(TBanner::getMerchantId, merchant.getId())
                );
        if (user instanceof WxUser) {
            builder.andWhere(WeekendSqls.<TBanner>custom()
                    .andLessThanOrEqualTo(TBanner::getEffectiveStart, new Date())
            ).andWhere(WeekendSqls.<TBanner>custom()
                    .andGreaterThanOrEqualTo(TBanner::getEffectiveEnd, new Date())
                    .orIsNull(TBanner::getEffectiveEnd)
            );
        }

        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> bannerMapper.selectByExample(builder.build()));
        } else {
            value = bannerMapper.selectByExample(builder.build());
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "新增", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("add")
    @RequiresRoles("merchant")
    public Result<?> add(@ApiParam @RequestBody @Validated(AddGroup.class) TBanner banner) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        merchant.setForAdd(banner);
        int ret = bannerMapper.insertSelective(banner);
        return ret > 0 ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "修改", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("update")
    @RequiresRoles("merchant")
    public Result<?> update(@ApiParam @RequestBody @Validated(UpdateGroup.class) TBanner banner) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        int ret = bannerMapper.updateByExampleSelective(banner.setMerchantId(null),
                Example.builder(TBanner.class)
                        .where(WeekendSqls.<TBanner>custom()
                                .andEqualTo(TBanner::getId, banner.getId())
                                .andEqualTo(TBanner::getMerchantId, merchant.getId())
                        )
                        .build()
        );
        return ret > 0 ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "删除", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("remove/{id}")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam @PathVariable int id) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        int ret = bannerMapper.deleteByExample(Example.builder(TBanner.class)
                .where(WeekendSqls.<TBanner>custom()
                        .andEqualTo(TBanner::getId, id)
                        .andEqualTo(TBanner::getMerchantId, merchant.getId())
                )
                .build()
        );
        return ret > 0 ? Result.ok() : Result.fail();
    }
}
