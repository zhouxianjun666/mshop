package com.all580.mshop.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TWithdraw;
import com.all580.mshop.entity.TWithdrawConfig;
import com.all580.mshop.manager.WithdrawManager;
import com.all580.mshop.mapper.TMemberMapper;
import com.all580.mshop.mapper.TWithdrawConfigMapper;
import com.all580.mshop.mapper.TWithdrawMapper;
import com.all580.mshop.util.KeyLock;
import com.all580.mshop.vo.WithdrawApplyVo;
import com.all580.mshop.vo.WithdrawPayerVo;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 9:23
 */
@Api(tags = "提现接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/withdraw")
@Slf4j
public class WithdrawController {
    @Resource
    private TWithdrawMapper withdrawMapper;
    @Resource
    private TMemberMapper memberMapper;
    @Resource
    private TWithdrawConfigMapper withdrawConfigMapper;
    @Autowired
    private WithdrawManager withdrawManager;
    private KeyLock<String> lock = new KeyLock<>();

    @ApiOperation(value = "获取提现列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickname", value = "会员昵称"),
            @ApiImplicitParam(name = "phone", value = "会员手机号码"),
            @ApiImplicitParam(name = "start", value = "提现时间-开始"),
            @ApiImplicitParam(name = "end", value = "提现时间-结束"),
            @ApiImplicitParam(name = "status", value = "状态"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(String nickname, String phone, Date start, Date end, Integer status, Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> withdrawMapper.search(merchant.getId(), nickname, phone, start, end, status));
        } else {
            value = withdrawMapper.search(merchant.getId(), nickname, phone, start, end, status);
        }
        return Result.builder().code(Result.SUCCESS).value(value).build();
    }

    @ApiOperation(value = "获取提现配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("config/get")
    @RequiresRoles(value = { "wx", "merchant" }, logical = Logical.OR)
    public Result<?> getConfig() {
        Object principal = SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = principal instanceof WxUser ? ((WxUser) principal).getMerchant() : (TMerchant) principal;
        TWithdrawConfig config = withdrawConfigMapper.selectOne(new TWithdrawConfig().setMerchantId(merchant.getId()));
        return Result.ok(config);
    }

    @ApiOperation(value = "设置提现配置", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("config/set")
    @RequiresRoles("merchant")
    public Result<?> setConfig(@ApiParam(readOnly = true) @RequestBody TWithdrawConfig config) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        merchant.setForAdd(config);
        int ret = withdrawConfigMapper.insertOrUpdate(config);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "审核通过", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("accept/{id}")
    @RequiresRoles("merchant")
    public Result<?> accept(@ApiParam(required = true, value = "提现ID") @PathVariable int id) {
        TWithdraw withdraw = withdrawMapper.selectByPrimaryKey(id);
        if (withdraw == null) {
            return Result.fail("提现申请不存在");
        }
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        if (merchant.getId().intValue() != withdraw.getMerchantId()) {
            return Result.fail("权限不足");
        }
        if (withdraw.getStatus() != Constant.WithdrawStatus.WAIT_AUDIT) {
            return Result.fail("当前提现已审核");
        }
        int ret = withdrawMapper.updateByPrimaryKeySelective(new TWithdraw()
                .setId(id)
                .setStatus(Constant.WithdrawStatus.WAIT_PAY)
                .setAuditTime(new Date())
        );
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "提现申请", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("apply")
    @RequiresRoles("wx")
    public Result<?> apply(@ApiParam(required = true) @RequestBody WithdrawApplyVo apply) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TWithdrawConfig config = withdrawConfigMapper.selectOne(new TWithdrawConfig().setMerchantId(merchant.getId()));
        if (config != null) {
            if (apply.getMoney() < config.getLimit()) {
                return Result.fail("不能低于最低限额:" + (config.getLimit() / 100));
            }
            if (config.getType() == Constant.WithdrawLimitType.MONTH && DateUtil.dayOfMonth(new Date()) != config.getMonthDay()) {
                return Result.fail("今日不能提现");
            }
            int dayWeek = DateUtil.dayOfWeek(new Date());
            if (dayWeek == 1) {
                dayWeek = 7;
            } else {
                dayWeek -= 1;
            }
            if (config.getType() == Constant.WithdrawLimitType.WEEK && dayWeek != config.getWeekDay()) {
                return Result.fail("今日不能提现");
            }
            if (config.getStart() != null && config.getEnd() != null) {
                if (!DateUtil.isIn(new Date(), DateUtil.parseTimeToday(DateUtil.formatTime(config.getStart())), DateUtil.parseTimeToday(DateUtil.formatTime(config.getEnd())))) {
                    return Result.fail("当前时间不能提现");
                }
            }
        }

        member = memberMapper.selectByPrimaryKey(member.getId());
        if (member.getStatus() == Constant.MemberStatus.LIMIT_WITHDRAW) {
            return Result.fail("该会员禁止提现");
        }
        if (member.getBalance() < apply.getMoney()) {
            return Result.fail("余额不足");
        }
        if (apply.getType() == Constant.WithdrawType.ALIPAY) {
            if (StrUtil.isBlank(member.getAlipayAccount()) || StrUtil.isBlank(member.getAlipayName())) {
                return Result.fail("请完善支付宝信息");
            }
        }
        if (apply.getType() == Constant.WithdrawType.BANK) {
            if (StrUtil.isBlank(member.getBankAccountName()) || StrUtil.isBlank(member.getBankBranch()) || StrUtil.isBlank(member.getBankCardNo()) || StrUtil.isBlank(member.getBankName())) {
                return Result.fail("请完善银联信息");
            }
        }
        int count = withdrawMapper.selectCountByExample(Example.builder(TWithdraw.class)
                .where(WeekendSqls.<TWithdraw>custom()
                        .andNotEqualTo(TWithdraw::getStatus, Constant.WithdrawStatus.SUCCESS)
                        .andEqualTo(TWithdraw::getMemberId, member.getId())
                )
                .build());
        if (count > 0) {
            return Result.fail("您当前还有未完成的提现申请");
        }
        int ret = withdrawMapper.insertSelective(new TWithdraw()
                .setStatus(Constant.WithdrawStatus.WAIT_AUDIT)
                .setCreateTime(new Date())
                .setMemberId(member.getId())
                .setMerchantId(merchant.getId())
                .setMoney(apply.getMoney())
                .setType(apply.getType())
        );
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "付款", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("payment/{id}")
    @RequiresRoles("merchant")
    public Result<?> payment(@ApiParam(required = true, value = "提现ID") @PathVariable int id,
                             @ApiParam @RequestBody WithdrawPayerVo vo) {
        TWithdraw withdraw = withdrawMapper.selectByPrimaryKey(id);
        if (withdraw == null) {
            return Result.fail("提现申请不存在");
        }
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        if (merchant.getId().intValue() != withdraw.getMerchantId()) {
            return Result.fail("权限不足");
        }
        if (withdraw.getStatus() != Constant.WithdrawStatus.WAIT_PAY) {
            return Result.fail("当前提现不能付款");
        }

        try {
            lock.lock(String.valueOf(withdraw.getId()));
            withdrawManager.payment(vo, withdraw, merchant);
        } finally {
            lock.unlock(String.valueOf(withdraw.getId()));
        }
        return Result.ok();
    }
}
