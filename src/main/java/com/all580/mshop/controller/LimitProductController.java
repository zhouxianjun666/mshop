package com.all580.mshop.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TLimitProduct;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.mapper.TLimitProductMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 9:22
 */
@Api(tags = "限时抢购产品接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/limit/product")
public class LimitProductController {
    @Autowired
    private PlatformManager platformManager;
    @Resource
    private TLimitProductMapper limitProductMapper;

    @ApiOperation(value = "拉取产品列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("ticket")
    @RequiresRoles("merchant")
    public Result<?> ticket(String name, @RequestParam int type, @RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "20") int pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        PlatformProductResult result = platformManager.productApi(merchant).items(merchant, name, type, pageNum, pageSize);
        List<PlatformProductItem> items = result.getList();
        if (CollectionUtils.isEmpty(items)) {
            return Result.ok(result.setList(Collections.emptyList()));
        }
        Set<Long> codes = items.stream().map(PlatformProductItem::getCode).collect(Collectors.toSet());
        List<TLimitProduct> products = limitProductMapper.selectByExample(new Example.Builder(TLimitProduct.class)
                .where(WeekendSqls.<TLimitProduct>custom()
                        .andIn(TLimitProduct::getSubCode, codes)
                        .andEqualTo(TLimitProduct::getMerchantId, merchant.getId())
                )
                .build()
        );
        Map<Long, TLimitProduct> limitMap = products.stream().collect(Collectors.toMap(TLimitProduct::getSubCode, p -> p));
        items.forEach(item -> {
            TLimitProduct product = limitMap.get(item.getCode());
            boolean isSet = product != null;
            item.put("isSet", isSet);
            if (isSet) {
                item.put("price", product.getPrice());
                item.put("startTime", product.getStartTime());
                item.put("endTime", product.getEndTime());
            }
        });
        return Result.ok(result);
    }

    @ApiOperation(value = "获取已设置的抢购产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(String name, @RequestParam int type,
                          Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        WeekendSqls<TLimitProduct> custom = WeekendSqls.custom();
        custom.andEqualTo(TLimitProduct::getMerchantId, merchant.getId()).andEqualTo(TLimitProduct::getType, type);
        Example.Builder builder = new Example.Builder(TLimitProduct.class).where(custom).orderBy("seq").orderByDesc("id");
        if (StrUtil.isNotBlank(name)) {
            name = "%" + name + "%";
            builder.andWhere(WeekendSqls.<TLimitProduct>custom().andLike(TLimitProduct::getProductName, name).orLike(TLimitProduct::getSubName, name));
        }

        Example example = builder.build();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> limitProductMapper.selectByExample(example));
        } else {
            value = limitProductMapper.selectByExample(example);
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "新增抢购产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("add")
    @RequiresRoles("merchant")
    public Result<?> add(@ApiParam(required = true) @RequestBody List<TLimitProduct> products) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            merchant.setForAdd(p);
            if (p.getMaxQuantity() == null) {
                p.setMaxQuantity(0);
            }
            if (StringUtils.isEmpty(p.getSupplier()) || StringUtils.isEmpty(p.getProductImgs())) {
                PlatformProductResult result = platformManager.productApi(merchant).items(merchant, null, p.getType(),
                        p.getProductId(),
                        Collections.singleton(p.getSubCode()),
                        1, 1);
                List<PlatformProductItem> items = result.getList();
                if (CollectionUtils.isEmpty(items)) {
                    throw new RuntimeException("获取产品信息失败");
                }
                PlatformProductItem item = items.get(0);
                p.setProductImgs(JSONUtil.toJsonStr(item.getImg()));
                p.setSubImgs(JSONUtil.toJsonStr(item.getSubImg()));
                p.setSupplier(item.getSupplier());
                p.setProductName(item.getProductName());
                p.setSubName(item.getName());
            }
        });
        try {
            int ret = limitProductMapper.insertList(products);
            return Result.builder().code(ret == products.size() ? Result.SUCCESS : Result.FAIL).build();
        } catch (DuplicateKeyException e) {
            return Result.fail("该产品已添加");
        }
    }

    @ApiOperation(value = "修改抢购产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update")
    @RequiresRoles("merchant")
    public Result<?> update(@ApiParam(required = true) @RequestBody List<TLimitProduct> products) {
        check(products);
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            limitProductMapper.updateByPrimaryKeySelective((TLimitProduct) p.setMerchantId(null).setUpdateTime(new Date()));
        });
        return Result.ok();
    }

    @ApiOperation(value = "重设抢购产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("reset")
    @RequiresRoles("merchant")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> reset(@ApiParam(required = true) @RequestBody List<TLimitProduct> products) {
        check(products);
        String ids = products.stream().map(p -> String.valueOf(p.getId())).collect(Collectors.joining(","));
        limitProductMapper.deleteByIds(ids);
        return add(products);
    }

    @ApiOperation(value = "删除抢购产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove/{id}")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @PathVariable int id) {
        check(Collections.singleton(id));
        int ret = limitProductMapper.deleteByPrimaryKey(id);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "删除抢购产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @RequestBody List<Integer> ids) {
        check(ids);
        int ret = limitProductMapper.deleteByIds(ids.stream().map(String::valueOf).collect(Collectors.joining(",")));
        return Result.builder().code(ret == ids.size() ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "获取限时抢购销售产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("sale")
    @RequiresRoles("wx")
    public Result<?> sale(Integer type, Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        Example.Builder builder = new Example.Builder(TLimitProduct.class)
                .where(WeekendSqls.<TLimitProduct>custom()
                        .andEqualTo(TLimitProduct::getMerchantId, merchant.getId())
                        .andGreaterThan(TLimitProduct::getEndTime, new Date())
                ).orderBy("seq").orderByDesc("id");
        if (type != null) {
            builder.where(WeekendSqls.<TLimitProduct>custom().andEqualTo(TLimitProduct::getType, type));
        }
        PageInfo<TLimitProduct> pageInfo = null;
        List<TLimitProduct> products;
        if (pageNum != null && pageSize != null) {
            pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> limitProductMapper.selectByExample(builder.build()));
            products = pageInfo.getList();
        } else {
            products = limitProductMapper.selectByExample(builder.build());
        }
        if (!CollectionUtils.isEmpty(products)) {
            sale(products, merchant);
        }
        return Result.ok(pageInfo == null ? products : pageInfo);
    }

    @ApiOperation(value = "获取产品信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("info/{id}")
    @RequiresRoles("wx")
    public Result<PlatformProductInfo> info(@ApiParam(required = true) @PathVariable int id) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TLimitProduct product = limitProductMapper.selectByPrimaryKey(id);
        if (product == null) {
            return Result.fail("产品不存在");
        }
        Set<Long> codes = Collections.singleton(product.getSubCode());
        PlatformProductInfo info = platformManager.productApi(merchant).info(merchant, product.getType(), product.getProductId(), new Date(), codes);

        if (info == null) {
            return Result.fail();
        }
        info.getSubInfo().forEach(subInfo -> subInfo.setPrice(product.getPrice())
                .put("start", product.getStartTime())
                .put("end", product.getEndTime())
                .put(TLimitProduct::getNotice, product)
                .put(TLimitProduct::getTitle, product)
        );
        return Result.ok(info);
    }

    @ApiOperation(value = "获取销售日历", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "产品编号", required = true),
            @ApiImplicitParam(name = "start", value = "开始日期,默认当日"),
            @ApiImplicitParam(name = "end", value = "结束日期,默认本月末")
    })
    @GetMapping("calendar/{code}")
    @RequiresRoles("wx")
    public Result<List<PlatformCalendar>> calendar(@PathVariable long code,
                                                   Date start, Date end) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TLimitProduct product = limitProductMapper.selectOne((TLimitProduct) new TLimitProduct().setMerchantId(merchant.getId()).setSubCode(code));
        if (product == null) {
            return Result.fail("产品不存在");
        }
        start = start == null ? DateUtil.beginOfDay(new Date()) : start;
        end = end == null ? DateUtil.endOfMonth(new Date()) : end;
        List<PlatformCalendar> calendars = platformManager.productApi(merchant).calendar(merchant, code, start, end);
        if (calendars != null) {
            calendars.forEach(c -> c.setPrice(product.getPrice()));
        }
        return Result.ok(calendars);
    }

    private List<TLimitProduct> sale(List<TLimitProduct> products, TMerchant merchant) {
        // 获取子产品编号集合
        Set<Long> codes = products.stream().map(TLimitProduct::getSubCode).collect(Collectors.toSet());
        // 按子产品获取主产品的最小价格
        List<ProductMinPrice> prices = platformManager.productApi(merchant).minPrice(merchant, new Date(), codes);

        // 设置是否可以售卖
        products.forEach(p -> p.setSale(prices.stream().anyMatch(ps -> ps.getId() == p.getProductId())));
        return products;
    }

    private List<TLimitProduct> check(List<TLimitProduct> products) {
        return check(products.stream().map(TLimitProduct::getId).collect(Collectors.toSet()));
    }
    private List<TLimitProduct> check(Collection<Integer> collection) {
        String ids = collection.stream().map(String::valueOf).collect(Collectors.joining(","));
        List<TLimitProduct> list = limitProductMapper.selectByIds(ids);
        if (list == null || list.size() != collection.size()) {
            throw new RuntimeException("产品不存在");
        }
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        list.forEach(p -> {
            if (merchant.getId().intValue() != p.getMerchantId()) {
                throw new RuntimeException("权限不足");
            }
        });
        return list;
    }
}
