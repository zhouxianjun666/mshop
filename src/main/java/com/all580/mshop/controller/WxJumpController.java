package com.all580.mshop.controller;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.SystemConfig;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.filter.WxFilter;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TMemberMapper;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.util.Util;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.shiro.authc.AccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/10 13:21
 */
@Controller
@RequestMapping("wx/jump")
@Slf4j
public class WxJumpController {
    private final static String WX_AUTH_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";

    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private WxManager wxManager;
    @Resource
    private TMemberMapper memberMapper;
    @Resource
    private TMerchantMapper merchantMapper;

    @RequestMapping("go")
    @SneakyThrows
    public void jump(HttpServletRequest request, HttpServletResponse response) {
        if (WxFilter.is(request)) {
            // 商户accessId
            String mch = Util.filterMch(request);
            // 0:to 地址不带参数 1: to参数 2: 全地址
            String[] jumpInfo = getJumpUrl(request);
            String query = jumpInfo[1];
            if (query.contains("#")) {
                query = StrUtil.subBefore(query, "#", false) + "&" + StrUtil.subAfter(query, "?", true);
            }
            Map<String, String> params = HttpUtil.decodeParamMap(query, StandardCharsets.UTF_8.name());

            TWxConfig config = wxManager.getConfig(mch);
            if (config == null) {
                Result.fail("没有配置微信信息,请联系管理员").write();
                return;
            }

            String domain = StrUtil.emptyToDefault(this.systemConfig.getDomain(), String.format("%s://%s%s",
                    request.getScheme(),
                    request.getServerName(),
                    request.getContextPath()
            ));
            Map<String, Object> map = new LinkedHashMap<>(4);
            map.put("appid", config.getAppId());
            map.put("redirect_uri", domain + "/wx/jump/back?_=" + HttpUtil.encodeUtf8(jumpInfo[2]) + "&mch=" + mch);
            map.put("response_type", "code");
            map.put("scope", params.getOrDefault("scope", "snsapi_userinfo"));
            String redirect = HttpUtil.urlWithForm(WX_AUTH_URL, map, CharsetUtil.CHARSET_UTF_8, true);
            redirect += "#wechat_redirect";
            log.info("wx jump url: {}", redirect);
            response.sendRedirect(redirect);
        } else {
            Result.fail("请使用微信客户端访问").write();
        }
    }

    @RequestMapping("back")
    @SneakyThrows
    public void back(@RequestParam String code, @RequestParam("_") String to, @RequestParam String mch,
                     HttpServletRequest request, HttpServletResponse response) {
        TWxConfig config = wxManager.getConfig(mch);
        if (config == null) {
            Result.fail("没有配置微信信息,请联系管理员").write();
            return;
        }
        Map<String, String> params = HttpUtil.decodeParamMap(to, CharsetUtil.CHARSET_UTF_8.name());
        Map<String, Object> map = wxManager.getOpenid(config.getAppId(), config.getAppSecret(), code);
        String openid = MapUtils.getString(map, "openid");
        if (StrUtil.isBlank(openid)) {
            Result.fail("获取openid失败").setValue(map).write();
            return;
        }
        request.getSession().setAttribute(Constant.WX_OPENID, openid);
        String scope = params.getOrDefault("scope", "snsapi_userinfo");
        Map<String, Object> info = new HashMap<>(1);
        if ("snsapi_userinfo".equals(scope)) {
            info = wxManager.getInfo(MapUtils.getString(map, "access_token"), openid);
            request.getSession().setAttribute(Constant.WX_USER_INFO, info);
        }
        String sign = DigestUtil.md5Hex(config.getAppSecret() + openid);
        int index = to.indexOf("?");
        if (index > -1) {
            to = to.substring(0, index + 1) + "openid=" + openid + "&_s=" + sign + "&" + to.substring(index + 1);
        } else {
            to += "?openid=" + openid + "&_s=" + sign;
        }
        to = HttpUtil.decode(to, CharsetUtil.CHARSET_UTF_8);
        log.info("wx jump back url: {}", to);

        // 自动注册或更新会员
        TMerchant merchant = merchantMapper.selectOne(new TMerchant().setAccessId(mch));
        if (merchant == null) {
            throw new AccountException("商户不存在");
        }
        TMember record = new TMember().setOpenid(openid).setMerchantId(merchant.getId());
        TMember member = memberMapper.selectOne(record);
        member = member == null ? record.setCreateTime(new Date()) : member;
        member.setNickname(MapUtils.getString(info, "nickname", member.getNickname()))
                .setAvatar(MapUtils.getString(info, "headimgurl", member.getAvatar()))
                .setCountry(MapUtils.getString(info, "country", member.getCountry()))
                .setProvince(MapUtils.getString(info, "province", member.getProvince()))
                .setCity(MapUtils.getString(info, "city", member.getCity()))
                .setSex(MapUtils.getIntValue(info, "sex", Optional.ofNullable(member.getSex()).orElse(0)));
        if (member.getId() == null) {
            memberMapper.insertSelective(member);
        } else {
            memberMapper.updateByPrimaryKeySelective(member);
        }
        response.sendRedirect(to);
    }

    private String[] getJumpUrl(HttpServletRequest request) {
        String referer = StrUtil.emptyToDefault(request.getParameter("url"), request.getHeader("referer"));
        int index = referer.indexOf("?");
        if (index > -1) {
            return new String[]{referer.substring(0, index), referer.substring(index + 1), referer};
        }
        return new String[]{referer, "", referer};
    }
}
