package com.all580.mshop.controller.notify;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.Order;
import com.all580.mshop.dto.Result;
import com.all580.mshop.entity.TGroupOrderMember;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.manager.GroupOrderManager;
import com.all580.mshop.manager.OrderManager;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TGroupOrderMemberMapper;
import com.all580.mshop.mapper.TOrderMapper;
import com.all580.mshop.mapper.TWxConfigMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/13 11:32
 */
@Controller
@RequestMapping("notify/wx")
@Slf4j
public class WxNotifyController {
    @Resource
    private TOrderMapper orderMapper;
    @Resource
    private TGroupOrderMemberMapper groupOrderMemberMapper;
    @Resource
    private TWxConfigMapper wxConfigMapper;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private GroupOrderManager groupOrderManager;

    @SneakyThrows
    @PostMapping("pay")
    public void pay(HttpServletRequest request, HttpServletResponse response) {
        String xml = IoUtil.read(request.getInputStream(), StandardCharsets.UTF_8);
        log.info("微信通知: IP={} Msg={}", ServletUtil.getClientIP(request), xml);
        Result<TreeMap<String, Object>> result = WxManager.wxResult(xml);
        if (!result.isSuccess()) {
            log.warn("微信通知: 失败 => {}", result.getMsg());
            return;
        }
        TreeMap<String, Object> map = result.getValue();
        long number = MapUtils.getLongValue(map, "out_trade_no");
        ((WxNotifyController) AopContext.currentProxy()).process(number, map, response);
    }

    @Locks("#{#number}")
    public void process(long number, TreeMap<String, Object> map, HttpServletResponse response) {
        String attach = Objects.toString(map.get("attach"));
        Order order;
        if ("group".equals(attach)) {
            order = groupOrderMemberMapper.selectOne(new TGroupOrderMember().setNumber(number));
        } else {
            order = orderMapper.selectOne(new TOrder().setOrderNumber(number));
        }
        if (order == null) {
            log.warn("微信通知: 订单号={} 不存在", number);
            return;
        }
        int total = MapUtils.getIntValue(map, "total_fee");
        if (total != order.getMoney()) {
            log.warn("微信通知: 订单号={} 支付金额不匹配 => {} -> {}", number, total, order.getMoney());
            return;
        }
        TWxConfig config = wxConfigMapper.selectOne(new TWxConfig().setMerchantId(order.getMerchantId()));
        if (config == null) {
            log.warn("微信通知: 订单号={} 微信支付配置不存在, 不检查签名.", number);
        } else {
            boolean verify = WxManager.verify(map, config);
            if (!verify) {
                log.warn("微信通知: 订单号={} 签名验证失败", number);
                return;
            }
        }
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("return_code", WxManager.SUCCESS);
        resultMap.put("return_msg", "OK");
        ServletUtil.write(response, XmlUtil.mapToXmlStr(resultMap, "xml"), "text/xml");
        if (order instanceof TGroupOrderMember) {
            groupOrderManager.paid(number, MapUtils.getString(map, "transaction_id"));
        } else {
            orderManager.paid(number, MapUtils.getString(map, "transaction_id"));
        }
    }
}
