package com.all580.mshop.controller;

import cn.hutool.core.util.StrUtil;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMemberBills;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.mapper.TMemberBillsMapper;
import com.all580.mshop.util.Util;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/18 9:51
 */
@Api(tags = "会员(推广员)流水接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/member/bills")
@Slf4j
public class MemberBillsController {
    @Resource
    private TMemberBillsMapper memberBillsMapper;

    @ApiOperation(value = "获取自己流水列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型:1-返利;2-提现"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("wx")
    public Result<?> listSelf(Integer type, Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        return search(wxUser.getMember().getId(), type, null, null, pageNum, pageSize);
    }

    @ApiOperation(value = "获取会员流水列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "会员ID"),
            @ApiImplicitParam(name = "type", value = "类型:1-返利;2-提现"),
            @ApiImplicitParam(name = "memberId", value = "会员ID"),
            @ApiImplicitParam(name = "start", value = "开始时间"),
            @ApiImplicitParam(name = "end", value = "结束时间"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    @GetMapping("list/{memberId}")
    @RequiresRoles("merchant")
    public Result<?> list(@PathVariable int memberId, Integer type, Date start, Date end, Integer pageNum, Integer pageSize) {
        return search(memberId, type, start, end, pageNum, pageSize);
    }

    @ApiOperation(value = "获取自己流水总变动值", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型:1-返利;2-提现")
    })
    @GetMapping("total")
    @RequiresRoles("wx")
    public Result<?> total(Integer type) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMember member = wxUser.getMember();
        return Result.ok(memberBillsMapper.sumTotal(member.getId(), type));
    }

    @ApiOperation(value = "返利明细(Admin|Member)", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "refId", value = "关联ID")
    })
    @GetMapping("detail/rebate/{refId}")
    @RequiresRoles(value = { "wx", "merchant" }, logical = Logical.OR)
    public Result<?> rebateDetail(@PathVariable int refId) {
        Object principal = SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> bill = memberBillsMapper.getRebateBill(refId, principal instanceof WxUser ? ((WxUser) principal).getMember().getId() : null);
        bill.computeIfPresent("orderNumber", (key, val) -> Util.hide(StrUtil.utf8Str(val), 4, '*'));
        return Result.ok(bill);
    }

    @ApiOperation(value = "提现明细(Admin|Member)", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "refId", value = "关联ID")
    })
    @GetMapping("detail/withdraw/{refId}")
    @RequiresRoles(value = { "wx", "merchant" }, logical = Logical.OR)
    public Result<?> withdrawDetail(@PathVariable int refId) {
        Object principal = SecurityUtils.getSubject().getPrincipal();
        return Result.ok(memberBillsMapper.getWithdrawBill(refId, principal instanceof WxUser ? ((WxUser) principal).getMember().getId() : null));
    }

    @ApiOperation(value = "返利列表(Admin)", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fromId", value = "推广员ID"),
            @ApiImplicitParam(name = "fromNickname", value = "推广员昵称"),
            @ApiImplicitParam(name = "fromPhone", value = "推广员手机号"),
            @ApiImplicitParam(name = "productName", value = "产品名称"),
            @ApiImplicitParam(name = "start", value = "开始时间"),
            @ApiImplicitParam(name = "end", value = "结束时间"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数")
    })
    @GetMapping("detail/rebate")
    @RequiresRoles("merchant")
    public Result<?> rebateDetailList(Integer fromId,
                                      String fromNickname,
                                      String fromPhone,
                                      String productName,
                                      Date start,
                                      Date end,
                                      Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> memberBillsMapper.searchRebateBills(merchant.getId(), fromId, fromNickname, fromPhone, productName, start, end));
        } else {
            value = memberBillsMapper.searchRebateBills(merchant.getId(), fromId, fromNickname, fromPhone, productName, start, end);
        }

        return Result.ok(value);
    }

    private Result<?> search(int memberId, Integer type, Date start, Date end, Integer pageNum, Integer pageSize) {
        WeekendSqls<TMemberBills> where = WeekendSqls.custom();
        where.andEqualTo(TMemberBills::getMemberId, memberId);
        if (type != null) {
            where.andEqualTo(TMemberBills::getType, type);
        }
        if (start != null) {
            where.andGreaterThanOrEqualTo(TMemberBills::getCreateTime, start);
        }
        if (end != null) {
            where.andLessThanOrEqualTo(TMemberBills::getCreateTime, end);
        }
        Example example = new Example.Builder(TMemberBills.class).where(where).orderByDesc("id").build();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> memberBillsMapper.selectByExample(example));
        } else {
            value = memberBillsMapper.selectByExample(example);
        }
        return Result.ok(value);
    }
}
