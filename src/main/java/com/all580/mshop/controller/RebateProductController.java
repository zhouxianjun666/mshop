package com.all580.mshop.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TRebateProduct;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.mapper.TRebateProductMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 9:22
 */
@Api(tags = "返利产品接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/rebate/product")
public class RebateProductController {
    @Autowired
    private PlatformManager platformManager;
    @Resource
    private TRebateProductMapper rebateProductMapper;

    @ApiOperation(value = "拉取产品列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页码"),
            @ApiImplicitParam(name = "pageSize", value = "页大小"),
    })
    @GetMapping("ticket")
    @RequiresRoles("merchant")
    public Result<PlatformProductResult> ticket(String name, @RequestParam int type, @RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "20") int pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        PlatformProductResult result = platformManager.productApi(merchant).items(merchant, name, type, pageNum, pageSize);
        List<PlatformProductItem> items = result.getList();
        if (CollectionUtils.isEmpty(items)) {
            return Result.ok(result.setList(Collections.emptyList()));
        }
        Set<Long> codes = items.stream().map(PlatformProductItem::getCode).collect(Collectors.toSet());
        List<TRebateProduct> products = rebateProductMapper.selectByExample(new Example.Builder(TRebateProduct.class)
                .where(WeekendSqls.<TRebateProduct>custom()
                        .andIn(TRebateProduct::getSubCode, codes)
                        .andEqualTo(TRebateProduct::getMerchantId, merchant.getId())
                )
                .build()
        );
        Map<Long, TRebateProduct> rebateMap = products.stream().collect(Collectors.toMap(TRebateProduct::getSubCode, p -> p));
        items.forEach(item -> {
            TRebateProduct product = rebateMap.get(item.getCode());
            boolean isSet = product != null;
            item.put("isSet", isSet);
            if (isSet) {
                item.put(TRebateProduct::getPrice, product);
                item.put(TRebateProduct::getRebate, product);
                item.put(TRebateProduct::getRebateType, product);
                item.put(TRebateProduct::getRebatePercent, product);
            }
        });
        return Result.ok(result);
    }

    @ApiOperation(value = "获取已设置的返利产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(String name, @RequestParam int type,
                          Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        WeekendSqls<TRebateProduct> custom = WeekendSqls.custom();
        custom.andEqualTo(TRebateProduct::getMerchantId, merchant.getId()).andEqualTo(TRebateProduct::getType, type);
        Example.Builder builder = new Example.Builder(TRebateProduct.class).where(custom).orderByDesc("id");
        if (StrUtil.isNotBlank(name)) {
            name = "%" + name + "%";
            builder.andWhere(WeekendSqls.<TRebateProduct>custom().andLike(TRebateProduct::getProductName, name).orLike(TRebateProduct::getSubName, name));
        }

        Example example = builder.build();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> rebateProductMapper.selectByExample(example));
        } else {
            value = rebateProductMapper.selectByExample(example);
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "新增返利产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("add")
    @RequiresRoles("merchant")
    public Result<?> add(@ApiParam(required = true) @RequestBody List<TRebateProduct> products) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            merchant.setForAdd(p);
        });
        int ret = rebateProductMapper.insertList(products);
        return Result.builder().code(ret == products.size() ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "修改返利产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update")
    @RequiresRoles("merchant")
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public Result<?> update(@ApiParam(required = true) @RequestBody List<TRebateProduct> products) {
        check(products);
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            rebateProductMapper.updateByPrimaryKeySelective((TRebateProduct) p.setMerchantId(null).setUpdateTime(new Date()));
        });
        return Result.ok();
    }

    @ApiOperation(value = "删除返利产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove/{id}")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @PathVariable int id) {
        check(Collections.singleton(id));
        int ret = rebateProductMapper.deleteByPrimaryKey(id);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "删除返利产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove")
    @RequiresRoles("merchant")
    public Result<?> removeList(@ApiParam(required = true) @RequestBody List<Integer> ids) {
        check(ids);
        int ret = rebateProductMapper.deleteByIds(ids.stream().map(String::valueOf).collect(Collectors.joining(",")));
        return Result.builder().code(ret == ids.size() ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "获取返利销售产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("sale")
    @RequiresRoles("wx")
    public Result<?> sale(@RequestParam int type,
                          Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        PageInfo<TRebateProduct> pageInfo = null;
        List<TRebateProduct> products;
        if (pageNum != null && pageSize != null) {
            pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> rebateProductMapper.selectGroupProductId(merchant.getId(), type));
            products = pageInfo.getList();
        } else {
            products = rebateProductMapper.selectGroupProductId(merchant.getId(), type);
        }
        if (!CollectionUtils.isEmpty(products)) {
            sale(products, merchant);
        }
        return Result.ok(pageInfo == null ? products : pageInfo);
    }

    @ApiOperation(value = "获取产品信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "产品ID", required = true),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true)
    })
    @GetMapping("info")
    @RequiresRoles("wx")
    public Result<PlatformProductInfo> info(@RequestParam int productId, @RequestParam int type) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        List<TRebateProduct> products = rebateProductMapper.select((TRebateProduct) new TRebateProduct().setMerchantId(merchant.getId()).setType(type).setProductId(productId));
        if (CollectionUtils.isEmpty(products)) {
            return Result.fail("产品不存在");
        }
        Set<Long> codes = products.stream().map(TRebateProduct::getSubCode).collect(Collectors.toSet());
        PlatformProductInfo info = platformManager.productApi(merchant).info(merchant, type, productId, new Date(), codes);

        if (info == null) {
            return Result.fail();
        }
        Map<Long, TRebateProduct> map = products.stream().collect(Collectors.toMap(TRebateProduct::getSubCode, Function.identity()));
        info.getSubInfo().forEach(subInfo -> {
            TRebateProduct product = map.get(subInfo.getCode());
            subInfo.setPrice(product.getPrice())
                    .put(TRebateProduct::getRebate, product)
                    .put(TRebateProduct::getRebateType, product)
                    .put(TRebateProduct::getRebatePercent, product);
        });
        return Result.ok(info);
    }

    @ApiOperation(value = "获取销售日历", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "产品编号", required = true),
            @ApiImplicitParam(name = "start", value = "开始日期,默认当日"),
            @ApiImplicitParam(name = "end", value = "结束日期,默认本月末")
    })
    @GetMapping("calendar/{code}")
    @RequiresRoles("wx")
    public Result<List<PlatformCalendar>> calendar(@PathVariable long code,
                                                   Date start, Date end) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TRebateProduct product = rebateProductMapper.selectOne((TRebateProduct) new TRebateProduct().setMerchantId(merchant.getId()).setSubCode(code));
        if (product == null) {
            return Result.fail("产品不存在");
        }
        start = start == null ? DateUtil.beginOfDay(new Date()) : start;
        end = end == null ? DateUtil.endOfMonth(new Date()) : end;
        List<PlatformCalendar> calendars = platformManager.productApi(merchant).calendar(merchant, code, start, end);
        if (calendars != null) {
            calendars.forEach(c -> c.setPrice(product.getPrice()));
        }
        return Result.ok(calendars);
    }

    private List<TRebateProduct> sale(List<TRebateProduct> products, TMerchant merchant) {
        // 获取当前页的主产品ID集合
        Set<Integer> ids = products.stream().map(TRebateProduct::getProductId).collect(Collectors.toSet());
        // 查询当前页主产品的所有子产品列表
        List<TRebateProduct> list = rebateProductMapper.selectByExample(new Example.Builder(TRebateProduct.class)
                .where(WeekendSqls.<TRebateProduct>custom().andIn(TRebateProduct::getProductId, ids))
                .build()
        );
        // 获取子产品编号集合
        Set<Long> codes = list.stream().map(TRebateProduct::getSubCode).collect(Collectors.toSet());
        // 按子产品获取主产品的最小价格
        List<ProductMinPrice> prices = platformManager.productApi(merchant).minPrice(merchant, new Date(), codes);

        // 主产品最小价格分组
        Map<Integer, Integer> map = list.stream()
                .collect(Collectors.groupingBy(
                        TRebateProduct::getProductId,
                        Collectors.collectingAndThen(
                                Collectors.collectingAndThen(
                                        Collectors.minBy(Comparator.comparingInt(TRebateProduct::getPrice)),
                                        Optional::get
                                ),
                                TRebateProduct::getPrice
                        ))
                );

        // 设置最低价和是否可以售卖
        products.forEach(p -> p.setPrice(map.get(p.getProductId())).setSale(prices.stream().anyMatch(ps -> ps.getId() == p.getProductId())));
        return products;
    }

    private void check(List<TRebateProduct> products) {
        check(products.stream().map(TRebateProduct::getId).collect(Collectors.toSet()));
    }
    private void check(Collection<Integer> collection) {
        String ids = collection.stream().map(String::valueOf).collect(Collectors.joining(","));
        List<TRebateProduct> list = rebateProductMapper.selectByIds(ids);
        if (list == null || list.size() != collection.size()) {
            throw new RuntimeException("产品不存在");
        }
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        list.forEach(p -> {
            if (merchant.getId().intValue() != p.getMerchantId()) {
                throw new RuntimeException("权限不足");
            }
        });
    }
}
