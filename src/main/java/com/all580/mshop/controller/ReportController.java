package com.all580.mshop.controller;

import cn.hutool.core.util.StrUtil;
import com.all580.mshop.dto.Result;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.mapper.TOrderMapper;
import com.alone.tk.mybatis.JoinExample;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/20 17:57
 */
@Api(tags = "报表接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/report")
public class ReportController {
    @Resource
    private TOrderMapper orderMapper;

    @ApiOperation(value = "获取报表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型:1-返利;2-限购"),
            @ApiImplicitParam(name = "productType", value = "产品类型"),
            @ApiImplicitParam(name = "productName", value = "产品名称"),
            @ApiImplicitParam(name = "start", value = "开始时间"),
            @ApiImplicitParam(name = "end", value = "结束时间"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(Integer type, Integer productType, String productName, Date start, Date end,
                          Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> orderMapper.report(type, merchant.getId(), productName, productType, start, end));
        } else {
            value = orderMapper.report(type, merchant.getId(), productName, productType, start, end);
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "获取抢购明细", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productType", value = "产品类型"),
            @ApiImplicitParam(name = "productName", value = "产品名称"),
            @ApiImplicitParam(name = "status", value = "状态"),
            @ApiImplicitParam(name = "start", value = "开始时间"),
            @ApiImplicitParam(name = "end", value = "结束时间"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("limit/detail")
    @RequiresRoles("merchant")
    public Result<?> limitDetail(Integer productType, String status, String productName, Date start, Date end,
                          Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        Set<String> statusSet = StrUtil.isBlank(status) ? null : Arrays.stream(status.split(",")).collect(Collectors.toSet());
        JoinExample example = JoinExample.builder(TOrder.class)
                .addCol(TOrder.class)
                .addCol(TMember::getNickname)
                .addTable(new JoinExample.Table(TMember.class, TMember::getId, TOrder::getMemberId))
                .where(JoinExample.Where.custom()
                        .andEqualTo(TOrder::getMerchantId, merchant.getId())
                        .andEqualTo(TMember::getMerchantId, merchant.getId())
                        .andEqualTo(TOrder::getProductType, productType)
                        .andGreaterThanOrEqualTo(TOrder::getCreateTime, start)
                        .andLessThanOrEqualTo(TOrder::getCreateTime, end)
                        .andIn(TOrder::getStatus, statusSet)
                )
                .andWhere(JoinExample.Where.custom()
                        .andLike(TOrder::getProductName, productName)
                        .orLike(TOrder::getSubName, productName)
                )
                .desc(TOrder::getId)
                .build();
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> orderMapper.selectByJoinExample(example));
        } else {
            value = orderMapper.selectByJoinExample(example);
        }
        return Result.ok(value);
    }
}
