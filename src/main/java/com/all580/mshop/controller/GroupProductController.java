package com.all580.mshop.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TGroupProduct;
import com.all580.mshop.entity.TLimitProduct;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.mapper.TGroupProductMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 9:22
 */
@Api(tags = "拼团产品接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/group/product")
public class GroupProductController {
    @Autowired
    private PlatformManager platformManager;
    @Resource
    private TGroupProductMapper groupProductMapper;

    @ApiOperation(value = "拉取产品列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("ticket")
    @RequiresRoles("merchant")
    public Result<?> ticket(String name, @RequestParam int type, @RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "20") int pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        PlatformProductResult result = platformManager.productApi(merchant).items(merchant, name, type, pageNum, pageSize);
        List<PlatformProductItem> items = result.getList();
        if (CollectionUtils.isEmpty(items)) {
            return Result.ok(result.setList(Collections.emptyList()));
        }
        Set<Long> codes = items.stream().map(PlatformProductItem::getCode).collect(Collectors.toSet());
        List<TGroupProduct> products = groupProductMapper.selectByExample(new Example.Builder(TGroupProduct.class)
                .where(WeekendSqls.<TGroupProduct>custom()
                        .andIn(TGroupProduct::getSubCode, codes)
                        .andEqualTo(TGroupProduct::getMerchantId, merchant.getId())
                )
                .build()
        );
        Map<Long, TGroupProduct> map = products.stream().collect(Collectors.toMap(TGroupProduct::getSubCode, p -> p));
        items.forEach(item -> {
            TGroupProduct product = map.get(item.getCode());
            boolean isSet = product != null;
            item.put("isSet", isSet);
            if (isSet) {
                item.put(TGroupProduct::getId, product);
                item.put(TGroupProduct::getPrice, product);
                item.put(TGroupProduct::getGroupPrice, product);
                item.put(TGroupProduct::getNum, product);
                item.put(TGroupProduct::getMaxQuantity, product);
                item.put(TGroupProduct::getNotes, product);
            }
        });
        return Result.ok(result);
    }

    @ApiOperation(value = "获取已设置的拼团产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "产品名称"),
            @ApiImplicitParam(name = "type", value = "产品类型", required = true),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(String name, @RequestParam int type,
                          Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        WeekendSqls<TGroupProduct> custom = WeekendSqls.custom();
        custom.andEqualTo(TGroupProduct::getMerchantId, merchant.getId()).andEqualTo(TGroupProduct::getType, type);
        Example.Builder builder = new Example.Builder(TGroupProduct.class).where(custom).orderByDesc("id");
        if (StrUtil.isNotBlank(name)) {
            name = "%" + name + "%";
            builder.andWhere(WeekendSqls.<TGroupProduct>custom().andLike(TGroupProduct::getProductName, name).orLike(TGroupProduct::getSubName, name));
        }

        Example example = builder.build();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> groupProductMapper.selectByExample(example));
        } else {
            value = groupProductMapper.selectByExample(example);
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "新增拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("add")
    @RequiresRoles("merchant")
    public Result<?> add(@ApiParam(required = true) @RequestBody @Validated(AddGroup.class) TGroupProduct product) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        if (product.getPurchasePrice() == null || product.getPurchasePrice() <= 0) {
            throw new RuntimeException("进货价必须大于0");
        }
        merchant.setForAdd(product);
        int ret = groupProductMapper.insertSelective((TGroupProduct) product.setId(null));
        return ret > 0 ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "批量新增拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("add-list")
    @RequiresRoles("merchant")
    public Result<?> addList(@ApiParam(required = true) @RequestBody @Validated(AddGroup.class) List<TGroupProduct> products) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            merchant.setForAdd(p);
        });
        int ret = groupProductMapper.insertList(products);
        return Result.builder().code(ret == products.size() ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "批量修改拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update-list")
    @RequiresRoles("merchant")
    public Result<?> updateList(@ApiParam(required = true) @RequestBody @Validated(UpdateGroup.class) List<TGroupProduct> products) {
        check(products);
        products.forEach(p -> {
            if (p.getPurchasePrice() == null || p.getPurchasePrice() <= 0) {
                throw new RuntimeException("进货价必须大于0");
            }
            groupProductMapper.updateByPrimaryKeySelective((TGroupProduct) p.setMerchantId(null).setUpdateTime(new Date()));
        });
        return Result.ok();
    }

    @ApiOperation(value = "修改拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update")
    @RequiresRoles("merchant")
    public Result<?> update(@ApiParam(required = true) @RequestBody @Validated(UpdateGroup.class) TGroupProduct product) {
        check(Collections.singletonList(product));
        if (product.getPurchasePrice() == null || product.getPurchasePrice() <= 0) {
            throw new RuntimeException("进货价必须大于0");
        }
        groupProductMapper.updateByPrimaryKeySelective((TGroupProduct) product.setMerchantId(null).setUpdateTime(new Date()));
        return Result.ok();
    }

    @ApiOperation(value = "删除拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove/{id}")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @PathVariable int id) {
        check(Collections.singleton(id));
        int ret = groupProductMapper.deleteByPrimaryKey(id);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "删除拼团产品", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("remove")
    @RequiresRoles("merchant")
    public Result<?> remove(@ApiParam(required = true) @RequestBody List<Integer> ids) {
        check(ids);
        int ret = groupProductMapper.deleteByIds(ids.stream().map(String::valueOf).collect(Collectors.joining(",")));
        return Result.builder().code(ret == ids.size() ? Result.SUCCESS : Result.FAIL).build();
    }

    @ApiOperation(value = "获取拼团销售产品列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("sale")
    @RequiresRoles("wx")
    public Result<?> sale(Integer type, Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        PageInfo<TGroupProduct> pageInfo = null;
        List<TGroupProduct> products;
        if (pageNum != null && pageSize != null) {
            pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> groupProductMapper.selectGroupProductId(merchant.getId(), type));
            products = pageInfo.getList();
        } else {
            products = groupProductMapper.selectGroupProductId(merchant.getId(), type);
        }
        if (!CollectionUtils.isEmpty(products)) {
            sale(products, merchant);
        }
        return Result.ok(pageInfo == null ? products : pageInfo);
    }

    @ApiOperation(value = "获取产品信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "产品ID", required = true),
            @ApiImplicitParam(name = "type", value = "产品类型:5101-景点;5102-酒店;5103-线路", required = true)
    })
    @GetMapping("info")
    @RequiresRoles("wx")
    public Result<PlatformProductInfo> info(@RequestParam int productId, @RequestParam int type) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        List<TGroupProduct> products = groupProductMapper.select((TGroupProduct) new TGroupProduct().setMerchantId(merchant.getId()).setType(type).setProductId(productId));
        if (CollectionUtils.isEmpty(products)) {
            return Result.fail("产品不存在");
        }
        Set<Long> codes = products.stream().map(TGroupProduct::getSubCode).collect(Collectors.toSet());
        PlatformProductInfo info = platformManager.productApi(merchant).info(merchant, type, productId, new Date(), codes);

        if (info == null) {
            return Result.fail();
        }
        Map<Long, TGroupProduct> map = products.stream().collect(Collectors.toMap(TGroupProduct::getSubCode, Function.identity()));
        info.getSubInfo().forEach(subInfo -> {
            TGroupProduct product = map.get(subInfo.getCode());
            subInfo.setPrice(product.getPrice())
                    .put(TGroupProduct::getGroupPrice, product)
                    .put(TGroupProduct::getNum, product)
                    .put(TGroupProduct::getMaxQuantity, product)
                    .put(TGroupProduct::getNotes, product);
        });
        return Result.ok(info);
    }

    @ApiOperation(value = "获取销售日历", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "产品编号", required = true),
            @ApiImplicitParam(name = "start", value = "开始日期,默认当日"),
            @ApiImplicitParam(name = "end", value = "结束日期,默认本月末")
    })
    @GetMapping("calendar/{code}")
    @RequiresRoles("wx")
    public Result<List<PlatformCalendar>> calendar(@PathVariable long code,
                                                   Date start, Date end) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TGroupProduct product = groupProductMapper.selectOne((TGroupProduct) new TGroupProduct().setMerchantId(merchant.getId()).setSubCode(code));
        Assert.notNull(product, "产品不存在");
        start = start == null ? DateUtil.beginOfDay(new Date()) : start;
        end = end == null ? DateUtil.endOfMonth(new Date()) : end;
        List<PlatformCalendar> calendars = platformManager.productApi(merchant).calendar(merchant, code, start, end);
        if (calendars != null) {
            calendars.forEach(c -> c.setPrice(product.getPrice()).put(TGroupProduct::getGroupPrice, product));
        }
        return Result.ok(calendars);
    }

    private List<TGroupProduct> sale(List<TGroupProduct> products, TMerchant merchant) {
        // 获取当前页的主产品ID集合
        Set<Integer> ids = products.stream().map(TGroupProduct::getProductId).collect(Collectors.toSet());
        // 查询当前页主产品的所有子产品列表
        List<TGroupProduct> list = CollectionUtils.isEmpty(ids) ? Collections.emptyList() : groupProductMapper.selectByExample(new Example.Builder(TLimitProduct.class)
                .where(WeekendSqls.<TGroupProduct>custom().andIn(TGroupProduct::getProductId, ids))
                .build()
        );
        // 获取最低拼团价集合<主产品ID, 最低拼团价>
        Map<Integer, Integer> priceMap = list.stream().collect(Collectors.groupingBy(
                TGroupProduct::getProductId,
                Collectors.collectingAndThen(
                        Collectors.collectingAndThen(
                                Collectors.minBy(Comparator.comparingInt(TGroupProduct::getGroupPrice)),
                                Optional::get
                        ),
                        TGroupProduct::getGroupPrice
                )
        ));
        // 获取子产品编号集合
        Set<Long> codes = list.stream().map(TGroupProduct::getSubCode).collect(Collectors.toSet());
        // 按子产品获取主产品的最小价格
        List<ProductMinPrice> prices = platformManager.productApi(merchant).minPrice(merchant, new Date(), codes);

        products.forEach(p -> p.setGroupPrice(priceMap.get(p.getProductId()))
                .setSale(prices.stream().anyMatch(ps -> ps.getId() == p.getProductId())));
        return products;
    }

    private void check(List<TGroupProduct> products) {
        check(products.stream().map(TGroupProduct::getId).collect(Collectors.toSet()));
    }
    private void check(Collection<Integer> collection) {
        String ids = collection.stream().map(String::valueOf).collect(Collectors.joining(","));
        List<TGroupProduct> list = groupProductMapper.selectByIds(ids);
        if (list == null || list.size() != collection.size()) {
            throw new RuntimeException("产品不存在");
        }
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        list.forEach(p -> {
            if (merchant.getId().intValue() != p.getMerchantId()) {
                throw new RuntimeException("权限不足");
            }
        });
    }
}
