package com.all580.mshop.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.*;
import com.all580.mshop.manager.OrderManager;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TOrderMapper;
import com.all580.mshop.mapper.TRefundOrderMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 14:04
 */
@Api(tags = "订单接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/order")
@Slf4j
public class OrderController {
    @Resource
    private TOrderMapper orderMapper;
    @Resource
    private TRefundOrderMapper refundOrderMapper;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private WxManager wxManager;
    @Autowired
    private PlatformManager platformManager;

    @ApiOperation(value = "下单", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("create/{type}")
    @RequiresRoles("wx")
    public Result<?> create(@ApiParam(required = true, value = "类型:1-返利;2-限购") @PathVariable int type,
                            @ApiParam(required = true) @RequestBody Map<String, Object> params,
                            @ApiParam(required = true) @RequestParam long code,
                            @ApiParam(value = "来源:推荐码") @RequestParam(required = false, name = "_from") String from) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        BasicProduct p = orderManager.check(merchant, type, code, member.getId(), params);
        TMember fromMember = orderManager.parseFromMember(merchant, from);
        if (fromMember != null && fromMember.getId().intValue() == member.getId()) {
            fromMember = null;
        }

        Result<?> result = platformManager.orderApi(merchant).create(merchant, params, p);
        if (!result.isSuccess()) {
            return result;
        }
        OrderTmp orderTmp = platformManager.orderApi(merchant).transformOrder(merchant, params, result, true);
        Assert.notNull(orderTmp, "下单失败");

        int money = p.getPrice() * orderTmp.getQuantity() * orderTmp.getDays();
        TOrder order = new TOrder()
                .setStatus(Constant.OrderStatus.PENDING_PAY)
                .setCreateTime(new Date())
                .setMerchantId(merchant.getId())
                .setMemberId(member.getId())
                .setFromMember(fromMember == null ? member.getPid() : fromMember.getId())
                .setOrderNumber(orderTmp.getNumber())
                .setItemNumber(orderTmp.getItemNumber())
                .setOriginalMoney(orderTmp.getMoney())
                .setMoney(money)
                .setProductId(p.getProductId())
                .setSubCode(code)
                .setProductName(orderTmp.getName())
                .setSubName(orderTmp.getSubName())
                .setType(type)
                .setProductType(p.getType())
                .setShopProductId(p.getId())
                .setQuantity(orderTmp.getQuantity())
                .setDays(orderTmp.getDays())
                .setBooking(orderTmp.getBooking())
                .setRebate(p instanceof TRebateProduct ? (int) (((TRebateProduct) p).getRebateMoney(money)) : 0)
                .setRefundRule(orderTmp.getRefundRule())
                .setContact(orderTmp.getContact())
                .setContactPhone(orderTmp.getContactPhone())
                .setFormData(JSONUtil.toJsonStr(params));
        int ret = orderMapper.insertSelective(order);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).value(orderTmp.setMoney(order.getMoney())).build();
    }

    @ApiOperation(value = "支付", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("pay/{number}")
    @RequiresRoles("wx")
    @Locks("#{#number}")
    public Result<?> pay(@ApiParam(required = true) @PathVariable long number,
                         HttpServletRequest request) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TOrder order = orderMapper.selectOne(new TOrder().setOrderNumber(number));
        Assert.notNull(order, "订单不存在");
        Assert.isTrue(member.getId().intValue() == order.getMemberId(), "权限不足");
        Assert.isTrue(Constant.OrderStatus.PAID != order.getStatus(), "该订单已支付");
        Assert.isTrue(Constant.OrderStatus.CANCEL != order.getStatus(), "该订单已取消");
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        if (config == null) {
            return Result.fail("未配置微信支付信息,请联系商家");
        }

        boolean checkPay = orderManager.processOrderStatus(order, config);
        if (checkPay) {
            return Result.ok();
        }

        Result<Boolean> result = platformManager.orderApi(merchant).canPayment(merchant, order);
        if (!result.isSuccess()){
            return result;
        }
        if (!result.getValue()) {
            return Result.fail("当前订单不能支付:P10006");
        }

        // 更新状态
        int ret = orderMapper.updateByPrimaryKeySelective(new TOrder().setId(order.getId()).setStatus(Constant.OrderStatus.PAYING));
        if (ret <= 0) {
            return Result.fail();
        }

        return orderManager.pay(order, config, member, request);
    }

    @ApiOperation(value = "获取会员自己订单列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型:1-返利;2-限购"),
            @ApiImplicitParam(name = "status", value = "类型:1-待支付;2-支付中;3-已支付,逗号分隔"),
            @ApiImplicitParam(name = "productType", value = "产品类型"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list/member")
    @RequiresRoles("wx")
    public Result<?> listByMember(Integer type, String status, Integer productType, Integer pageNum, Integer pageSize) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        List<PlatformOrderInfo> list = orderManager.list(merchant,
                new SearchOrder()
                        .setType(type)
                        .setMemberId(member.getId())
                        .setMerchantId(merchant.getId())
                        .setStatus(StrUtil.isNotBlank(status) ? Arrays.stream(status.split(",")).map(Integer::valueOf).collect(Collectors.toSet()) : null)
                        .setProductType(productType)
                , pageNum, pageSize
        );
        if (list instanceof Page) {
            return Result.ok(PageInfo.of(list));
        }
        return Result.ok(list);
    }

    @ApiOperation(value = "获取订单列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(@ApiParam SearchOrder searchOrder, Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        List<PlatformOrderInfo> list = orderManager.list(merchant, searchOrder, pageNum, pageSize);
        if (list instanceof Page) {
            return Result.ok(PageInfo.of(list));
        }
        return Result.ok(list);
    }

    @ApiOperation(value = "获取订单详情", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("detail/{number}")
    @RequiresRoles(value = { "wx", "merchant" }, logical = Logical.OR)
    public Result<PlatformItemDetail> detail(@ApiParam(required = true, value = "子订单编号") @PathVariable long number) {
        Object principal = SecurityUtils.getSubject().getPrincipal();
        TMember member = principal instanceof WxUser ? ((WxUser) principal).getMember() : null;
        TMerchant merchant = principal instanceof WxUser ? ((WxUser) principal).getMerchant() : (TMerchant) principal;
        TOrder order = orderMapper.selectOne(new TOrder()
                .setItemNumber(number)
                .setMemberId(member == null ? null : member.getId())
                .setMerchantId(merchant.getId())
        );
        Assert.notNull(order, "订单不存在");
        return Result.ok(platformManager
                .orderApi(merchant)
                .detail(merchant, order)
                .put(TOrder::getMoney, order)
                .put(TOrder::getProductName, order)
                .put(TOrder::getSubName, order)
                .put(TOrder::getProductType, order)
                .put(TOrder::getType, order)
                .put(TOrder::getDays, order)
                .put(TOrder::getQuantity, order)
                .put(TOrder::getRefundRule, order)
                .put(TOrder::getPayTime, order)
                .put(TOrder::getStatus, order)
                .put(TOrder::getOrderNumber, order)
                .put(TOrder::getItemNumber, order)
                .put(TOrder::getContact, order)
                .put(TOrder::getContactPhone, order)
                .put(TOrder::getCreateTime, order)
        );
    }

    @ApiOperation(value = "退订预览", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("refund/preview/{number}")
    @RequiresRoles("wx")
    public Result<PlatformRefundPreview> refundPreview(@ApiParam(required = true) @PathVariable long number) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TOrder order = orderMapper.selectOne(new TOrder().setItemNumber(number).setMemberId(member.getId()));
        Assert.notNull(order, "订单不存在");
        PlatformRefundPreview preview = platformManager.orderApi(merchant).refundPreview(merchant, order);
        return Result.ok(preview
                .put("fee", platformManager
                        .orderApi(merchant)
                        .calcRefundFee(merchant, order, new Date(),
                                order.getQuantity() * order.getDays() - preview.getUsedQuantity() - preview.getRefundQuantity()))
                .put(TOrder::getMoney, order)
                .put(TOrder::getProductName, order)
                .put(TOrder::getSubName, order)
                .put(TOrder::getProductType, order)
                .put(TOrder::getDays, order)
                .put(TOrder::getQuantity, order)
                .put(TOrder::getRefundRule, order)
                .put(TOrder::getItemNumber, order)
                .put(TOrder::getCreateTime, order)
        );
    }

    @ApiOperation(value = "退订", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("refund/{number}")
    @RequiresRoles("wx")
    @Locks("#{#number}")
    public Result<?> refund(@ApiParam(required = true) @PathVariable long number,
                            @ApiParam(required = true) @RequestBody Map<String, Object> params) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMerchant merchant = wxUser.getMerchant();
        TMember member = wxUser.getMember();
        TOrder order = orderMapper.selectOne(new TOrder().setItemNumber(number).setMemberId(member.getId()));
        Assert.notNull(order, "订单不存在");
        Result<PlatformRefundResult> result = platformManager.orderApi(merchant).refund(merchant, order, params);
        if (!result.isSuccess()) {
            return result;
        }
        PlatformRefundResult vo = result.getValue();
        int fee = platformManager.orderApi(merchant).calcRefundFee(merchant, order, vo.getCreateTime(), vo.getQuantity());
        if (fee > order.getMoney()) {
            return Result.fail("退订失败:手续费大于支付金额");
        }
        refundOrderMapper.insertSelective(new TRefundOrder()
                .setCreateTime(vo.getCreateTime())
                .setFee(fee)
                .setOriginalFee(vo.getFee())
                .setItemNumber(order.getItemNumber())
                .setOrderNumber(order.getOrderNumber())
                .setMemberId(member.getId())
                .setMerchantId(merchant.getId())
                .setPayAmount(order.getMoney())
                .setMoney(order.getMoney() - fee)
                .setNumber(vo.getNumber())
                .setQuantity(vo.getQuantity())
                .setCause(vo.getCause())
                .setStatus(Constant.RefundOrderStatus.PENDING)
        );
        return Result.ok();
    }

    @ApiOperation(value = "待退款列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("refund/list")
    @RequiresRoles("merchant")
    public Result<?> refundList(@ApiParam SearchRefund searchRefund,
                                Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> refundOrderMapper.search(searchRefund.setMerchantId(merchant.getId())));
        } else {
            value = refundOrderMapper.search(searchRefund.setMerchantId(merchant.getId()));
        }
        return Result.ok(value);
    }

    @ApiOperation(value = "退款确认", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("refund/confirm/{number}")
    @RequiresRoles("merchant")
    @Locks("#{#number}")
    public Result<?> refundConfirm(@ApiParam(value = "退订编号", required = true) @PathVariable long number) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        boolean isRefund = platformManager.orderApi(merchant).isRefund(merchant, number);
        if (!isRefund) {
            return Result.fail("该订单暂未完成");
        }
        return orderManager.refundConfirm(merchant, number);
    }
}
