package com.all580.mshop.controller;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.SystemConfig;
import com.all580.mshop.dto.WxUser;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.manager.AliyunSmsManager;
import com.all580.mshop.mapper.TMemberMapper;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/6 13:35
 */
@Api(tags = "会员(推广员)接口")
@RestController
@CrossOrigin(allowCredentials = "true")
@RequestMapping("api/member")
@Slf4j
public class MemberController {
    @Resource
    private TMemberMapper memberMapper;
    @Autowired
    private AliyunSmsManager aliyunSmsManager;
    @Autowired
    private SystemConfig systemConfig;

    @ApiOperation(value = "获取会员列表", notes = "如果传入page参数则返回value为PageInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickname", value = "昵称"),
            @ApiImplicitParam(name = "phone", value = "手机号码"),
            @ApiImplicitParam(name = "start", value = "注册时间-开始"),
            @ApiImplicitParam(name = "end", value = "注册时间-结束"),
            @ApiImplicitParam(name = "pageNum", value = "页数"),
            @ApiImplicitParam(name = "pageSize", value = "页总数"),
    })
    @GetMapping("list")
    @RequiresRoles("merchant")
    public Result<?> list(String nickname, String phone, Date start, Date end, Integer pageNum, Integer pageSize) {
        TMerchant merchant = (TMerchant) SecurityUtils.getSubject().getPrincipal();
        Object value;
        WeekendSqls<TMember> custom = WeekendSqls.custom();
        custom.andEqualTo(TMember::getMerchantId, merchant.getId());
        if (StrUtil.isNotBlank(nickname)) {
            custom.andLike(TMember::getNickname, "%" + nickname + "%");
        }
        if (StrUtil.isNotBlank(phone)) {
            custom.andLike(TMember::getPhone, "%" + phone + "%");
        }
        if (start != null) {
            custom.andGreaterThanOrEqualTo(TMember::getCreateTime, start);
        }
        if (end != null) {
            custom.andLessThanOrEqualTo(TMember::getCreateTime, end);
        }
        Example example = Example.builder(TMember.class).where(custom).orderByDesc("id").build();
        if (pageNum != null && pageSize != null) {
            value = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> memberMapper.selectByExample(example));
        } else {
            value = memberMapper.selectByExample(example);
        }
        return Result.builder().code(Result.SUCCESS).value(value).build();
    }

    @ApiOperation(value = "发送绑定验证码", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("code/send")
    @RequiresRoles("wx")
    public Result<?> send(@ApiParam(required = true, value = "手机号") @RequestParam String mobile) {
        if (!Validator.isMobile(mobile)) {
            return Result.fail("手机号码有误");
        }
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        int count = memberMapper.selectCount(new TMember().setPhone(mobile).setMerchantId(wxUser.getMerchant().getId()));
        if (count > 0) {
            return Result.fail("该手机号码已被绑定");
        }
        String openid = wxUser.getMember().getOpenid();
        String code = Constant.STRING_CACHE.get(openid, false);
        if (!StrUtil.isBlank(code)) {
            return Result.fail("请稍候再试");
        }
        code = RandomUtil.randomNumbers(6);
        Result<?> result = aliyunSmsManager.send(systemConfig.getRegSmsCode(), Collections.singletonMap("code", code), mobile);
        if (!result.isSuccess()) {
            return result;
        }
        log.info("验证码: {} - {}", mobile, code);
        Constant.STRING_CACHE.put(openid, code + ":" + mobile, TimeUnit.MINUTES.toMillis(1));
        return Result.ok();
    }

    @ApiOperation(value = "绑定手机号", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("bind/{code}")
    @RequiresRoles("wx")
    public Result<?> bind(@ApiParam(required = true, value = "验证码") @PathVariable String code,
                          @ApiParam(required = true, value = "手机号") @RequestParam String mobile) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        TMember member = wxUser.getMember();
        String old = Constant.STRING_CACHE.get(member.getOpenid(), false);
        if (old == null) {
            return Result.fail("验证码错误");
        }
        String[] arr = old.split(":");
        if (!arr[0].equalsIgnoreCase(code)) {
            return Result.fail("验证码错误");
        }
        if (!arr[1].equals(mobile)) {
            return Result.fail("手机号码不匹配");
        }
        try {
            int ret = memberMapper.updateByPrimaryKeySelective(new TMember().setPhone(mobile).setId(member.getId()));
            return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
        } catch (DuplicateKeyException e) {
            return Result.fail("该手机号码已被绑定");
        }
    }

    @ApiOperation(value = "会员自己修改信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("modify")
    @RequiresRoles("wx")
    public Result<?> modify(@ApiParam(required = true) @RequestBody TMember member) {
        WxUser wxUser = (WxUser) SecurityUtils.getSubject().getPrincipal();
        member.setId(wxUser.getMember().getId());
        return updateMember(member);
    }

    @ApiOperation(value = "修改会员信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("update")
    public Result<?> update(@ApiParam(required = true) @RequestBody TMember member) {
        return updateMember(member);
    }

    private Result<?> updateMember(@RequestBody @ApiParam(required = true) TMember member) {
        member.setBalance(null);
        member.setRebate(null);
        member.setOpenid(null);
        member.setMerchantId(null);
        member.setPhone(null);
        member.setPid(null);
        int ret = memberMapper.updateByVersion(member);
        return Result.builder().code(ret > 0 ? Result.SUCCESS : Result.FAIL).build();
    }
}
