package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/17 14:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PlatformRefundResult extends ExpandData {
    @ApiModelProperty("退订编号")
    private long number;
    @ApiModelProperty("退订数量")
    private int quantity;
    @ApiModelProperty("手续费")
    private int fee;
    @ApiModelProperty("退订原因")
    private String cause;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
