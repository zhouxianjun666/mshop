package com.all580.mshop.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/17 17:29
 */
@Data
@Configuration
@ConfigurationProperties(value = "oss")
public class OssConfig {
    private String region;
    private String endpoint;
    private String stsEndpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String roleArn;
    private String policy;
    private String imgBucket;
}
