package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 11:10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LineProductInfo extends PlatformProductInfo {
    @ApiModelProperty("出发地点")
    private String startArea;
    @ApiModelProperty("标签")
    private List<String> label;
    @ApiModelProperty("集合地点")
    private String setArea;
    @ApiModelProperty("几天几晚")
    private String days;
    @ApiModelProperty("费用包含")
    private String costContain;
    @ApiModelProperty("费用不包含")
    private String costNotContain;
    @ApiModelProperty("退款说明")
    private String refundExplain;
    @ApiModelProperty("出游前须知")
    private String outingBefore;
    @ApiModelProperty("出游后须知")
    private String outingAfter;
}
