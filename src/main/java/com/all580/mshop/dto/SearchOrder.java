package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/19 16:15
 */
@Data
@Accessors(chain = true)
public class SearchOrder {
    @ApiModelProperty(readOnly = true, hidden = true)
    private Integer merchantId;
    @ApiModelProperty("会员编号")
    private Integer memberId;
    @ApiModelProperty("会员昵称")
    private String memberNickname;
    @ApiModelProperty("会员手机号")
    private String memberPhone;
    @ApiModelProperty("推广员昵称")
    private String fromNickname;
    @ApiModelProperty("推广员手机号")
    private String fromPhone;
    @ApiModelProperty("产品名称")
    private String productName;
    @ApiModelProperty("类型: 1-返利;2-限购;")
    private Integer type;
    @ApiModelProperty("产品类型")
    private Integer productType;
    @ApiModelProperty("订单状态: 1-待支付;2-支付中;3-已支付")
    private Collection<Integer> status;
    @ApiModelProperty("联系人")
    private String contact;
    @ApiModelProperty("联系人手机号")
    private String contactPhone;
    @ApiModelProperty("开始时间")
    private Date start;
    @ApiModelProperty("结束时间")
    private Date end;
}
