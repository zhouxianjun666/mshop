package com.all580.mshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 14:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(description = "销售日历")
public class PlatformCalendar extends ExpandData implements Serializable {
    @ApiModelProperty("最低售价")
    private Integer minSellPrice;
    @ApiModelProperty("价格")
    private int price;
    @ApiModelProperty("市场价")
    private Integer marketPrice;
    @ApiModelProperty("当天开始时间")
    private Date start;
    @ApiModelProperty("结束时间")
    private Date end;
    @ApiModelProperty("库存")
    private int stock;
    @ApiModelProperty("已售")
    private int saleQuantity;
    @ApiModelProperty("已退")
    private int refundQuantity;
}
