package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import tk.mybatis.mapper.weekend.Fn;
import tk.mybatis.mapper.weekend.reflection.Reflections;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/14 10:46
 */
public class ExpandData implements Serializable {
    @ApiModelProperty("扩展数据")
    @Getter
    @Setter
    private Map<String, Object> data = new HashMap<>(0);
    public <T extends ExpandData> T put(String key, Object val) {
        this.data.put(key, val);
        return (T) this;
    }

    public <T extends ExpandData, R> T put(Fn<R, Object> fn, R bean) {
        return put(Reflections.fnToFieldName(fn), fn.apply(bean));
    }
}
