package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/17 10:52
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PlatformRefundPreview extends ExpandData {
    @ApiModelProperty("已使用数量")
    private int usedQuantity;
    @ApiModelProperty("已退订数量")
    private int refundQuantity;
    @ApiModelProperty("过期张数")
    private int expiredQuantity;
}
