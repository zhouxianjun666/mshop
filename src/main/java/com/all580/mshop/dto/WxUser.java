package com.all580.mshop.dto;

import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import lombok.*;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/9 13:59
 */
@EqualsAndHashCode(callSuper = true)
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class WxUser extends AbstractUser {
    @NonNull
    private TMerchant merchant;
    @NonNull
    private TMember member;

    @Override
    public String getUsername() {
        return getMember().getOpenid();
    }

    @Override
    public String getPassword() {
        return new SimpleHash("MD5", getUsername(), getSalt()).toString();
    }

    @Override
    public List<String> getRoles() {
        return Stream.of("wx").collect(Collectors.toList());
    }
}
