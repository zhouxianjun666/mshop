package com.all580.mshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 13:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "平台子产品")
public class PlatformProductItem extends ExpandData implements Serializable {
    @ApiModelProperty("编号")
    private long code;
    @ApiModelProperty("进货价")
    private int purchasePrice;
    @ApiModelProperty("主产品ID")
    private int productId;
    @ApiModelProperty("id")
    private int id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("主产品名称")
    private String productName;
    @ApiModelProperty("主产品图片")
    private List<String> img;
    @ApiModelProperty("子产品图片")
    private List<String> subImg;
    @ApiModelProperty("供应商")
    private String supplier;
}
