package com.all580.mshop.dto;

import javax.validation.groups.Default;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/10 13:34
 */
public interface UpdateGroup extends Default {
}
