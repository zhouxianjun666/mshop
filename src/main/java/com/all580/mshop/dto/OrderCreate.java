package com.all580.mshop.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 17:48
 */
@Data
@Accessors(chain = true)
public class OrderCreate implements Serializable {
    private long number;
    private long itemNumber;
    private String subName;
    private String name;
    private int quantity;
    private int money;
}
