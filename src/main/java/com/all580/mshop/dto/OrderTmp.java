package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 16:47
 */
@Data
@Accessors(chain = true)
public class OrderTmp implements Serializable {
    @ApiModelProperty("订单编号")
    private long number;
    @ApiModelProperty("子订单编号")
    private long itemNumber;
    @ApiModelProperty("金额")
    private int money;
    @ApiModelProperty("张数")
    private int quantity;
    @ApiModelProperty("天数")
    private int days;
    @ApiModelProperty("产品名称")
    private String name;
    @ApiModelProperty("子产品名称")
    private String subName;
    @ApiModelProperty("游玩日期")
    private Date booking;
    @ApiModelProperty("退订规则")
    private String refundRule;
    @ApiModelProperty("订单联系人")
    private String contact;
    @ApiModelProperty("订单联系人手机号")
    private String contactPhone;
}
