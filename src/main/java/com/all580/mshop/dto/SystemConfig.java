package com.all580.mshop.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/30 14:08
 */
@Data
@Configuration
@ConfigurationProperties(value = "config")
public class SystemConfig {
    private String regSmsCode;
    private String domain;
    private int groupExpiredHours;
    private String orderPaidWxTemplate;
    private String orderRebateWxTemplate;
}
