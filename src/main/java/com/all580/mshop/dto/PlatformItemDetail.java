package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/14 10:30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class PlatformItemDetail extends ExpandData implements Serializable {
    @ApiModelProperty("订单状态码")
    private int status;
    @ApiModelProperty("订单状态名称")
    private String statusName;
    @ApiModelProperty("支付时间")
    private Date payTime;
    @ApiModelProperty("子订单状态码")
    private int itemStatus;
    @ApiModelProperty("订单状态名称")
    private String statusDesc;
    @ApiModelProperty("子订单状态名称")
    private String itemStatusName;
    @ApiModelProperty("开始时间")
    private Date start;
    @ApiModelProperty("结束时间")
    private Date end;
    @ApiModelProperty("最小生效日期")
    private Date minEffectiveDate;
    @ApiModelProperty("最大失效日期")
    private Date maxExpiryDate;
    @ApiModelProperty("已使用数量")
    private int usedQuantity;
    @ApiModelProperty("已退订数量")
    private int refundQuantity;
    @ApiModelProperty("出发地")
    private String placeOfDeparture;
    @ApiModelProperty("目的地")
    private String destination;
    @ApiModelProperty("天数(白天)")
    private int dayCount;
    @ApiModelProperty("几晚")
    private int nights;
    @ApiModelProperty("订单联系人")
    private String name;
    @ApiModelProperty("订单联系人手机")
    private String phone;
    @ApiModelProperty("订单联系人身份证")
    private String sid;
    @ApiModelProperty("过期张数")
    private int expiredQuantity;
    @ApiModelProperty("退订状态")
    private int refundStatus;
    @ApiModelProperty("游客信息")
    private List<PlatformVisitor> visitors;
}
