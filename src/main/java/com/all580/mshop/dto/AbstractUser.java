package com.all580.mshop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/9 13:55
 */
public abstract class AbstractUser {
    /**
     * 用户名
     * @return
     */
    public abstract String getUsername();

    /**
     * 密码(加密后)
     * @return
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public abstract String getPassword();

    /**
     * 盐值
     * @return
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getSalt() {
        return getUsername();
    }
    @Transient
    @Getter
    private List<String> roles = new ArrayList<>(0);
}
