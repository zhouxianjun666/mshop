package com.all580.mshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 14:12
 */
@Data
@Accessors(chain = true)
@ApiModel("平台产品信息")
public class PlatformProductInfo implements Serializable {
    @ApiModelProperty("主产品ID")
    private int id;
    @ApiModelProperty("主产品名称")
    private String name;
    @ApiModelProperty("主产品图片")
    private List<String> img;
    @ApiModelProperty("地址")
    private String address;
    @ApiModelProperty("概要")
    private String summary;
    @ApiModelProperty("等级")
    private Integer level;
    @ApiModelProperty("地理位置坐标")
    private String map;
    @ApiModelProperty("子产品信息列表")
    private List<PlatformSubInfo> subInfo;
}
