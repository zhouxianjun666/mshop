package com.all580.mshop.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/7 17:43
 */
@Data
@Configuration
@ConfigurationProperties(value = "sms.aliyun")
public class AliyunSms {
    private String accessId;
    private String accessKey;
    private String region;
    private String sign;
}
