package com.all580.mshop.dto;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/29 16:56
 */
public interface Order {
    Integer getMoney();
    String getTransactionId();
    Long getNumber();
    String getProductName();
    String getSubName();
    Long getSubCode();
    Integer getMerchantId();
    Integer getStatus();
    default String getAttach() {
        return null;
    }
}
