package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/14 10:29
 */
@Data
@Accessors(chain = true)
public class PlatformVisitor implements Serializable {
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("身份证")
    private String sid;
    @ApiModelProperty("性别")
    private int sex;
}
