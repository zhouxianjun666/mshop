package com.all580.mshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 14:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel("平台子产品信息")
public class PlatformSubInfo extends ExpandData implements Serializable {
    @ApiModelProperty("ID")
    private int id;
    @ApiModelProperty("编号")
    private long code;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("票据字典: 成人票/儿童票")
    private int ticketDict;
    @ApiModelProperty("票据类型：次票/月/年")
    private int ticketType;
    @ApiModelProperty("市场价")
    private int marketPrice;
    @ApiModelProperty("预定须知")
    private String bookingNotes;
    @ApiModelProperty("使用须知")
    private String useNotes;
    @ApiModelProperty("图片")
    private List<String> img;
    @ApiModelProperty("价格")
    private int price;
    @ApiModelProperty("身份证限制")
    private int requireSid;
    @ApiModelProperty("身份证一天购买次数")
    private int sidDayCount;
    @ApiModelProperty("身份证一天购买张数")
    private int sidDayQuantity;
    @ApiModelProperty("预定时间是否限制")
    private int bookingLimit;
    @ApiModelProperty("预定时间限制天(多少天之前预定)")
    private int bookingDayLimit;
    @ApiModelProperty("预定时间限制时间(某个时间之前预定)00:00")
    private String bookingTimeLimit;
}
