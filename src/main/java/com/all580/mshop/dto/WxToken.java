package com.all580.mshop.dto;

import lombok.Getter;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/9 13:54
 */
public class WxToken extends UsernamePasswordToken {
    @Getter
    private String mch;
    public WxToken(String username, String password, String mch) {
        super(username, password);
        this.mch = mch;
    }
}
