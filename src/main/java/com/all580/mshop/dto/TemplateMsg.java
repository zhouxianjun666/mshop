package com.all580.mshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/22 15:38
 */
@Data
@Accessors(chain = true)
public class TemplateMsg {
    @NonNull
    private String touser;
    @NonNull
    private String template_id;
    private String url;
    private String topcolor;
    private TemplateMiniprogram miniprogram;
    private final Map<String, TemplateDataItem> data = new HashMap<>(5);
    @JsonIgnore
    private final Map<String, String> transform = new HashMap<>(5);

    public static TemplateMsg builder(String touser, String templateId) {
        return new TemplateMsg(touser, templateId);
    }

    public static TemplateMsg builder(String touser) {
        return new TemplateMsg(touser, "");
    }

    public TemplateMsg mapData(Map<String, ?> data) {
        data.forEach((key, value) -> this.add(transform.getOrDefault(key, key), Optional.ofNullable(value).map(Object::toString).orElse(null)));
        return this;
    }

    public TemplateMsg transform(String old, String key) {
        transform.put(old, key);
        return this;
    }

    public TemplateMsg add(String key, String value) {
        data.put(key, new TemplateDataItem().setValue(value));
        return this;
    }

    public TemplateMsg add(String key, String value, String color) {
        data.put(key, new TemplateDataItem().setValue(value).setColor(color));
        return this;
    }

    public <K, V> Map<K, V> toMap(Function<? super Map.Entry<String, TemplateDataItem>, ? extends K> keyMapper,
                                    Function<? super Map.Entry<String, TemplateDataItem>, ? extends V> valueMapper) {
        return data.entrySet().stream().collect(Collectors.toMap(keyMapper, valueMapper));
    }
}
