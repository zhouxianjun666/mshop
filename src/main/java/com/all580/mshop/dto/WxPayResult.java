package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/13 11:14
 */
@Data
@Accessors(chain = true)
public class WxPayResult {
    @ApiModelProperty("签名")
    private String sign;
    @ApiModelProperty("随机串")
    private String nonceStr;
    @ApiModelProperty("时间戳")
    private long timestamp;
    @ApiModelProperty("应用ID")
    private String appId;
    @ApiModelProperty("预支付ID")
    private String prepayId;
    @ApiModelProperty("二维码链接")
    private String codeUrl;
}
