package com.all580.mshop.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/22 15:42
 */
@Data
@Accessors(chain = true)
public class TemplateDataItem {
    private String value;
    private String color = "#173177";
}
