package com.all580.mshop.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 15:42
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "平台拉取产品结果")
public class PlatformProductResult implements Serializable {
    List<PlatformProductItem> list;
    int total;
}
