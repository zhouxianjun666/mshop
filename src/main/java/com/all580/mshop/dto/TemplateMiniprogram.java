package com.all580.mshop.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/22 16:02
 */
@Data
@Accessors(chain = true)
public class TemplateMiniprogram {
    private String appid;
    private String pagepath;
}
