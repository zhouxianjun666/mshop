package com.all580.mshop.dto;

import cn.hutool.core.util.StrUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.entity.TMember;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.mapper.TMemberMapper;
import com.all580.mshop.mapper.TMerchantMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import javax.annotation.Resource;
import java.util.HashSet;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/30 10:59
 */
@Slf4j
public class UserRealm extends AuthorizingRealm {
    @Resource
    private TMerchantMapper merchantMapper;
    @Resource
    private TMemberMapper memberMapper;

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        //md5加密默认1次
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        super.setCredentialsMatcher(hashedCredentialsMatcher);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        if (principalCollection == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        AbstractUser user = (AbstractUser) getAvailablePrincipal(principalCollection);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setRoles(new HashSet<>(user.getRoles()));
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        if (StrUtil.isBlank(token.getUsername())) {
            throw new AccountException("用户名不能为空");
        }

        AbstractUser user;
        if (token instanceof WxToken) {
            WxToken wxToken = (WxToken) token;
            TMerchant merchant = merchantMapper.selectOne(new TMerchant().setAccessId(wxToken.getMch()));
            if (merchant == null) {
                throw new AccountException("商户不存在");
            }
            TMember member = memberMapper.selectOne(new TMember().setOpenid(token.getUsername()).setMerchantId(merchant.getId()));
            if (member == null) {
                throw new AccountException("会员不存在");
            }
            if (member.getStatus() == Constant.MemberStatus.DISABLED) {
                throw new DisabledAccountException("该会员已被禁用");
            }
            user = new WxUser(merchant, member);
        } else {
            user = merchantMapper.selectOne(new TMerchant().setPhone(token.getUsername()));
            if (user == null) {
                throw new AuthenticationException("用户不存在");
            }
        }

        return new SimpleAuthenticationInfo(user, user.getPassword(), ByteSource.Util.bytes(user.getUsername()), getName());
    }
}
