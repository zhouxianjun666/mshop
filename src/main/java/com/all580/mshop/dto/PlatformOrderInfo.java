package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/14 10:20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class PlatformOrderInfo extends ExpandData implements Serializable {
    @ApiModelProperty("订单编号")
    private long number;
    @ApiModelProperty("订单状态码")
    private int status;
    @ApiModelProperty("子订单状态码")
    private int itemStatus;
    @ApiModelProperty("订单状态")
    private String statusDesc;
    @ApiModelProperty("订单状态名称")
    private String statusName;
    @ApiModelProperty("子订单编号")
    private long itemNumber;
    @ApiModelProperty("最小生效日期")
    private Date minEffectiveDate;
    @ApiModelProperty("最大失效日期")
    private Date maxExpiryDate;
    @ApiModelProperty("已使用数量")
    private int usedQuantity;
    @ApiModelProperty("已退订数量")
    private int refundQuantity;
}
