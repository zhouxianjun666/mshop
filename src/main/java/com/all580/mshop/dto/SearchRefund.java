package com.all580.mshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/20 11:41
 */
@Data
@Accessors(chain = true)
public class SearchRefund {
    @ApiModelProperty(readOnly = true, hidden = true)
    private Integer merchantId;
    @ApiModelProperty(value = "会员手机号")
    private String memberPhone;
    @ApiModelProperty(value = "会员昵称")
    private String memberNickname;
    @ApiModelProperty(value = "会员ID")
    private Integer memberId;
    @ApiModelProperty(value = "类型:1-返利;2-限购")
    private Integer type;
    @ApiModelProperty(value = "产品类型")
    private Integer productType;
    @ApiModelProperty(value = "产品名称")
    private String productName;
    @ApiModelProperty(value = "订单号")
    private Long number;
    @ApiModelProperty(value = "时间查询类型:1-下单时间;2-退订时间")
    private Integer timeType;
    @ApiModelProperty(value = "状态: 1-退订中;2-退款中;3-退订成功")
    private Integer status;
    @ApiModelProperty(value = "开始时间")
    private Date start;
    @ApiModelProperty(value = "结束时间")
    private Date end;
}
