package com.all580.mshop.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/10 16:38
 */
@Data
@Accessors(chain = true)
public class ProductMinPrice implements Serializable {
    private int id;
    private int price;
    private long code;
}
