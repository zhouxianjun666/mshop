package com.all580.mshop.dto;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/5/11 10:00
 */
@Data
@Builder
@Accessors(chain = true)
@ApiModel("返回结果")
public class Result<E> implements Serializable {
    private static final long serialVersionUID = 816334571679000487L;
    /**当前操作是否成功*/
    @ApiModelProperty("是否成功")
    private boolean success;
    /**返回文本信息*/
    @ApiModelProperty("描述信息")
    private String msg;
    @ApiModelProperty("操作代码")
    @Builder.Default
    private int code = SUCCESS;
    /**附加信息*/
    @ApiModelProperty("附加信息")
    private Map<String, Object> data;
    @ApiModelProperty("返回值")
    private E value;

    public final static int SUCCESS = 200;
    public final static int FAIL = 0;
    public final static int PARAM_FAIL = 400;
    public final static int UNAUTHORIZED = 403;
    public final static int NO_LOGIN = 99;
    public final static int HAS_REFUND = 1001;
    public final static int UNIQUE_KEY_ERROR = 10003;

    public boolean isSuccess() {
        return code == SUCCESS;
    }

    private void setSuccess(boolean success) {}

    public Result<E> put(String key, Object value){
        getData().put(key, value);
        return this;
    }

    public Result<E> putAll(Map<String, Object> data){
        getData().putAll(data);
        return this;
    }

    public Result<E> value(E value) {
        this.value = value;
        return this;
    }

    public Map<String, Object> getData() {
        if (data == null) {
            data = new HashMap<>(1);
        }
        return data;
    }

    @SuppressWarnings("unchecked")
    @JsonIgnore
    public <T> T get(String key){
        return (T) getData().get(key);
    }

    public static <T> Result<T> ok() {
        return Result.<T>builder().code(SUCCESS).build();
    }

    public static <T> Result<T> ok(T t) {
        return Result.<T>builder().code(SUCCESS).value(t).build();
    }

    public static <T> Result<T> ok(String msg) {
        return Result.<T>builder().code(SUCCESS).msg(msg).build();
    }

    public static <T> Result<T> fail() {
        return Result.<T>builder().code(FAIL).msg("操作失败").build();
    }
    public static <T> Result<T> fail(int code) {
        return Result.<T>builder().code(code).msg("操作失败").build();
    }
    public static <T> Result<T> fail(String msg) {
        return Result.<T>builder().code(FAIL).msg(msg).build();
    }
    public static <T> Result<T> fail(int code, String msg) {
        return Result.<T>builder().code(code).msg(msg).build();
    }

    @SneakyThrows
    public void write() {
        HttpServletResponse res = cors();
        Assert.notNull(res, "no response");
        try (PrintWriter writer = res.getWriter()) {
            writer.write(JSONUtil.toJsonStr(this));
        }
    }

    public static HttpServletResponse cors() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        Assert.notNull(attributes, "没有MVC上下文");
        HttpServletRequest req = attributes.getRequest();
        HttpServletResponse res = attributes.getResponse();
        Assert.notNull(res, "no response");
        res.setCharacterEncoding("UTF-8");
        res.setContentType("application/json");
        //设置允许传递的参数
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-wx-openid, x-wx-sign, x-wx-mch");
        //设置允许带上cookie
        res.setHeader("Access-Control-Allow-Credentials", "true");
        String origin = req == null ? "*" : Optional.ofNullable(req.getHeader("Origin")).orElse(req.getHeader("Referer"));
        //设置允许的请求来源
        res.setHeader("Access-Control-Allow-Origin", origin);
        //设置允许的请求方法
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
        return res;
    }
}
