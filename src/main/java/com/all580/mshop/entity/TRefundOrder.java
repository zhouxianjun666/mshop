package com.all580.mshop.entity;

import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Data
@Accessors(chain = true)
@Table(name = "t_refund_order")
public class TRefundOrder implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 主订单编号
     */
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 子订单编号
     */
    @Column(name = "item_number")
    private Long itemNumber;

    /**
     * 退订编号
     */
    private Long number;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 退订数量
     */
    private Integer quantity;

    /**
     * 订单支付金额
     */
    @Column(name = "pay_amount")
    private Integer payAmount;

    /**
     * 手续费
     */
    private Integer fee;

    /**
     * 退支付者金额
     */
    private Integer money;

    /**
     * 原手续费
     */
    @Column(name = "original_fee")
    private Integer originalFee;

    /**
     * 原因
     */
    private String cause;

    /**
     * 状态:1-退订中;2-退款中;3-退款成功
     */
    private Integer status;

    /**
     * 微信退款单号
     */
    @Column(name = "refund_id")
    private String refundId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

}
