package com.all580.mshop.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@Table(name = "t_group_product")
public class TGroupProduct extends BasicProduct {
    /**
     * 售价(单独购买)
     */
    @NotNull(message = "售价不能为空", groups = Default.class)
    @Min(value = 1, message = "售价必须大于0", groups = Default.class)
    private Integer price;

    /**
     * 拼团价
     */
    @Column(name = "group_price")
    @NotNull(message = "拼团价不能为空", groups = Default.class)
    @Min(value = 1, message = "拼团价必须大于0", groups = Default.class)
    private Integer groupPrice;

    /**
     * 成团人数
     */
    @NotNull(message = "成团人数不能为空", groups = Default.class)
    @Min(value = 1, message = "成团人数必须大于0", groups = Default.class)
    private Integer num;

    /**
     * 最大张数0不限定
     */
    @Column(name = "max_quantity")
    private Integer maxQuantity;

    /**
     * 须知
     */
    private String notes;

    private static final long serialVersionUID = 1L;

}
