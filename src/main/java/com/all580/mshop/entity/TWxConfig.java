package com.all580.mshop.entity;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_wx_config")
public class TWxConfig implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    @ApiModelProperty(value = "商户ID", readOnly = true)
    private Integer merchantId;

    /**
     * 微信P12证书
     */
    @ApiModelProperty("P12证书BASE64")
    private String certificate;

    /**
     * 微信应用ID
     */
    @Column(name = "app_id")
    @ApiModelProperty("应用ID")
    private String appId;

    /**
     * 微信商户ID
     */
    @Column(name = "mch_id")
    @ApiModelProperty("微信商户ID")
    private String mchId;

    /**
     * 微信商户KEY
     */
    @Column(name = "mch_key")
    @ApiModelProperty("微信商户KEY")
    private String mchKey;

    /**
     * 微信应用秘钥
     */
    @Column(name = "app_secret")
    @ApiModelProperty("微信应用秘钥")
    private String appSecret;

    /**
     * 是否走PHP微信中控
     */
    @ApiModelProperty("是否走PHP微信中控")
    private Boolean center;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    public boolean equal(TWxConfig config) {
        return StrUtil.nullToEmpty(getCertificate()).equals(StrUtil.nullToEmpty(config.getCertificate())) &&
                StrUtil.nullToEmpty(getAppId()).equals(StrUtil.nullToEmpty(config.getAppId())) &&
                StrUtil.nullToEmpty(getMchId()).equals(StrUtil.nullToEmpty(config.getMchId())) &&
                StrUtil.nullToEmpty(getAppSecret()).equals(StrUtil.nullToEmpty(config.getAppSecret()));
    }
    private static final long serialVersionUID = 1L;

}
