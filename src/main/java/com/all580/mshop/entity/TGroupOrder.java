package com.all580.mshop.entity;

import com.all580.mshop.annotation.Append;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_group_order")
public class TGroupOrder implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 拼团号
     */
    private Long number;

    /**
     * 当前已加入人数
     */
    @Append
    private Integer num;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 发起者
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 主产品ID
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 子产品编号
     */
    @Column(name = "sub_code")
    private Long subCode;

    /**
     * 主产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 子产品名称
     */
    @Column(name = "sub_name")
    private String subName;

    /**
     * 是否单人直接购买
     */
    private Boolean single;

    /**
     * 状态:1-创建;2-开启;3-失败;4-完成;
     */
    private Integer status;

    /**
     * 成团时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

}
