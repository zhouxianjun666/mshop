package com.all580.mshop.entity;

import com.all580.mshop.dto.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/11/26 10:46
 */
@Data
@Accessors(chain = true)
public class BasicProduct implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(readOnly = true)
    @NotNull(message = "ID不能为空", groups = UpdateGroup.class)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    @ApiModelProperty(readOnly = true)
    private Integer merchantId;

    /**
     * 主产品ID
     */
    @Column(name = "product_id")
    @NotNull(message = "主产品ID不能为空", groups = Default.class)
    private Integer productId;

    /**
     * 主产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 主产品图片
     */
    @Column(name = "product_imgs")
    private String productImgs;

    /**
     * 子产品编号
     */
    @Column(name = "sub_code")
    @NotNull(message = "子产品编号不能为空", groups = Default.class)
    private Long subCode;

    /**
     * 子产品名称
     */
    @Column(name = "sub_name")
    private String subName;

    /**
     * 子产品图片
     */
    @Column(name = "sub_imgs")
    private String subImgs;

    @ApiModelProperty("供应商")
    private String supplier;

    /**
     * 产品类型
     */
    @NotNull(message = "产品类型不能为空", groups = Default.class)
    private Integer type;

    /**
     * 进货价(分)(设置时当天进货价)
     */
    @Column(name = "purchase_price")
    private Integer purchasePrice;

    /**
     * 价格(分)
     */
    private Integer price;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(readOnly = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    @ApiModelProperty(readOnly = true)
    private Date updateTime;

    /**
     * 是否可售(主产品列表)
     */
    @Transient
    private boolean sale;
}
