package com.all580.mshop.entity;

import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Data
@Accessors(chain = true)
@Table(name = "t_withdraw")
public class TWithdraw implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 提现类型:1-微信;2-支付宝;3-银行卡
     */
    private Integer type;

    /**
     * 提现金额(分)
     */
    private Integer money;

    /**
     * 付款账号(支付宝账号、银行卡号)
     */
    @Column(name = "payment_account")
    private String paymentAccount;

    /**
     * 付款人
     */
    private String payer;

    /**
     * 状态:1-待审核;2-待付款;3-已提现
     */
    private Integer status;

    /**
     * 申请/创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 审核时间
     */
    @Column(name = "audit_time")
    private Date auditTime;

    /**
     * 付款时间
     */
    @Column(name = "payment_time")
    private Date paymentTime;

    /**
     * 流水号
     */
    @Column(name = "bill_no")
    private String billNo;

    private static final long serialVersionUID = 1L;

}
