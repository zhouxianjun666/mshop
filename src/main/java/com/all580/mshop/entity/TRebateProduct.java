package com.all580.mshop.entity;

import cn.hutool.core.util.NumberUtil;
import com.all580.mshop.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@Table(name = "t_rebate_product")
public class TRebateProduct extends BasicProduct {
    /**
     * 返利类型: 1-百分比;2-固定
     */
    @Column(name = "rebate_type")
    private Integer rebateType;
    /**
     * 返利(0~100)%
     */
    private Integer rebate;

    @Transient
    public double getRebatePercent() {
        return NumberUtil.div(Optional.ofNullable(rebate).orElse(0).intValue(), 100, 2);
    }

    @Transient
    public double getRebateMoney(int money) {
        return rebateType != null && rebateType == Constant.RebateType.FIXED ? Optional.ofNullable(rebate).orElse(0) :
                getRebatePercent() * money;
    }

    private static final long serialVersionUID = 1L;

}
