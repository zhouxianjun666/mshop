package com.all580.mshop.entity;

import com.all580.mshop.dto.Order;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_group_order_member")
public class TGroupOrderMember implements Order, Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 主订单编号
     */
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 子订单编号
     */
    @Column(name = "item_number")
    private Long itemNumber;

    /**
     * 拼团ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 拼团流水号
     */
    private Long number;

    /**
     * 游玩日期
     */
    private Date booking;

    /**
     * 天数
     */
    private Integer days;

    /**
     * 数量(一天的张数)
     */
    private Integer quantity;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 身份证
     */
    private String sid;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 用户支付金额
     */
    private Integer money;

    /**
     * 微信支付订单号
     */
    @Column(name = "transaction_id")
    private String transactionId;

    /**
     * 状态:1-待支付;2-支付中;3-已支付;
     */
    private Integer status;

    /**
     * 支付时间
     */
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 取消时间
     */
    @Column(name = "cancel_time")
    private Date cancelTime;

    @Transient
    private TGroupOrder groupOrder;

    private static final long serialVersionUID = 1L;

    @Override
    public String getProductName() {
        return groupOrder == null ? null : getGroupOrder().getProductName();
    }

    @Override
    public String getSubName() {
        return groupOrder == null ? null : getGroupOrder().getSubName();
    }

    @Override
    public Long getSubCode() {
        return groupOrder == null ? null : getGroupOrder().getSubCode();
    }

    @Override
    public String getAttach() {
        return "group";
    }
}
