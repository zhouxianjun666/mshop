package com.all580.mshop.entity;

import cn.hutool.core.util.ReflectUtil;
import com.all580.mshop.dto.AbstractUser;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Accessors(chain = true)
@Table(name = "t_merchant")
public class TMerchant extends AbstractUser implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(readOnly = true)
    private Integer id;

    /**
     * 企业授权ID
     */
    @Column(name = "access_id")
    @ApiModelProperty(readOnly = true)
    private String accessId;

    /**
     * 企业授权KEY
     */
    @Column(name = "access_key")
    @ApiModelProperty(readOnly = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String accessKey;

    /**
     * 微店名称
     */
    @ApiModelProperty(readOnly = true)
    private String name;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 平台
     */
    @ApiModelProperty(readOnly = true)
    private String platform;

    /**
     * 平台地址
     */
    @ApiModelProperty(readOnly = true)
    private String url;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(readOnly = true)
    private Date createTime;

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(readOnly = true)
    public void setForAdd(Object obj) {
        try {
            ReflectUtil.setFieldValue(obj, "createTime", new Date());
            ReflectUtil.setFieldValue(obj, "merchantId", this.getId());
        } catch (Exception ignored) {}
    }

    @Transient
    @Override
    public String getUsername() {
        return getPhone();
    }

    @Override
    public List<String> getRoles() {
        return Stream.of("merchant").collect(Collectors.toList());
    }
}
