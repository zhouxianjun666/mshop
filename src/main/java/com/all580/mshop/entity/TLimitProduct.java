package com.all580.mshop.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Table(name = "t_limit_product")
@Data
@Accessors(chain = true)
public class TLimitProduct extends BasicProduct {
    /**
     * 标题
     */
    private String title;

    /**
     * 会员该产品最大购买数量
     */
    @Column(name = "max_quantity")
    private Integer maxQuantity;

    /**
     * 序号
     */
    private Integer seq;

    /**
     * 抢购须知
     */
    private String notice;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    @ApiModelProperty("开始时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    @ApiModelProperty("结束时间")
    private Date endTime;

    private static final long serialVersionUID = 1L;
}
