package com.all580.mshop.entity;

import com.all580.mshop.dto.Order;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_order")
public class TOrder implements Order, Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商户ID(冗余字段)
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 主产品ID
     */
    @Column(name = "product_id")
    private Integer productId;

    /**
     * 子产品编号
     */
    @Column(name = "sub_code")
    private Long subCode;

    /**
     * 主产品名称
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * 子产品名称
     */
    @Column(name = "sub_name")
    private String subName;

    /**
     * 微商城产品ID
     */
    @Column(name = "shop_product_id")
    private Integer shopProductId;

    /**
     * 类型:1-返利;2-限购;
     */
    private Integer type;

    /**
     * 主订单编号
     */
    @Column(name = "order_number")
    private Long orderNumber;

    /**
     * 子订单编号
     */
    @Column(name = "item_number")
    private Long itemNumber;

    /**
     * 数量(一天的张数)
     */
    private Integer quantity;

    /**
     * 天数
     */
    private Integer days;

    /**
     * 游玩日期
     */
    private Date booking;

    /**
     * 用户需要支付的金额(分)
     */
    private Integer money;

    /**
     * 原价(订单金额(分))
     */
    @Column(name = "original_money")
    private Integer originalMoney;

    /**
     * 返利金额(分)
     */
    private Integer rebate;

    /**
     * 哪个会员分享的
     */
    @Column(name = "from_member")
    private Integer fromMember;

    /**
     * 微信支付订单号
     */
    @Column(name = "transaction_id")
    private String transactionId;

    /**
     * 状态:1-待支付;2-支付中;3-已支付;
     */
    private Integer status;

    /**
     * 支付时间
     */
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 产品类型
     */
    @Column(name = "product_type")
    private Integer productType;

    /**
     * 退订规则
     */
    @Column(name = "refund_rule")
    private String refundRule;

    /**
     * 订单联系人
     */
    private String contact;

    /**
     * 订单联系人手机号
     */
    @Column(name = "contact_phone")
    private String contactPhone;

    /**
     * 表单数据
     */
    @Column(name = "form_data")
    private String formData;

    /**
     * 订单日志
     */
    private String log;

    /**
     * 是否已经返利
     */
    @Column(name = "is_rebate")
    private Boolean isRebate;

  /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    @Override
    public Long getNumber() {
        return getOrderNumber();
    }
}
