package com.all580.mshop.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_withdraw_config")
public class TWithdrawConfig implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(readOnly = true)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    @ApiModelProperty(readOnly = true)
    private Integer merchantId;

    /**
     * 每周几(1~7)
     */
    @Column(name = "week_day")
    @ApiModelProperty("每周几(1~7)")
    private Integer weekDay;

    /**
     * 每月多少号(1~31)
     */
    @Column(name = "month_day")
    @ApiModelProperty("每月多少号(1~31)")
    private Integer monthDay;

    /**
     * 类型: 1 - week; 2 - month
     */
    @ApiModelProperty("类型: 1 - week; 2 - month")
    private Integer type;

    /**
     * 最低限额(分)
     */
    @ApiModelProperty("最低限额(分)")
    private Integer limit;

    /**
     * 开始时间
     */
    @ApiModelProperty("开始时间")
    private Date start;

    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    private Date end;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(readOnly = true)
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @ApiModelProperty(readOnly = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;

}
