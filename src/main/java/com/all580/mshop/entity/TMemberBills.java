package com.all580.mshop.entity;

import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Data
@Accessors(chain = true)
@Table(name = "t_member_bills")
public class TMemberBills implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 关联ID
     */
    @Column(name = "ref_id")
    private Integer refId;

    /**
     * 类型: 1-返利;2-提现
     */
    private Integer type;

    /**
     * 变动之前余额(分)
     */
    private Integer old;

    /**
     * 变动之后余额(分)
     */
    private Integer newer;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

}
