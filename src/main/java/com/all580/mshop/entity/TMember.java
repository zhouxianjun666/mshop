package com.all580.mshop.entity;

import com.all580.mshop.annotation.Append;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import tk.mybatis.mapper.annotation.Version;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@Table(name = "t_member")
@ApiModel("会员")
public class TMember implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(readOnly = true)
    private Integer id;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    @ApiModelProperty(value = "商户ID", readOnly = true)
    private Integer merchantId;

    /**
     * 昵称
     */
    @ApiModelProperty("昵称")
    private String nickname;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String phone;

    /**
     * 余额(分)
     */
    @ApiModelProperty(value = "余额(分)", readOnly = true)
    @Append
    private Integer balance;

    /**
     * 返利总额(分)
     */
    @ApiModelProperty(value = "返利总额(分)", readOnly = true)
    @Append
    private Integer rebate;

    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid", readOnly = true)
    private String openid;

    /**
     * 性别(值为1时是男性，值为2时是女性，值为0时是未知)
     */
    @ApiModelProperty("性别(值为1时是男性，值为2时是女性，值为0时是未知)")
    private Integer sex;

    /**
     * 省份
     */
    @ApiModelProperty("省份")
    private String province;

    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String city;

    /**
     * 国家
     */
    @ApiModelProperty("国家")
    private String country;

    /**
     * 头像地址
     */
    @ApiModelProperty("头像地址")
    private String avatar;

    /**
     * 支付宝账号
     */
    @Column(name = "alipay_account")
    @ApiModelProperty("支付宝账号")
    private String alipayAccount;

    /**
     * 支付宝姓名
     */
    @Column(name = "alipay_name")
    @ApiModelProperty("支付宝姓名")
    private String alipayName;

    /**
     * 银行卡银行名称
     */
    @Column(name = "bank_name")
    @ApiModelProperty("银行卡银行名称")
    private String bankName;

    /**
     * 银行卡银行支行
     */
    @Column(name = "bank_branch")
    @ApiModelProperty("银行卡银行支行")
    private String bankBranch;

    /**
     * 银行卡账号名
     */
    @Column(name = "bank_account_name")
    @ApiModelProperty("银行卡账号名")
    private String bankAccountName;

    /**
     * 银行卡卡号
     */
    @Column(name = "bank_card_no")
    @ApiModelProperty("银行卡卡号")
    private String bankCardNo;

    /**
     * 推广会员ID
     */
    @ApiModelProperty(readOnly = true)
    private Integer pid;

    /**
     * 推广刷新时间
     */
    @Column(name = "refresh_time")
    @ApiModelProperty(readOnly = true)
    private Date refreshTime;

    /**
     * 状态:0-禁用;1-启用;2-限制提现
     */
    @ApiModelProperty("状态:0-禁用;1-启用;2-限制提现")
    private Integer status;

    /**
     * 版本号
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(readOnly = true)
    private Date createTime;

    private static final long serialVersionUID = 1L;

}
