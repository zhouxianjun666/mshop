package com.all580.mshop.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.all580.mshop.dto.UpdateGroup;
import lombok.Data;
import lombok.experimental.Accessors;

@Table(name = "t_banner")
@Data
@Accessors(chain = true)
public class TBanner implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull(message = "ID不能为空", groups = UpdateGroup.class)
    private Integer id;

    /**
     * 排序
     */
    private Integer seq;

    /**
     * 描述
     */
    private String title;

    /**
     * 图片
     */
    @NotBlank(message = "请上传图片", groups = Default.class)
    private String img;

    /**
     * 跳转地址
     */
    private String url;

    /**
     * 商户ID
     */
    @Column(name = "merchant_id")
    private Integer merchantId;

    /**
     * 有效开始时间
     */
    @Column(name = "effective_start")
    @NotNull(message = "请设置有效期", groups = Default.class)
    private Date effectiveStart;

    /**
     * 有效结束时间
     */
    @Column(name = "effective_end")
    private Date effectiveEnd;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
