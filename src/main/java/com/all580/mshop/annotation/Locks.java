package com.all580.mshop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/22 11:38
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Locks {
    String[] value() default {""};

    /**
     * 嵌套函数时有效:是否在当前函数结束时释放当前函数锁,默认false
     * 例如:A函数锁key: LOCK:A,当前函数 LOCK:B,当前函数结束不释放LOCK:B,在A函数结束时释放全部锁
     * @return
     */
    boolean afterRelease() default false;
    long waitTime() default 0;
}
