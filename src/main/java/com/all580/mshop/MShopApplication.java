package com.all580.mshop;

import cn.hutool.core.lang.Snowflake;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.all580.mshop.dto.OssConfig;
import lombok.SneakyThrows;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/5 13:49
 */
@SpringBootApplication
@EnableRetry
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass=true,exposeProxy=true)
@EnableTransactionManagement
@EnableConfigurationProperties
@ServletComponentScan
@MapperScan(basePackages = "com.all580.mshop.mapper")
public class MShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(MShopApplication.class, args);
    }

    @Bean
    public Snowflake snowflake() {
        return new Snowflake(1, 1);
    }

    @Bean
    @SneakyThrows
    public DefaultAcsClient acsClient(OssConfig config) {
        DefaultProfile.addEndpoint("", "", "Sts", config.getStsEndpoint());
        return new DefaultAcsClient(DefaultProfile.getProfile("", config.getAccessKeyId(), config.getAccessKeySecret()));
    }
}
