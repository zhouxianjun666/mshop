package com.all580.mshop.aop;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.util.KeyLock;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.expression.*;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.ConversionService;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.expression.spel.support.StandardTypeConverter;
import org.springframework.expression.spel.support.StandardTypeLocator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/10/22 11:42
 */
@Aspect
@Order(-1)
@Component
@Slf4j
public class LockAop {
    private final ExpressionParser parser = new SpelExpressionParser();
    private final static String LOCK_KEY = "LOCK:";
    private final static KeyLock<String> LOCK = new KeyLock<>();
    /**
     * 存储当前线程开始函数签名
     */
    private final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();
    /**
     * 存储当前线程所有锁KEY
     */
    private final ThreadLocal<Set<String>> THREAD_LOCAL_LOCKS = new ThreadLocal<>();

    @Autowired
    private BeanFactory beanFactory;
    private BeanExpressionContextAccessor beanExpressionContextAccessor;
    private BeanExpressionContext beanExpressionContext;
    private BeanFactoryAccessor beanFactoryAccessor;
    private MapAccessor mapAccessor;
    private EnvironmentAccessor environmentAccessor;
    private BeanFactoryResolver beanFactoryResolver;
    private StandardTypeLocator standardTypeLocator;
    private StandardTypeConverter standardTypeConverter;

    @PostConstruct
    public void init() {
        beanExpressionContext = new BeanExpressionContext((ConfigurableBeanFactory) beanFactory, null);
        beanFactoryResolver = new BeanFactoryResolver(beanFactory);
        beanExpressionContextAccessor = new BeanExpressionContextAccessor();
        beanFactoryAccessor = new BeanFactoryAccessor();
        mapAccessor = new MapAccessor();
        environmentAccessor = new EnvironmentAccessor();
        standardTypeLocator = new StandardTypeLocator(((ConfigurableBeanFactory) beanFactory).getBeanClassLoader());
        ConversionService conversionService = ((ConfigurableBeanFactory) beanFactory).getConversionService();
        if (conversionService != null) {
            standardTypeConverter = new StandardTypeConverter(conversionService);
        }
    }

    @Around(value = "@annotation(locks)")
    @SuppressWarnings("unchecked")
    public Object doAround(ProceedingJoinPoint point, Locks locks) throws Throwable {
        Set<String> keys = parseKeys(point, locks);
        if (CollectionUtils.isEmpty(keys)) {
            return point.proceed();
        }
        String signature = point.getSignature().toLongString();
        String shortSign = point.getSignature().toShortString();
        log.info("try lock {} => {}, times {}, el {}", shortSign, keys, locks.waitTime(), locks.value());
        if (THREAD_LOCAL.get() == null) {
            THREAD_LOCAL.set(signature);
        }
        try {
            if (keys.size() > 1) {
                LOCK.lock(keys.toArray(new String[0]));
            } else {
                LOCK.lock(keys.iterator().next());
            }
            if (!locks.afterRelease()) {
                currentKeys().addAll(keys);
            }
            return point.proceed();
        } finally {
            if (locks.afterRelease()) {
                LOCK.unlock(keys.toArray(new String[0]));
                log.info("unlock {} => {}", shortSign, keys);
            }
            if (THREAD_LOCAL.get().equals(signature)) {
                List<String> list = reverseKeys();
                LOCK.unlock(list.toArray(new String[0]));
                log.info("unlock {} => {}", shortSign, list);
                THREAD_LOCAL.remove();
                THREAD_LOCAL_LOCKS.remove();
            }
        }
    }

    private Set<String> parseKeys(ProceedingJoinPoint point, Locks locks) {
        Set<String> keys = new HashSet<>(1);
        String[] values = locks.value();
        Set<String> set = Arrays.stream(values).filter(StrUtil::isNotBlank).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(set)) {
            keys.add(LOCK_KEY + point.getSignature().toString());
        } else {
            String[] paramNames = ((CodeSignature) point.getSignature()).getParameterNames();
            Object[] params = point.getArgs();
            StandardEvaluationContext context = new StandardEvaluationContext(beanExpressionContext);
            context.addPropertyAccessor(beanExpressionContextAccessor);
            context.addPropertyAccessor(beanFactoryAccessor);
            context.addPropertyAccessor(mapAccessor);
            context.addPropertyAccessor(environmentAccessor);
            context.setBeanResolver(beanFactoryResolver);
            context.setTypeLocator(standardTypeLocator);
            if (standardTypeConverter != null) {
                context.setTypeConverter(standardTypeConverter);
            }
            for (int i = 0; i < paramNames.length; i++) {
                context.setVariable(paramNames[i], params[i]);
            }
            List<Object> list = set.stream()
                    .map(s -> parser.parseExpression(s, ParserContext.TEMPLATE_EXPRESSION).getValue(context))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            boolean checkType = list.stream().allMatch(this::checkType);
            if (!checkType) {
                throw new RuntimeException("lock key type is not basic type");
            }
            list.stream().filter(o -> o instanceof Collection).forEach(o -> ((Collection<?>) o).forEach(v -> keys.add(toString(v))));
            keys.addAll(list.stream().filter(o -> !(o instanceof Collection)).map(this::toString).collect(Collectors.toSet()));
        }
        keys.removeIf(k -> currentKeys().contains(k));
        return keys;
    }

    private boolean checkType(Object o) {
        if (ObjectUtil.isBasicType(o) || o instanceof String) {
            return true;
        }
        if (o instanceof Collection) {
            return ((Collection<?>) o).stream().allMatch(v -> ObjectUtil.isBasicType(v) || v instanceof String);
        }
        return false;
    }

    private String toString(Object o) {
        String str = StrUtil.utf8Str(o);
        str = StrUtil.cleanBlank(str);
        str = clear(str);
        return LOCK_KEY + str;
    }
    private String clear(String s) {
        if (s.startsWith(":")) {
            return clear(s.substring(1));
        }
        if (s.endsWith(":")) {
            return clear(s.substring(0, s.length() - 1));
        }
        return s;
    }

    private Set<String> currentKeys() {
        Set<String> contents = THREAD_LOCAL_LOCKS.get();
        if (contents == null) {
            contents = new LinkedHashSet<>();
            THREAD_LOCAL_LOCKS.set(contents);
        }
        return contents;
    }

    private List<String> reverseKeys() {
        List<String> list = new ArrayList<>(currentKeys());
        Collections.reverse(list);
        return list;
    }
}
