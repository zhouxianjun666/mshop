package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.dto.SearchRefund;
import com.all580.mshop.entity.TRefundOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TRefundOrderMapper extends IMapper<TRefundOrder> {
    /**
     * 查询退订列表
     * @param searchRefund
     * @return
     */
    List<Map<String, Object>> search(@Param("searchRefund") SearchRefund searchRefund);
}
