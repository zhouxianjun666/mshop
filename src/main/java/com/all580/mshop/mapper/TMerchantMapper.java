package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TMerchant;

public interface TMerchantMapper extends IMapper<TMerchant> {
}