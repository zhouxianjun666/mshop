package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TWxConfig;
import org.apache.ibatis.annotations.Param;

public interface TWxConfigMapper extends IMapper<TWxConfig> {
    TWxConfig selectByMerchantAccessId(@Param("accessId") String accessId);
}
