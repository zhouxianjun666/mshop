package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TMember;

public interface TMemberMapper extends IMapper<TMember> {
}