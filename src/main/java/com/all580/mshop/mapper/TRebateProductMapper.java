package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TRebateProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TRebateProductMapper extends IMapper<TRebateProduct> {
    /**
     * 获取返利产品列表按主产品ID分组
     * @param merchantId
     * @param type
     * @return
     */
    List<TRebateProduct> selectGroupProductId(@Param("merchantId") int merchantId, @Param("type") Integer type);
}
