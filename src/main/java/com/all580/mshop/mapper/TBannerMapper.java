package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TBanner;

public interface TBannerMapper extends IMapper<TBanner> {
}