package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TGroupOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface TGroupOrderMapper extends IMapper<TGroupOrder> {
    int updateNum(@Param("id") int id, @Param("num") int num, @Param("max") Integer max);
    List<Map> selectOpenList(@Param("subCode") Long subCode);
    List<TGroupOrder> selectFullMember(RowBounds rowBounds);
    Integer sumPendingMemberByCode(@Param("subCode") Long subCode);
    List<Map<String, Object>> selectByMember(@Param("memberId") int memberId, @Param("groupStatus") Integer groupStatus, @Param("status") Integer status);
}
