package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.dto.SearchOrder;
import com.all580.mshop.entity.TOrder;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TOrderMapper extends IMapper<TOrder> {
    List<TOrder> search(@Param("searchOrder") SearchOrder searchOrder);

    /**
     * 报表
     * @param type 类型
     * @param productName 产品名称
     * @param productType 产品类型
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    List<Map<String, Object>> report(@Param("type") Integer type,
                                     @Param("merchantId") Integer merchantId,
                                     @Param("productName") String productName,
                                     @Param("productType") Integer productType,
                                     @Param("start") Date start,
                                     @Param("end") Date end);
}
