package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TGroupProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TGroupProductMapper extends IMapper<TGroupProduct> {
    /**
     * 获取产品列表按主产品ID分组
     * @param merchantId
     * @param type
     * @return
     */
    List<TGroupProduct> selectGroupProductId(@Param("merchantId") int merchantId, @Param("type") Integer type);
}
