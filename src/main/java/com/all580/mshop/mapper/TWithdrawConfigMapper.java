package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TWithdrawConfig;

public interface TWithdrawConfigMapper extends IMapper<TWithdrawConfig> {
}