package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TWithdraw;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TWithdrawMapper extends IMapper<TWithdraw> {
    List<Map<String, Object>> search(@Param("merchantId") Integer merchantId,
                                     @Param("nickname") String nickname,
                                     @Param("phone") String phone,
                                     @Param("start") Date start,
                                     @Param("end") Date end,
                                     @Param("status") Integer status);
}
