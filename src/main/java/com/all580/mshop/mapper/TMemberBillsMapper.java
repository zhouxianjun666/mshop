package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TMemberBills;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TMemberBillsMapper extends IMapper<TMemberBills> {
    /**
     * 根据类型获取总变动值
     * @param memberId 会员ID
     * @param type 类型
     * @return
     */
    int sumTotal(@Param("memberId") int memberId, @Param("type") Integer type);

    /**
     * 获取返利流水信息
     * @param refId 流水关联ID
     * @param fromId 分享者会员ID
     * @return
     */
    Map<String, Object> getRebateBill(@Param("refId") int refId, @Param("fromId") Integer fromId);

    /**
     * 查询返利流水列表
     * @param merchantId 商户ID
     * @param fromId 分享者会员ID
     * @param fromNickname 分享者会员昵称
     * @param fromPhone 分享者会员手机号
     * @param productName 产品名称
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    List<Map<String, Object>> searchRebateBills(@Param("merchantId") int merchantId,
                                                @Param("fromId") Integer fromId,
                                                @Param("fromNickname") String fromNickname,
                                                @Param("fromPhone") String fromPhone,
                                                @Param("productName") String productName,
                                                @Param("start") Date start,
                                                @Param("end") Date end);

    /**
     * 获取提现流水信息
     * @param refId 流水关联ID
     * @param memberId 会员ID
     * @return
     */
    Map<String, Object> getWithdrawBill(@Param("refId") int refId, @Param("memberId") Integer memberId);
}
