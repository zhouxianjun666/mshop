package com.all580.mshop.mapper;

import com.all580.mshop.IMapper;
import com.all580.mshop.entity.TGroupOrderMember;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TGroupOrderMemberMapper extends IMapper<TGroupOrderMember> {
    List<TGroupOrderMember> selectByFail(RowBounds rowBounds);

    /**
     * 查询拼图明细
     * @param bookingStart 预定日期开始
     * @param bookingEnd 预定日期结束
     * @param start 开团时间开始
     * @param end 开团时间结束
     * @param ownerPhone 开团会员手机号
     * @param ownerNickname 开团会员昵称
     * @param subCode 子产品编号
     * @param productName 主产品名称
     * @param subName 子产品名称
     * @param single 是否为单人购买
     * @param joinPhone 加入会员手机号
     * @param joinNickname 加入会员昵称
     * @return
     */
    List<Map<String, Object>> selectDetail(@Param("bookingStart") Date bookingStart,
                                           @Param("bookingEnd") Date bookingEnd,
                                           @Param("start") Date start,
                                           @Param("end") Date end,
                                           @Param("ownerPhone") String ownerPhone,
                                           @Param("ownerNickname") String ownerNickname,
                                           @Param("subCode") Long subCode,
                                           @Param("productName") String productName,
                                           @Param("subName") String subName,
                                           @Param("single") Boolean single,
                                           @Param("joinPhone") String joinPhone,
                                           @Param("joinNickname") String joinNickname,
                                           @Param("merchantId") int merchantId);
}
