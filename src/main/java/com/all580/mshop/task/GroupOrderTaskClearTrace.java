package com.all580.mshop.task;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.dto.Result;
import com.all580.mshop.dto.SystemConfig;
import com.all580.mshop.entity.*;
import com.all580.mshop.manager.GroupOrderManager;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TGroupOrderMapper;
import com.all580.mshop.mapper.TGroupOrderMemberMapper;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.mapper.TRefundOrderMapper;
import com.github.pagehelper.PageRowBounds;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.all580.mshop.Constant.GroupOrderStatus.*;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 拼团订单任务
 * @date 2018/9/17 17:16
 */
@Component
@Slf4j
public class GroupOrderTaskClearTrace {
    private final static int PAGE_SIZE = 20;
    @Resource
    private TGroupOrderMapper groupOrderMapper;
    @Resource
    private TGroupOrderMemberMapper groupOrderMemberMapper;
    @Resource
    private GroupOrderManager groupOrderManager;
    @Resource
    private TRefundOrderMapper refundOrderMapper;
    @Resource
    private TMerchantMapper merchantMapper;
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private WxManager wxManager;

    @Scheduled(fixedDelay = 60 * 1000 * 5, initialDelay = 1000 * 30)
    public void cancel() {
        this.pullOfCancel(1);
    }

    @Scheduled(fixedDelay = 60 * 1000 * 8, initialDelay = 1000 * 45)
    public void cancelGroup() {
        this.pullOfTimeout(1);
    }

    @Scheduled(fixedDelay = 60 * 1000 * 18, initialDelay = 1000 * 60)
    public void refund() {
        this.pullOfRefund(1);
    }

    @Scheduled(fixedDelay = 60 * 1000 * 12, initialDelay = 1000 * 75)
    public void complete() {
        this.pullOfComplete(1);
    }

    private void pullOfCancel(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TGroupOrderMember> list = groupOrderMemberMapper.selectByExampleAndRowBounds(
                    Example.builder(TGroupOrderMember.class)
                            .where(
                                    WeekendSqls.<TGroupOrderMember>custom()
                                    .andIn(TGroupOrderMember::getStatus, Stream.of(Constant.OrderStatus.PENDING_PAY, Constant.OrderStatus.PAYING).collect(Collectors.toSet()))
                                    .andLessThan(TGroupOrderMember::getCreateTime, DateUtil.offsetMinute(new Date(), -3))
                            )
                            .build(),
                    rowBounds
            );
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.stream().filter(m -> !Constant.STRING_CACHE.containsKey("PAID:PLATFORM:" + m.getGroupId())).forEach(this::doCancelNoException);
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfCancel(++page);
            }
        }
    }

    private void pullOfTimeout(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TGroupOrder> list = groupOrderMapper.selectByExampleAndRowBounds(
                    Example.builder(TGroupOrder.class)
                            .where(
                                    WeekendSqls.<TGroupOrder>custom()
                                            .andIn(TGroupOrder::getStatus, Stream.of(CREATE, OPEN).collect(Collectors.toSet()))
                                            .andLessThan(TGroupOrder::getCreateTime, DateUtil.offsetHour(new Date(), -systemConfig.getGroupExpiredHours()))
                            )
                            .build(),
                    rowBounds
            );
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.forEach(this::doFailNoException);
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfTimeout(++page);
            }
        }
    }

    private void pullOfRefund(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TGroupOrderMember> list = groupOrderMemberMapper.selectByFail(rowBounds);
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.forEach(this::doRefundNoException);
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfRefund(++page);
            }
        }
    }

    private void pullOfComplete(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TGroupOrder> list = groupOrderMapper.selectFullMember(rowBounds);
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.stream().filter(m -> !Constant.STRING_CACHE.containsKey("PAID:PLATFORM:" + m.getId())).forEach(this::doCompleteNoException);
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfComplete(++page);
            }
        }
    }

    private void doCancelNoException(TGroupOrderMember order) {
        try {
            ((GroupOrderTaskClearTrace) AopContext.currentProxy()).doCancel(order);
        } catch (Exception e) {
            log.warn("支付超时取消异常", e);
        }
    }
    private void doFailNoException(TGroupOrder order) {
        try {
            ((GroupOrderTaskClearTrace) AopContext.currentProxy()).doFail(order);
        } catch (Exception e) {
            log.warn("拼团超时取消异常", e);
        }
    }

    @Locks("#{#order.number}")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doCancel(TGroupOrderMember order) {
        log.debug("检查拼团成员订单: {} 支付状态", order.getNumber());
        order = groupOrderMemberMapper.selectByPrimaryKey(order.getId());
        if (order.getStatus() == Constant.OrderStatus.PAYING) {
            TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
            TWxConfig config = wxManager.getConfig(merchant.getAccessId());
            boolean checkPay = groupOrderManager.processOrderStatus(order, config);
            if (checkPay) {
                return;
            }
        }
        if (order.getStatus() == Constant.OrderStatus.PENDING_PAY || order.getStatus() == Constant.OrderStatus.PAYING) {
            log.debug("拼团成员订单: {} 未支付自动取消", order.getNumber());
            groupOrderManager.cancelOrderMember(order);
        }
    }

    @Locks("#{#order.number}")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doFail(TGroupOrder order) {
        order = groupOrderMapper.selectByPrimaryKey(order.getId());
        if (order.getStatus() == CREATE || order.getStatus() == OPEN) {
            log.debug("拼团订单: {} 失败", order.getNumber());
            groupOrderMapper.updateByPrimaryKeySelective(order.setStatus(FAIL));
        }
    }

    @Locks("#{#order.number}")
    public void doRefund(TGroupOrderMember order) {
        order = groupOrderMemberMapper.selectByPrimaryKey(order.getId());
        boolean refund = false;
        TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
        if (order.getStatus() == Constant.OrderStatus.PAYING) {
            TWxConfig config = wxManager.getConfig(merchant.getAccessId());
            Result<?> queryResult = groupOrderManager.queryOrderStatus(order, config);
            int status = queryResult.get("status");
            if (status == 0) {
                throw new RuntimeException("查询订单支付状态异常");
            }
            if (status == Constant.OrderStatus.PAYING) {
                throw new RuntimeException("该订单正在支付中");
            }
            if (status == Constant.OrderStatus.PAID) {
                refund = true;
            }
        }
        if (refund || order.getStatus() == Constant.OrderStatus.PAID) {
            ((GroupOrderTaskClearTrace) AopContext.currentProxy()).doRefund(order, merchant);
        }
    }

    private void doRefundNoException(TGroupOrderMember order) {
        try {
            ((GroupOrderTaskClearTrace) AopContext.currentProxy()).doRefund(order);
        } catch (Exception e) {
            log.warn("拼团已支付退款异常", e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doRefund(TGroupOrderMember order, TMerchant merchant) {
        log.debug("拼团成员订单: {} 退款", order.getNumber());
        TRefundOrder refundOrder = refundOrderMapper.selectOne(new TRefundOrder().setOrderNumber(order.getNumber()));
        if (refundOrder == null) {
            refundOrder = new TRefundOrder()
                    .setCreateTime(new Date())
                    .setFee(0)
                    .setOriginalFee(0)
                    .setItemNumber(0L)
                    .setOrderNumber(order.getNumber())
                    .setMemberId(order.getMemberId())
                    .setMerchantId(merchant.getId())
                    .setPayAmount(order.getMoney())
                    .setMoney(order.getMoney())
                    .setNumber(snowflake.nextId())
                    .setQuantity(order.getQuantity())
                    .setCause("拼团失败")
                    .setStatus(Constant.RefundOrderStatus.PENDING);
            refundOrderMapper.insertSelective(refundOrder);
        }
        groupOrderMemberMapper.updateByPrimaryKeySelective(new TGroupOrderMember().setId(order.getId()).setStatus(Constant.OrderStatus.CANCEL).setCancelTime(new Date()));
        groupOrderManager.refundConfirm(merchant, refundOrder.getNumber());
    }

    private void doCompleteNoException(TGroupOrder order) {
        try {
            groupOrderManager.paidToPlatform(null, order.getNumber());
        } catch (Exception e) {
            log.warn("拼团完成检查异常", e);
        }
    }
}
