package com.all580.mshop.task;

import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.manager.OrderManager;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.mapper.TOrderMapper;
import com.all580.mshop.mapper.TWxConfigMapper;
import com.github.pagehelper.PageRowBounds;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单返利任务
 * @date 2018/9/17 17:16
 */
@Component
@Slf4j
public class OrderRebateClearTrace {
    private final static int PAGE_SIZE = 20;
    @Resource
    private TOrderMapper orderMapper;
    @Resource
    private OrderManager orderManager;

    @Scheduled(fixedDelay = 60 * 1000 * 3, initialDelay = 1000 * 30)
    public void run() {
        this.pull(1);
    }

    private void pull(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TOrder> list = orderMapper.selectByExampleAndRowBounds(
                    Example.builder(TOrder.class)
                            .where(WeekendSqls.<TOrder>custom()
                                    .andEqualTo(TOrder::getStatus, Constant.OrderStatus.PAID)
                                    .andEqualTo(TOrder::getIsRebate, false)
                                    .andEqualTo(TOrder::getType, Constant.ProductType.REBATE)
                                    .andIsNotNull(TOrder::getFromMember)
                            )
                            .build(),
                    rowBounds
            );
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.forEach(m -> ((OrderRebateClearTrace) AopContext.currentProxy()).doRebate(m));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pull(++page);
            }
        }
    }

    @Locks("#{#order.number}")
    public void doRebate(TOrder order) {
        try {
            orderManager.rebate(orderMapper.selectByPrimaryKey(order.getId()));
        } catch (Exception e) {
            log.warn("订单 {} 返利异常", order.getNumber(), e);
        }
    }
}
