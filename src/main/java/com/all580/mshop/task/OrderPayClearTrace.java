package com.all580.mshop.task;

import cn.hutool.core.date.DateUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.annotation.Locks;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.entity.TWxConfig;
import com.all580.mshop.manager.OrderManager;
import com.all580.mshop.manager.WxManager;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.mapper.TOrderMapper;
import com.github.pagehelper.PageRowBounds;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 订单支付任务
 * @date 2018/9/17 17:16
 */
@Component
@Slf4j
public class OrderPayClearTrace {
    private final static int PAGE_SIZE = 20;
    @Resource
    private TOrderMapper orderMapper;
    @Resource
    private OrderManager orderManager;
    @Resource
    private TMerchantMapper merchantMapper;
    @Autowired
    private WxManager wxManager;

    @Scheduled(fixedDelay = 60 * 1000 * 3, initialDelay = 1000 * 30)
    public void cancel() {
        this.pullOfCancel(1);
    }

    @Scheduled(fixedDelay = 60 * 1000 * 5, initialDelay = 1000 * 30)
    public void paying() {
        this.pullOfPaying(1);
    }

    private void pullOfCancel(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TOrder> list = orderMapper.selectByExampleAndRowBounds(
                    Example.builder(TOrder.class)
                            .where(WeekendSqls.<TOrder>custom()
                                    .andEqualTo(TOrder::getStatus, Constant.OrderStatus.PENDING_PAY)
                                    .andLessThan(TOrder::getCreateTime, DateUtil.offsetMinute(new Date(), -20))
                            )
                            .build(),
                    rowBounds
            );
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.forEach(m -> ((OrderPayClearTrace) AopContext.currentProxy()).doCancelNoException(m));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfCancel(++page);
            }
        }
    }

    private void pullOfPaying(int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TOrder> list = orderMapper.selectByExampleAndRowBounds(
                    Example.builder(TOrder.class)
                            .where(WeekendSqls.<TOrder>custom()
                                    .andEqualTo(TOrder::getStatus, Constant.OrderStatus.PAYING)
                                    .andLessThan(TOrder::getCreateTime, DateUtil.offsetMinute(new Date(), -3))
                            )
                            .build(),
                    rowBounds
            );
            if (CollectionUtils.isEmpty(list)) {
                return;
            }
            list.forEach(m -> ((OrderPayClearTrace) AopContext.currentProxy()).doPayingNoException(m));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pullOfPaying(++page);
            }
        }
    }

    private void doPayingNoException(TOrder order) {
        try {
            ((OrderPayClearTrace) AopContext.currentProxy()).doPaying(order);
        } catch (Exception e) {
            log.warn("检查订单 {} 支付中状态异常", order.getNumber(), e);
        }
    }

    private void doCancelNoException(TOrder order) {
        try {
            ((OrderPayClearTrace) AopContext.currentProxy()).doCancel(order);
        } catch (Exception e) {
            log.warn("取消订单 {} 异常", order.getNumber(), e);
        }
    }

    @Locks("#{#order.number}")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doPaying(TOrder order) {
        TMerchant merchant = merchantMapper.selectByPrimaryKey(order.getMerchantId());
        TWxConfig config = wxManager.getConfig(merchant.getAccessId());
        boolean checkPay = orderManager.processOrderStatus(order, config);
        if (checkPay) {
            return;
        }
        orderMapper.updateByPrimaryKeySelective(new TOrder()
                .setId(order.getId())
                .setStatus(Constant.OrderStatus.PENDING_PAY)
        );
    }

    @Locks("#{#order.number}")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class, RuntimeException.class})
    public void doCancel(TOrder order) {
        order = orderMapper.selectByPrimaryKey(order.getId());
        if (order.getStatus() == Constant.OrderStatus.PENDING_PAY) {
            orderMapper.updateByPrimaryKeySelective(new TOrder()
                    .setId(order.getId())
                    .setStatus(Constant.OrderStatus.CANCEL)
                    .setLog("支付超时")
            );
        }
    }
}
