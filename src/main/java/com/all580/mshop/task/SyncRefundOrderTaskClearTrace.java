package com.all580.mshop.task;

import com.all580.mshop.Constant;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TRefundOrder;
import com.all580.mshop.manager.OrderManager;
import com.all580.mshop.manager.PlatformManager;
import com.all580.mshop.mapper.TMerchantMapper;
import com.all580.mshop.mapper.TRefundOrderMapper;
import com.github.pagehelper.PageRowBounds;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 同步退订订单任务
 * @date 2018/9/17 17:16
 */
@Component
@Slf4j
public class SyncRefundOrderTaskClearTrace {
    private AtomicBoolean run = new AtomicBoolean(false);
    private final static int PAGE_SIZE = 20;
    @Resource
    private TRefundOrderMapper refundOrderMapper;
    @Resource
    private TMerchantMapper merchantMapper;
    @Autowired
    private PlatformManager platformManager;
    @Autowired
    private OrderManager orderManager;

    @Scheduled(fixedDelay = 60 * 1000 * 5)
    public void run() {
        if (run.compareAndSet(false, true)) {
            try {
                List<TMerchant> merchants = merchantMapper.selectAll();
                for (TMerchant merchant : merchants) {
                    this.pull(merchant, 1);
                }
            } finally {
                run.set(false);
            }
        }
    }

    private void pull(TMerchant merchant, int page) {
        PageRowBounds rowBounds = new PageRowBounds((page - 1) * PAGE_SIZE, PAGE_SIZE);
        try {
            List<TRefundOrder> list = refundOrderMapper.selectByRowBounds(new TRefundOrder().setMerchantId(merchant.getId()).setStatus(Constant.RefundOrderStatus.PENDING), rowBounds);
            Map<Long, Boolean> refundMap = platformManager.orderApi(merchant).isRefund(merchant, list);
            if (CollectionUtils.isEmpty(refundMap)) {
                return;
            }
            refundMap.entrySet().stream().filter(Map.Entry::getValue).forEach(entry -> orderManager.refundConfirm(merchant, entry.getKey()));
        } finally {
            if (rowBounds.getTotal() > page * PAGE_SIZE) {
                this.pull(merchant, ++page);
            }
        }
    }
}
