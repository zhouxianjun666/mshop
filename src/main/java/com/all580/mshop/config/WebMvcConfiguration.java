package com.all580.mshop.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        Optional<HttpMessageConverter<?>> optional = converters.stream().filter(c -> c instanceof MappingJackson2HttpMessageConverter).findFirst();
        if (optional.isPresent()) {
            MappingJackson2HttpMessageConverter converter = (MappingJackson2HttpMessageConverter) optional.get();
            ObjectMapper objectMapper = converter.getObjectMapper();
            // 序列换成json时,将所有的long变成string, 因为js中得数字类型不能包含所有的java long值
            SimpleModule simpleModule = new SimpleModule();
            simpleModule.addSerializer(BigInteger.class, BigNumberSerializer.instance);
            simpleModule.addSerializer(Long.class, BigNumberSerializer.instance);
            simpleModule.addSerializer(Long.TYPE, BigNumberSerializer.instance);
            objectMapper.registerModule(simpleModule);
        }
    }
}
