package com.all580.mshop.config;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.IoUtil;
import com.all580.mshop.entity.TWxConfig;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/5/28 12:50
 */
@Component
public class OkHttpConfig {
    @Bean
    public OkHttpClient okHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);
        return builder.build();
    }

    public static OkHttpClient wxPayClient(TWxConfig config) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS)
                .sslSocketFactory(socketFactory(config))
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);
        return builder.build();
    }

    private static SSLSocketFactory socketFactory(TWxConfig config) {
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(IoUtil.toStream(Base64.decode(config.getCertificate())), config.getMchId().toCharArray());
            SSLContext sslContext = SSLContext.getInstance("TLS");
            KeyManagerFactory keyManagers = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagers.init(keyStore, config.getMchId().toCharArray());
            sslContext.init(keyManagers.getKeyManagers(), null, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException("微信证书加载失败", e);
        }
    }

    public static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");
    public static final okhttp3.MediaType XML = okhttp3.MediaType.parse("application/xml; charset=utf-8");
}
