package com.all580.mshop.config;

import com.all580.mshop.dto.UserRealm;
import com.all580.mshop.filter.LoginAuthorizationFilter;
import com.all580.mshop.filter.OptionsFilter;
import com.all580.mshop.filter.WxFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.annotation.PostConstruct;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/8/30 10:39
 */
@Configuration
@Slf4j
public class ShiroConfiguration {
    @Autowired
    private ShiroFilterFactoryBean shiroFilterFactoryBean;
    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public Realm userRealm() {
        return new UserRealm();
    }

    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        advisorAutoProxyCreator.setUsePrefix(true);
        return advisorAutoProxyCreator;
    }

    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();

        chainDefinition.addPathDefinition("/login", "anon");
        chainDefinition.addPathDefinition("/logout", "anon");
        chainDefinition.addPathDefinition("/swagger-ui.html", "anon");
        chainDefinition.addPathDefinition("/swagger-resources", "anon");
        chainDefinition.addPathDefinition("/v2/api-docs", "anon");
        chainDefinition.addPathDefinition("/webjars/springfox-swagger-ui/**", "anon");
        chainDefinition.addPathDefinition("/api/**", "options,wx,login,authc");
        chainDefinition.addPathDefinition("/notify/**", "anon");
        chainDefinition.addPathDefinition("/**", "options,wx,anon");
        return chainDefinition;
    }

    @Bean
    protected CacheManager cacheManager() {
        return new MemoryConstrainedCacheManager();
    }

    @PostConstruct
    public void init() {
        OptionsFilter optionsFilter = new OptionsFilter();
        WxFilter wxFilter = new WxFilter();
        applicationContext.getAutowireCapableBeanFactory().autowireBean(optionsFilter);
        applicationContext.getAutowireCapableBeanFactory().autowireBean(wxFilter);
        shiroFilterFactoryBean.getFilters().put("options", optionsFilter);
        shiroFilterFactoryBean.getFilters().put("wx", wxFilter);
        shiroFilterFactoryBean.getFilters().put("login", new LoginAuthorizationFilter());
    }
}
