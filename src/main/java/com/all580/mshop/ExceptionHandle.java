package com.all580.mshop;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.StrUtil;
import com.aliyuncs.exceptions.ClientException;
import com.all580.mshop.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description: 统一异常处理
 * @date 2018/5/24 16:54
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandle {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Result<?> handle(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        FieldError fieldError = bindingResult.getFieldError();
        return Result.fail(fieldError != null ? fieldError.getDefaultMessage() : "参数验证失败").value(bindingResult.getFieldErrors());
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public Result<?> unauthorized(UnauthorizedException e) {
        log.error("权限不足异常", e);
        return Result.fail(Result.UNAUTHORIZED, "没有权限");
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    @ResponseBody
    public Result<?> handle(IncorrectCredentialsException e) {
        log.error("密码不匹配", e);
        return Result.fail("密码不匹配");
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public Result<?> handle(AuthenticationException e) {
        log.error("认证失败", e);
        return Result.fail("认证失败");
    }

    @ExceptionHandler(DuplicateKeyException.class)
    @ResponseBody
    public Result<?> handle(DuplicateKeyException e) {
        log.error("数据库唯一约束异常", e);
        String msg = StrUtil.subBetween(e.getMessage(), "Duplicate entry '", "' for key");
        return Result.fail(Result.UNIQUE_KEY_ERROR, "重复数据:" + msg);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public Result<?> handle(DataIntegrityViolationException e) {
        log.error("数据库字段长度异常", e);
        String msg = StrUtil.subBetween(e.getMessage(), "Data too long for column '", "' at row");
        return Result.fail(Result.PARAM_FAIL, "超长数据:" + msg);
    }

    @ExceptionHandler(ClientException.class)
    @ResponseBody
    public Result<?> handle(ClientException e) {
        log.error("OSS 异常", e);
        return Result.fail(e.getErrMsg());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public Result<?> handle(RuntimeException e, HttpServletRequest request){
        log.error("未捕获的异常", e);
        return Result.builder()
                .code(Result.FAIL)
                .msg(e.getMessage())
                .build()
                .put("timestamp", System.currentTimeMillis())
                .put("message", ExceptionUtil.stacktraceToString(e))
                .put("path", request.getRequestURI());
    }
}
