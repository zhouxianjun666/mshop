package com.all580.mshop.platform;

import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.config.OkHttpConfig;
import com.all580.mshop.dto.Result;
import com.all580.mshop.entity.TMerchant;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Map;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 13:21
 */
@Slf4j
public abstract class AbstractPlatformManager {
    @Autowired
    @Getter
    private OkHttpClient okHttpClient;

    protected Result<?> get(TMerchant merchant, String path, Map<String, Object> params) throws IOException {
        params.put("access_id", merchant.getAccessId());
        String query = HttpUtil.toParams(params);
        String sign = sign(query, merchant.getAccessKey());
        return request(new Request.Builder().url(merchant.getUrl() + path + "?" + query).addHeader("sign", sign).build());
    }

    protected Result<?> post(TMerchant merchant, String path, Map<String, Object> params) throws IOException {
        params.put("access_id", merchant.getAccessId());
        String query = JSONUtil.toJsonStr(params);
        String sign = sign(query, merchant.getAccessKey());
        return request(new Request.Builder()
                .url(merchant.getUrl() + path)
                .post(RequestBody.create(OkHttpConfig.JSON, query))
                .addHeader("sign", sign)
                .build()
        );
    }

    protected Result<?> request(Request request) throws IOException {
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return parseBody(request, response, response.body().string());
            }
            log.warn("请求商户平台异常: {}", response);
            return Result.fail(response.toString());
        }
    }

    protected Result<?> parseBody(Request request, Response response, String string) {
        JSONObject object = JSONUtil.parseObj(string);
        int code = object.getInt("code", 0);
        Result<Object> result = Result.builder()
                .code(code)
                .msg(StrUtil.blankToDefault(object.getStr("error"), object.getStr("message")))
                .value(object.get("data"))
                .build()
                .put("original", object);
        if (!result.isSuccess()) {
            log.warn("请求商户平台失败:{} => {}", request, result);
        }
        return result;
    }

    @SneakyThrows
    protected String sign(String text, String key) {
        MessageDigest md = MessageDigest.getInstance("MD5");
        //使用指定的字节数组更新摘要。
        md.update(text.getBytes(StandardCharsets.UTF_8));
        //通过执行诸如填充之类的最终操作完成哈希计算。
        byte[] b = md.digest(key.getBytes());
        return HexUtil.encodeHexStr(b);
    }
}
