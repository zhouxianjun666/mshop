package com.all580.mshop.platform;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 15:18
 */
public interface PlatformApi {
    String type();

    /**
     * 转换集合实例
     * @param list 集合
     * @param transform 实例
     * @param mapping 字段mapping
     * @return
     */
    default <T> List<T> transformList(List<Map> list, Function<Map, T> transform, Map<String, String> mapping, String ...ignore) {
        if (list == null) {
            return null;
        }
        return list.stream().map(
                map -> transform(map, transform, mapping, ignore))
                .collect(Collectors.toList());
    }

    /**
     * 转换集合实例
     * @param list 集合
     * @param transform 实例
     * @return
     */
    default <T> List<T> transformList(List<Map> list, Function<Map, T> transform) {
        return transformList(list, transform, null, new String[0]);
    }

    default <T> List<T> transformList(List<Map> list, Function<Map, T> transform, String ...ignore) {
        return transformList(list, transform, null, ignore);
    }

    /**
     * 转换实例
     * @param map 集合
     * @param transform 实例
     * @param mapping 转换mapping
     * @param <T>
     * @return
     */
    default <T> T transform(Map map, Function<Map, T> transform, Map<String, String> mapping, String ...ignore) {
        return BeanUtil.fillBeanWithMap(
                map,
                transform.apply(map),
                true,
                CopyOptions.create()
                        .setIgnoreError(true)
                        .setIgnoreCase(true)
                        .setIgnoreNullValue(true)
                        .setFieldMapping(mapping)
                        .setIgnoreProperties(ignore)
        );
    }

    default <T> T transform(Map map, Function<Map, T> transform) {
        return transform(map, transform, null, new String[0]);
    }
    default <T> T transform(Map map, Function<Map, T> transform, String ...ignore) {
        return transform(map, transform, null, ignore);
    }
}
