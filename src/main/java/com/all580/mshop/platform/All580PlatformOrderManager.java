package com.all580.mshop.platform;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.Constant;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.BasicProduct;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.entity.TRefundOrder;
import com.all580.mshop.mapper.TOrderMapper;
import lombok.SneakyThrows;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 13:32
 */
@Component
public class All580PlatformOrderManager extends AbstractPlatformManager implements PlatformOrderApi {
    @Resource
    private TOrderMapper orderMapper;

    @Override
    @SneakyThrows
    public Result<?> create(TMerchant merchant, Map<String, Object> params, BasicProduct product) {
        return create(merchant, params, product.getType());
    }

    @Override
    @SneakyThrows
    public Result<?> create(TMerchant merchant, Map<String, Object> params, int type) {
        switch (type) {
            case Constant.PlatformProductType.TICKET:
                return post(merchant, "/api/order/create", params);
            case Constant.PlatformProductType.HOTEL:
                return post(merchant, "/api/order/hotel/create", params);
            case Constant.PlatformProductType.LINE:
                return post(merchant, "/api/order/line/create", params);
            default:
                return Result.fail("不支持的类型");
        }
    }

    @Override
    @SneakyThrows
    public Result<?> payment(TMerchant merchant, TOrder order) {
        Map<String, Object> params = new HashMap<>(3);
        params.put("order_sn", order.getOrderNumber());
        params.put("pay_type", 7111);
        return post(merchant, "/api/order/payment", params);
    }

    @Override
    @SneakyThrows
    public Result<?> batchPayment(TMerchant merchant, Set<String> orders) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("list", orders);
        return post(merchant, "/api/order/payment/batch", params);
    }

    @Override
    @SneakyThrows
    public List<PlatformOrderInfo> batchInfo(TMerchant merchant, List<TOrder> orders) {
        Set<Long> numbers = orders.stream().map(TOrder::getOrderNumber).collect(Collectors.toSet());
        return batchInfo(merchant, numbers);
    }

    @Override
    @SneakyThrows
    public List<PlatformOrderInfo> batchInfo(TMerchant merchant, Set<Long> numbers) {
        if (numbers.isEmpty()) {
            return Collections.emptyList();
        }
        Result<?> result = get(merchant, "/api/order/query/batch/order/info", MapUtil.builder(new HashMap<String, Object>(2)).put("numbers", numbers).build());
        if (!result.isSuccess()) {
            return Collections.emptyList();
        }
        JSONArray json = (JSONArray) result.getValue();
        return transformList(JSONUtil.toList(json, Map.class), map -> new PlatformOrderInfo(), MapUtil.builder("start", "booking").build());
    }

    @Override
    @SneakyThrows
    public PlatformItemDetail detail(TMerchant merchant, TOrder order) {
        Map<String, Object> params = new HashMap<>(3);
        params.put("number", order.getItemNumber());
        params.put("line", order.getProductType() == Constant.PlatformProductType.LINE);
        Result<?> result = get(merchant, "/api/order/query/item/detail", params);
        if (!result.isSuccess()) {
            return null;
        }
        Map map = JSONUtil.toBean((JSONObject) result.getValue(), Map.class);
        return transform(map, m -> new PlatformItemDetail().put("refundRuleDesc", parseRefundRule(order.getRefundRule())), MapUtil.builder("custRefundRule", "refundRule").build());
    }

    @Override
    @SneakyThrows
    public PlatformRefundPreview refundPreview(TMerchant merchant, TOrder order) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("number", order.getItemNumber());
        Result<?> result = get(merchant, "/api/order/query/refund/preview", params);
        if (!result.isSuccess()) {
            return null;
        }
        Map map = JSONUtil.toBean((JSONObject) result.getValue(), Map.class);
        return transform(map, m -> new PlatformRefundPreview().put("refundRuleDesc", parseRefundRule(order.getRefundRule())));
    }

    @Override
    public int calcRefundFee(TMerchant merchant, TOrder order, Date refundDate, int quantity) {
        if (!JSONUtil.isJsonObj(order.getRefundRule())) {
            return 0;
        }
        JSONObject jsonObject = JSONUtil.parseObj(order.getRefundRule());
        boolean all = jsonObject.getBool("all", true);
        // 无损退
        if (all) {
            return 0;
        }
        Object tmp = jsonObject.get("rule");
        if (!(tmp instanceof JSONArray)) {
            return 0;
        }
        JSONArray rules = (JSONArray) tmp;
        for (Object o : rules) {
            JSONObject item = (JSONObject) o;
            boolean isBefore = true;
            if (item.containsKey("before") && !"".equals(item.get("before"))) {
                JSONObject before = item.getJSONObject("before");
                if (before.containsKey("day")) {
                    Date date = parseRefundDay(before, order);
                    if (refundDate.compareTo(date) <= 0) {
                        return returnFee(item, order, quantity);
                    }
                    isBefore = false;
                }
            }
            if (item.containsKey("after") && !"".equals(item.get("after"))) {
                JSONObject after = item.getJSONObject("after");
                if (after.containsKey("day")) {
                    Date date = parseRefundDay(after, order);
                    if (refundDate.after(date) && isBefore) {
                        return returnFee(item, order, quantity);
                    }
                }
            }
        }
        return 0;
    }

    @Override
    @SneakyThrows
    public Result<PlatformRefundResult> refund(TMerchant merchant, TOrder order, Map<String, Object> params) {
        Result<?> result;
        switch (order.getProductType()) {
            case Constant.PlatformProductType.TICKET:
            case Constant.PlatformProductType.LINE:
                result = post(merchant, "/api/order/refund/ota/apply", params);
                break;
            case Constant.PlatformProductType.HOTEL:
                result = post(merchant, "/api/order/refund/hotel/apply", params);
                break;
            default:
                return Result.fail("不支持的类型");
        }
        if (!result.isSuccess()) {
            return Result.fail(result.getMsg());
        }
        Map map = JSONUtil.toBean((JSONObject) result.getValue(), Map.class);
        return Result.ok((PlatformRefundResult) transform(map, m -> new PlatformRefundResult()));
    }

    @Override
    @SneakyThrows
    public Map<Long, Boolean> isRefund(TMerchant merchant, List<TRefundOrder> refundOrders) {
        Set<Long> numbers = refundOrders.stream().map(TRefundOrder::getNumber).collect(Collectors.toSet());
        return isRefund(merchant, numbers);
    }

    private Map<Long, Boolean> isRefund(TMerchant merchant, Set<Long> numbers) throws IOException {
        if (numbers.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, Object> params = new HashMap<>(2);
        params.put("numbers", numbers);
        Result<?> result = get(merchant, "/api/order/query/batch/refund/info", params);
        if (!result.isSuccess()) {
            return Collections.emptyMap();
        }
        JSONArray json = (JSONArray) result.getValue();
        return json.stream().collect(Collectors.toMap(o -> ((JSONObject) o).getLong("number"), v -> ((JSONObject) v).getInt("status") == 345));
    }

    @Override
    @SneakyThrows
    public boolean isRefund(TMerchant merchant, Long number) {
        Map<Long, Boolean> map = isRefund(merchant, Collections.singleton(number));
        return map.getOrDefault(number, false);
    }

    @Override
    @SneakyThrows
    public OrderTmp transformOrder(TMerchant merchant, Map<String, Object> params, Result<?> result, boolean refundRule) {
        JSONObject json = (JSONObject) result.getValue();
        Object number = json.getByPath("$.order.number");
        Object amount = json.getByPath("$.order.pay_amount");
        Object itemNumber = json.getByPath("$.items[0].number");
        Object days = json.getByPath("$.items[0].days");
        Object quantity = json.getByPath("$.items[0].quantity");
        Object name = json.getByPath("$.items[0].pro_name");
        Object subName = json.getByPath("$.items[0].pro_sub_name");
        Object booking = json.getByPath("$.items[0].start");
        Map shippingMap = (Map) params.get("shipping");
        Map<String, Object> param = new HashMap<>(2);
        param.put("number", itemNumber);
        String refundRuleStr = null;
        if (refundRule) {
            Result<?> firstResult = get(merchant, "/api/order/query/item/detail/first", param);
            if (!firstResult.isSuccess()) {
                return null;
            }
            JSONObject firstJson = (JSONObject) firstResult.getValue();
            refundRuleStr = firstJson.getStr("cust_refund_rule");
        }
        return new OrderTmp()
                .setNumber(Long.valueOf(number.toString()))
                .setItemNumber(Long.valueOf(itemNumber.toString()))
                .setMoney(Integer.valueOf(amount.toString()))
                .setQuantity(Integer.valueOf(quantity.toString()))
                .setDays(Integer.valueOf(days.toString()))
                .setName(name.toString())
                .setSubName(subName.toString())
                .setBooking(DateUtil.parse(booking.toString()))
                .setRefundRule(refundRuleStr)
                .setContact(MapUtils.getString(shippingMap, "name"))
                .setContactPhone(MapUtils.getString(shippingMap, "phone"))
                ;
    }

    @Override
    public OrderTmp transformOrder(TMerchant merchant, Map<String, Object> params, Result<?> result) {
        return transformOrder(merchant, params, result, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Result<?> retryPayment(TMerchant merchant, TOrder order) {
        Map map = JSONUtil.toBean(order.getFormData(), Map.class);
        Result<?> result = create(merchant, map, order.getProductType());
        if (!result.isSuccess()) {
            return result;
        }
        OrderTmp orderTmp = transformOrder(merchant, map, result);
        if (orderTmp.getMoney() != order.getOriginalMoney()) {
            return Result.fail("重新支付失败:P10005");
        }
        order.setOrderNumber(orderTmp.getNumber()).setItemNumber(orderTmp.getItemNumber());
        orderMapper.updateByPrimaryKeySelective(new TOrder()
                .setId(order.getId())
                .setOrderNumber(orderTmp.getNumber())
                .setItemNumber(orderTmp.getItemNumber())
        );
        return payment(merchant, order);
    }

    @Override
    public Result<Boolean> canRetryPayment(TMerchant merchant, TOrder order, Result<?> result) {
        return Result.ok(!result.isSuccess() && result.getCode() == -10001);
    }

    @Override
    public Result<Boolean> canPayment(TMerchant merchant, TOrder order) {
        List<PlatformOrderInfo> infos = batchInfo(merchant, Collections.singletonList(order));
        if (CollectionUtils.isEmpty(infos)) {
            return Result.ok(false);
        }
        PlatformOrderInfo info = infos.get(0);
        if (info == null) {
            return Result.ok(false);
        }
        return Result.ok(Stream.of(311, 312).anyMatch(status -> status == info.getStatus()));
    }

    @Override
    public String type() {
        return "all580";
    }

    private int returnFee(JSONObject item, TOrder order, int quantity) {
        int fixed = item.getInt("fixed", 0);
        int percent = item.getInt("percent", 0);
        int type = item.getInt("type");
        return (int) ((type == 5071 ? fixed : NumberUtil.div(percent, 100) * order.getMoney()) * quantity);
    }

    private Date parseRefundDay(JSONObject object, TOrder order) {
        int day = object.getInt("day");
        String time = object.getStr("time");
        String[] arr = time.split(":");
        Date date = DateUtil.offsetDay(order.getBooking(), day);
        date = DateUtil.offsetHour(date, Integer.parseInt(arr[0]));
        date = DateUtil.offsetMinute(date, Integer.parseInt(arr[1]));
        return date;
    }

    private List<Map> parseRefundRule(String rule) {
        if (!JSONUtil.isJsonObj(rule)) {
            return Collections.emptyList();
        }
        JSONObject jsonObject = JSONUtil.parseObj(rule);
        boolean all = jsonObject.getBool("all", true);
        boolean refund = jsonObject.getBool("refund", true);
        if (all || !refund) {
            return Collections.emptyList();
        }
        JSONArray jsonArray = jsonObject.getJSONArray("rule");
        return jsonArray.stream().map(o -> {
            JSONObject object = (JSONObject) o;
            String beforeStr = object.getStr("before");
            JSONObject before = JSONUtil.isJsonObj(beforeStr) ? object.getJSONObject("before") : null;
            String afterStr = object.getStr("after");
            JSONObject after = JSONUtil.isJsonObj(afterStr) ? object.getJSONObject("after") : null;
            String beforeDesc = desc(before, i -> i < 0 ? "<=前" : i > 0 ? "<=后" : "<=当天");
            String afterDesc = desc(after, i -> i < 0 ? "前" : i > 0 ? "后" : "当天");
            afterDesc = StrUtil.isBlank(afterDesc) ? afterDesc : afterDesc + "<";
            int type = object.getInt("type");
            Integer percent = object.getInt("percent");
            Integer fixed = object.getInt("fixed");
            String val = type == 5072 ? String.format("手续费%d%%", percent) : String.format("手续费%d", (int) NumberUtil.div((int) fixed, 100));
            return MapUtil.builder("refundDesc", afterDesc + "T" + beforeDesc)
                    .put("refundRate", val).build();
        }).collect(Collectors.toList());
    }

    private String desc(JSONObject object, Function<Integer, String> function) {
        if (object != null && object.containsKey("day")){
            String time = object.getStr("time");
            int day = object.getInt("day", 0);
            int compare = Integer.compare(day, 0);
            return day == 0 ? String.format("%s%s", function.apply(0), time) : String.format("%s%d天%s", function.apply(compare), Math.abs(day), time);
        }
        return "";
    }
}
