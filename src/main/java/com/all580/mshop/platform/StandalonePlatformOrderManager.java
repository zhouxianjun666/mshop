package com.all580.mshop.platform;

import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 13:32
 */
@Component
public class StandalonePlatformOrderManager extends All580PlatformOrderManager {
    @Override
    public String type() {
        return "standalone";
    }
}
