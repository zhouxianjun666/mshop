package com.all580.mshop.platform;

import org.springframework.stereotype.Component;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 13:39
 */
@Component
public class StandalonePlatformProductManager extends All580PlatformProductManager {

    @Override
    public String type() {
        return "standalone";
    }
}
