package com.all580.mshop.platform;

import com.all580.mshop.Constant;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TMerchant;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 13:43
 */
public interface PlatformProductApi extends PlatformApi {
    /**
     * 获取平台子产品列表
     * @param merchant 商户
     * @param name 产品/子产品 名称
     * @param type 产品类型
     * @param pageNum 页码
     * @param pageSize 页大小
     * @return
     */
    PlatformProductResult items(TMerchant merchant, String name, int type, int pageNum, int pageSize);

    /**
     * 获取平台子产品列表
     * @param merchant 商户
     * @param name 产品/子产品 名称
     * @param type 产品类型
     * @param productId 主产品ID
     * @param codes 子产品编号集合
     * @param pageNum 页码
     * @param pageSize 页大小
     * @return
     */
    PlatformProductResult items(TMerchant merchant, String name, int type, Integer productId, Set<Long> codes , int pageNum, int pageSize);

    /**
     * 获取主产品集合的每个子产品最小价格
     * @param merchant 商户
     * @param start 游玩日期
     * @param codes 子产品编号集合
     * @return
     */
    List<ProductMinPrice> minPrice(TMerchant merchant, Date start, Set<Long> codes);

    /**
     * 获取产品信息
     * @param merchant 商户
     * @param type 类型
     * @param productId 产品ID
     * @param start 游玩日期
     * @param codes 指定子产品集
     * @return
     */
    default PlatformProductInfo info(TMerchant merchant, int type, int productId, Date start, Collection<Long> codes) {
        switch (type) {
            case Constant.PlatformProductType.TICKET:
                return ticketInfo(merchant, productId, start, codes);
            case Constant.PlatformProductType.HOTEL:
                return hotelInfo(merchant, productId, start, codes);
            case Constant.PlatformProductType.LINE:
                return lineInfo(merchant, productId, start, codes);
            default:
                throw new RuntimeException("不支持的类型");
        }
    }

    /**
     * 景点产品信息
     * @param merchant 商户
     * @param productId 产品ID
     * @param start 游玩日期
     * @param codes 指定子产品集
     * @return
     */
    PlatformProductInfo ticketInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes);

    /**
     * 酒店产品信息
     * @param merchant 商户
     * @param productId 产品ID
     * @param start 游玩日期
     * @param codes 指定子产品集
     * @return
     */
    PlatformProductInfo hotelInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes);

    /**
     * 线路产品信息
     * @param merchant 商户
     * @param productId 产品ID
     * @param start 游玩日期
     * @param codes 指定子产品集
     * @return
     */
    PlatformProductInfo lineInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes);

    /**
     * 销售日历
     * @param merchant 商户
     * @param code 子产品编号
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    List<PlatformCalendar> calendar(TMerchant merchant, long code, Date start, Date end);

    /**
     * 搜索主产品
     * @param merchant 商户
     * @param type 产品类型
     * @param name 名称
     * @return
     */
    List<PlatformProductInfo> searchProduct(TMerchant merchant, int type, String name);

    /**
     * 搜索子产品
     * @param merchant 商户
     * @param productId 主产品ID
     * @param name 名称
     * @return
     */
    List<PlatformProductItem> searchSub(TMerchant merchant, int productId, String name);
}
