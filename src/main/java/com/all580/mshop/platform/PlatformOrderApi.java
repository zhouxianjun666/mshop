package com.all580.mshop.platform;

import com.all580.mshop.dto.*;
import com.all580.mshop.entity.BasicProduct;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.entity.TOrder;
import com.all580.mshop.entity.TRefundOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/12 11:51
 */
public interface PlatformOrderApi extends PlatformApi {
    /**
     * 下单
     * @param merchant 商户
     * @param params 订单参数
     * @param product 产品
     * @return
     */
    Result<?> create(TMerchant merchant, Map<String, Object> params, BasicProduct product);

    /**
     * 下单
     * @param merchant 商户
     * @param params 订单参数
     * @param type 产品类型
     * @return
     */
    Result<?> create(TMerchant merchant, Map<String, Object> params, int type);

    /**
     * 支付订单
     * @param merchant 商户
     * @param order 订单
     * @return
     */
    Result<?> payment(TMerchant merchant, TOrder order);

    /**
     * 批量支付订单
     * @param merchant 商户
     * @param orders 订单
     * @return
     */
    Result<?> batchPayment(TMerchant merchant, Set<String> orders);

    /**
     * 批量拉取订单信息
     * @param merchant 商户
     * @param orders 订单集合
     * @return
     */
    List<PlatformOrderInfo> batchInfo(TMerchant merchant, List<TOrder> orders);

    /**
     * 批量拉取订单信息
     * @param merchant 商户
     * @param numbers 订单集合
     * @return
     */
    List<PlatformOrderInfo> batchInfo(TMerchant merchant, Set<Long> numbers);

    /**
     * 获取订单详情
     * @param merchant 商户
     * @param order 订单
     * @return
     */
    PlatformItemDetail detail(TMerchant merchant, TOrder order);

    /**
     * 退订预览
     * @param merchant 商户
     * @param order 订单
     * @return
     */
    PlatformRefundPreview refundPreview(TMerchant merchant, TOrder order);

    /**
     * 计算退订手续费
     * @param merchant 商户
     * @param order 订单
     * @param refundDate 退订时间
     * @param quantity 退订数量
     * @return
     */
    int calcRefundFee(TMerchant merchant, TOrder order, Date refundDate, int quantity);

    /**
     * 退订
     * @param merchant 商户
     * @param order 订单
     * @param params 参数
     * @return
     */
    Result<PlatformRefundResult> refund(TMerchant merchant, TOrder order, Map<String, Object> params);

    /**
     * 批量判断是否退订完成
     * @param merchant 商户
     * @param refundOrders 退订订单列表
     * @return
     */
    Map<Long, Boolean> isRefund(TMerchant merchant, List<TRefundOrder> refundOrders);

    /**
     * 判断是否退订完成
     * @param merchant 商户
     * @param number 退订订单号
     * @return
     */
    boolean isRefund(TMerchant merchant, Long number);

    /**
     * 转换订单
     * @param merchant 商户
     * @param params 请求参数
     * @param result 下单返回
     * @param refundRule 是否需要退订规则
     * @return
     */
    OrderTmp transformOrder(TMerchant merchant, Map<String, Object> params, Result<?> result, boolean refundRule);

    /**
     * 转换订单
     * @param merchant 商户
     * @param params 请求参数
     * @param result 下单返回
     * @return
     */
    OrderTmp transformOrder(TMerchant merchant, Map<String, Object> params, Result<?> result);

    /**
     * 重新支付
     * @param merchant 商户
     * @param order 订单
     * @return
     */
    Result<?> retryPayment(TMerchant merchant, TOrder order);

    /**
     * 支付失败是否可以重新支付
     * @param merchant 商户
     * @param order 订单
     * @param result 支付结果
     * @return
     */
    Result<Boolean> canRetryPayment(TMerchant merchant, TOrder order, Result<?> result);

    /**
     * 判断是否可以支付
     * @param merchant 商户
     * @param order 订单
     * @return
     */
    Result<Boolean> canPayment(TMerchant merchant, TOrder order);
}
