package com.all580.mshop.platform;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.all580.mshop.dto.*;
import com.all580.mshop.entity.TMerchant;
import com.all580.mshop.util.Util;
import com.jayway.jsonpath.JsonPath;
import lombok.SneakyThrows;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zhouxianjun(Alone)
 * @ClassName:
 * @Description:
 * @date 2018/9/11 13:39
 */
@Component
public class All580PlatformProductManager extends AbstractPlatformManager implements PlatformProductApi {
    private static final JsonPath TICKETS_PATH = JsonPath.compile("$.list[*]");

    @Override
    public String type() {
        return "all580";
    }

    @Override
    @SneakyThrows
    public PlatformProductResult items(TMerchant merchant, String name, int type, int pageNum, int pageSize) {
        return items(merchant, name, type, null, null, pageNum, pageSize);
    }

    @Override
    @SneakyThrows
    public PlatformProductResult items(TMerchant merchant, String name, int type, Integer productId, Set<Long> codes, int pageNum, int pageSize) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(7))
                .put("type", type)
                .put("name", name)
                .put("productId", productId)
                .put("codes", codes)
                .put("record_start", (pageNum - 1) * pageSize)
                .put("record_count", pageSize)
                .build();

        Result<?> result = get(merchant, "/api/plan/sale/sub/list", params);
        if (!result.isSuccess()) {
            return new PlatformProductResult();
        }
        JSONObject json = (JSONObject) result.getValue();
        return new PlatformProductResult()
                .setTotal(json.getInt("totalCount", 0))
                .setList(transformList(Util.readJson(TICKETS_PATH, json), map -> {
                    PlatformProductItem item = new PlatformProductItem();
                    String img = MapUtils.getString(map, "imgs");
                    if (JSONUtil.isJsonArray(img)) {
                        item.setImg(JSONUtil.parseArray(img).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
                    }
                    String subImg = MapUtils.getString(map, "sub_imgs");
                    if (JSONUtil.isJsonArray(subImg)) {
                        item.setSubImg(JSONUtil.parseArray(subImg).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
                    }
                    return item;
                }, MapUtil.builder("productSubName", "name").put("price", "purchasePrice").build()));
    }

    @Override
    @SneakyThrows
    public List<ProductMinPrice> minPrice(TMerchant merchant, Date start, Set<Long> codes) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(3))
                .put("start", DateUtil.formatDate(start))
                .put("codes", codes)
                .build();
        Result<?> result = get(merchant, "/api/product/sale/min/price", params);
        if (!result.isSuccess()) {
            return Collections.emptyList();
        }
        JSONArray json = (JSONArray) result.getValue();
        return JSONUtil.toList(json, ProductMinPrice.class);
    }

    @Override
    public PlatformProductInfo ticketInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes) {
        return info(merchant, productId, start, codes, "ticket", map -> transform(map, m -> {
            PlatformProductInfo info = new PlatformProductInfo();
            String img = MapUtils.getString(m, "imgs");
            if (JSONUtil.isJsonArray(img)) {
                info.setImg(JSONUtil.parseArray(img).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
            }
            info.setAddress(MapUtils.getString(m, "pcastr", "") + MapUtils.getString(m, "address", ""));

            transformSub(m, info);
            return info;
        }, MapUtil.builder("blurb", "summary").build(), "address"));
    }

    @Override
    public PlatformProductInfo hotelInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes) {
        return info(merchant, productId, start, codes, "hotel", map -> transform(map, m -> {
            PlatformProductInfo info = new PlatformProductInfo();
            String img = MapUtils.getString(m, "imgs");
            if (JSONUtil.isJsonArray(img)) {
                info.setImg(JSONUtil.parseArray(img).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
            }
            info.setAddress(MapUtils.getString(m, "pic_address", "") + MapUtils.getString(m, "address", ""));

            transformSub(m, info);
            return info;
        }, MapUtil.builder("levelType", "level").build(), "address"));
    }

    @Override
    public PlatformProductInfo lineInfo(TMerchant merchant, int productId, Date start, Collection<Long> codes) {
        return info(merchant, productId, start, codes, "line", map -> transform(map, m -> {
            LineProductInfo info = new LineProductInfo();
            String img = MapUtils.getString(m, "imgs");
            if (JSONUtil.isJsonArray(img)) {
                info.setImg(JSONUtil.parseArray(img).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
            }
            String label = MapUtils.getString(m, "label");
            if (JSONUtil.isJsonArray(label)) {
                info.setLabel(JSONUtil.parseArray(label).stream().map(Object::toString).collect(Collectors.toList()));
            }

            transformSub(m, info);
            return info;
        }, MapUtil.builder("detail", "summary").build()));
    }

    @Override
    @SneakyThrows
    public List<PlatformCalendar> calendar(TMerchant merchant, long code, Date start, Date end) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(5))
                .put("start_date",  DateUtil.formatDateTime(start))
                .put("end_date",  DateUtil.formatDateTime(end))
                .put("product_sub_code", code)
                .put("record_count", (int) (DateUtil.betweenDay(start, end, true) * 1.2))
                .build();

        Result<?> result = get(merchant, "/api/plan/sale/calendar", params);
        if (!result.isSuccess()) {
            return Collections.emptyList();
        }
        JSONObject json = (JSONObject) result.getValue();
        return transformList(Util.readJson(TICKETS_PATH, json), map -> new PlatformCalendar());
    }

    @Override
    @SneakyThrows
    public List<PlatformProductInfo> searchProduct(TMerchant merchant, int type, String name) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(3))
                .put("type", type)
                .put("name", name)
                .build();

        Result<?> result = get(merchant, "/api/product/list/sale/simple", params);
        if (!result.isSuccess()) {
            return Collections.emptyList();
        }
        JSONArray json = (JSONArray) result.getValue();
        return JSONUtil.toList(json, PlatformProductInfo.class);
    }

    @Override
    @SneakyThrows
    public List<PlatformProductItem> searchSub(TMerchant merchant, int productId, String name) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(5))
                .put("product_id", productId)
                .put("ticketType", 5031)
                .put("payType", 5011)
                .put("name", name)
                .build();

        Result<?> result = get(merchant, "/api/product/sub/list/sale/simple", params);
        if (!result.isSuccess()) {
            return Collections.emptyList();
        }

        JSONArray json = (JSONArray) result.getValue();
        return transformList(JSONUtil.toList(json, Map.class),
                map -> new PlatformProductItem(), MapUtil.builder("settlePrice", "purchasePrice").build());
    }

    @SneakyThrows
    private Result<?> info(TMerchant merchant, int productId, Date start, Collection<Long> codes, String type) {
        Map<String, Object> params = MapUtil.builder(new HashMap<String, Object>(4))
                .put("start", start == null ? null : DateUtil.formatDate(start))
                .put("product_id", productId)
                .put("codes", codes)
                .build();
        return get(merchant, String.format("/api/product/%s/info", type), params);
    }

    private PlatformProductInfo info(TMerchant merchant, int productId, Date start, Collection<Long> codes, String type, Function<Map, PlatformProductInfo> function) {
        Result<?> result = info(merchant, productId, start, codes, type);
        if (!result.isSuccess()) {
            return null;
        }
        Map map = JSONUtil.toBean((JSONObject) result.getValue(), Map.class);
        return function.apply(map);
    }

    @SuppressWarnings("unchecked")
    private void transformSub(Map map, PlatformProductInfo info) {
        List<Map> subs = (List<Map>) map.get("subs");
        info.setSubInfo(transformList(subs, itemMap -> {
            PlatformSubInfo subInfo = new PlatformSubInfo();
            String itemImg = MapUtils.getString(itemMap, "imgs");
            if (JSONUtil.isJsonArray(itemImg)) {
                subInfo.setImg(JSONUtil.parseArray(itemImg).stream().map(o -> ((JSONObject) o).getStr("link")).collect(Collectors.toList()));
            }
            return subInfo;
        }));
    }
}
